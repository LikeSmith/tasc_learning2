'''
Contains objects used in multiple different tasc configs
'''

import numpy as np
from keras.layers import Layer
from keras.callbacks import Callback
import keras.backend as KK

def quad(x, A):
    return KK.batch_dot(x, KK.dot(x, KK.transpose(A)), 1)

def norm(x):
    return KK.sqrt(KK.dot(x, x))

class nModel_Checkpoint(Callback):
    def __init__(self, mdls_to_save, net_files=None, monitor='val_loss', verbose=0, save_best_only=True, mode='auto', period=1):
        super(ModelCheckpoint, self).__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.mdls_to_save = mdls_to_save
        self.net_files = net_files
        self.save_best_only = save_best_only
        self.period = period
        self.epochs_since_last_save = 0

        if mode not in ['auto', 'min', 'max']:
            warnings.warn('ModelCheckpoint mode %s is unknown, '
                          'fallback to auto mode.' % (mode),
                          RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                self.monitor_op = np.less
                self.best = np.Inf

    def save_mdls(self):
        for i in range(len(self.mdls_to_save)):
            if self.net_files is not None:
                self.mdls_to_save[i].save(self.net_files[i])
            else:
                self.mdls_to_save[i].save()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    warnings.warn('Can save best model only with %s available, '
                                  'skipping.' % (self.monitor), RuntimeWarning)
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            print('Epoch %05d: %s improved from %0.5f to %0.5f,'
                                  ' saving models'
                                  % (epoch, self.monitor, self.best,
                                     current))
                        self.best = current
                        self.save_mdls()
                    else:
                        if self.verbose > 0:
                            print('Epoch %05d: %s did not improve' %
                                  (epoch, self.monitor))
            else:
                if self.verbose > 0:
                    print('Epoch %05d: saving model' % (epoch,))
                self.save_mdls()

class Add_Loss(Layer):
    def __init__(self, **kwargs):
        self.is_placeholder = True
        super(Add_Loss, self).__init__(**kwargs)

    def magnitude(self, x):
        return KK.sqrt(KK.batch_dot(x, x, 1))

    def call(self, inputs):
        loss = KK.mean(self.magnitude(inputs))
        self.add_loss(loss, inputs=inputs)
        # returned value should not actually be used
        return loss
