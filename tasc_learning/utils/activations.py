'''
custom activation functions
'''

import keras.backend as KK


avail_activ = ['leakyRelu', 'leakyRelu0_01']

def get(func):
    if func is None:
        return 'linear'
    elif isinstance(func, list):
        ret = []
        for func_i in func:
            ret += [get(func_i)]

        return ret
    elif isinstance(func, str):
        func_id = func.split(':')[0]
        if func_id in avail_activ:
            if func_id == avail_activ[0]:
                alpha = float(func.split(':')[1])
                return leakyRelu(alpha)
            elif func_id == avail_activ[1]:
                return leakyRelu0_01
    return func

def gen_custom_obj(labels, cust={}):
    if isinstance(labels, list) or isinstance(labels, tuple):
        for label in labels:
            if isinstance(label, str) and label not in cust.keys() and label.split(':')[0] in avail_activ:
                cust[label] = get(label)
    elif isinstance(labels, str) and labels not in cust.keys() and labels.split(':')[0] in avail_activ:
        cust[labels] = get(labels)
    return cust

def get_activ_lipshitz(label):
    if label == 'relu':
        return 1.0
    elif label == 'tanh':
        return 1.0
    elif label == 'elu':
        return 1.0
    elif label == 'sigmoid':
        return 1.0
    elif label == 'linear':
        return 1.0
    elif label == 'leakyRelu0_01':
        return 1.0
    elif label.split(':')[0] == 'leakyRelu':
        return max([1.0, float(label.split(':')[1])])

def leakyRelu(alpha):
    def f(x):
        return KK.relu(x, alpha=alpha)
    return f

def leakyRelu0_01(x):
    return KK.relu(x, alpha=0.01)
