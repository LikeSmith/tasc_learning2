'''
lolRoa.py

This code evaluates the Region of Attraction for results of LOL training
'''

import numpy as np
from itertools import combinations as combos
import matplotlib.pyplot as plt

class Triangulation(object):
    verts = None
    simps = None
    def __init__(self, n, bound_verts, bound_faces):
        self.n = n

        self.add_verts(bound_verts)
        self.orig = self.add_verts(np.zeros((1, self.n)))

        new_simps = np.concatenate((bound_faces, self.orig*np.ones((bound_faces.shape[0], 1), dtype=int)), 1)
        self.add_simps(new_simps)

    def add_verts(self, toadd):
        if len(toadd.shape) == 1:
            toadd = np.expand_dims(toadd, 0)
        assert len(toadd.shape) == 2 and toadd.shape[1] == self.n

        if self.verts is None:
            self.verts = np.copy(toadd)
            start = 0
        else:
            start = self.verts.shape[0]
            self.verts = np.concatenate((self.verts, toadd), 0)

        return start

    def add_simps(self, toadd):
        if len(toadd.shape) == 1:
            toadd = np.expand_dims(toadd, 0)
        assert len(toadd.shape) == 2 and toadd.shape[1] == self.n+1

        if self.simps is None:
            self.simps = np.copy(toadd)
            start = 0
        else:
            start = self.simps.shape[0]
            self.simps = np.concatenate((self.simps, toadd), 0)

        return start

    def subdivide_simps(self, ind, center=None, rand=False):
        if center is None:
            if rand:
                lam = np.random.rand(self.n+1)
                lam /= np.sum(lam)
            else:
                lam = np.ones(self.n+1)*(1.0/(self.n+1.0))

            center = np.zeros(self.n)
            for i in range(self.n+1):
                center += lam[i]*self.verts[self.simps[ind, i], :]

        new_vert = self.add_verts(center)

        old_simps = self.simps[ind, :]
        new_simps = np.concatenate((np.array(list(combos(old_simps, self.n))), new_vert*np.ones((self.n+1, 1), dtype=int)), 1)
        self.simps = np.delete(self.simps, ind, 0)
        self.add_simps(new_simps)

    def __getitem__(self, key):
        if not isinstance(key, tuple):
            ret = np.zeros((self.n+1, self.n))
            for i in range(self.n+1):
                ret[i, :] = self.verts[self.simps[key, i], :]

            return ret
        elif len(key) == 2:
            return self.verts[self.simps[key[0], key[1]], :]
        elif len(key) == 3:
            return self.verts[self.simps[key[0], key[1]], key[2]]
        else:
            raise KeyError

class Lyap_Triangulation(Triangulation):
    dsc = None
    asc = None
    sta = None
    rod = None

    def __init__(self, lyap=None, f=None, eps_simp=0.1, eps_lips=0.001, **kwargs):
        self.lyap = lyap
        self.f = f
        self.eps_simp = eps_simp
        self.eps_lips = eps_lips

        super(Lyap_Triangulation, self).__init__(**kwargs)

        self.V = zeros(self.verts.shape)

    def sort_verts(self):
        self.dsc = []
        self.asc = []
        self.sta = []

        diffs = self.lyap(self.f(self.verts)) - self.lyap(self.verts)
        for i in diffs.shape[0]:
            if diffs[i, 0] < 0.0:
                self.dsc += [i]
            elif diffs]i, 0] > 0.0
                self.asc += [i]
            else:
                self.sta += [i]

if __name__ == '__main__':
    verts = np.array([[1.0, 1.0], [-1.0, 1.0], [-1.0, -1.0], [1.0, -1.0]])
    faces = np.array([[0, 1], [1, 2], [2, 3], [3, 4]])
    n = 2

    Tri = Triangulation(n, verts, faces)

    Tri.subdivide_simps(0, center=np.array([0.0, 0.25]))
    Tri.subdivide_simps(0, rand=True)
    Tri.subdivide_simps(0)

    plt.figure()
    for i in range(Tri.simps.shape[0]):
        plt.plot([Tri[i, 0, 0], Tri[i, 1, 0]], [Tri[i, 0, 1], Tri[i, 1, 1]])
        plt.plot([Tri[i, 1, 0], Tri[i, 2, 0]], [Tri[i, 1, 1], Tri[i, 2, 1]])
        plt.plot([Tri[i, 2, 0], Tri[i, 0, 0]], [Tri[i, 2, 1], Tri[i, 0, 1]])

    plt.show()
