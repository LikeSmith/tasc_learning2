from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm

import numpy as np

import keras
import keras.backend as KK
from keras.models import load_model, Model
from keras.layers import Lambda
from keras.layers.merge import Concatenate


def f_jacobian_symb(model, inidx, outidx, batchsize=1):
    acc = f_derivative_symb(model, inidx, outidx, batchsize, 0)
    for idx in np.arange(1, batchsize):
        res = f_derivative_symb(model, inidx, outidx, batchsize, idx)
        acc += res
    return acc


def f_derivative_symb(model, inidx, outidx, batchsize=1, batchidx=0):
    '''out_shape = model.layers[-1].get_output_shape_at(outidx)[1]
    in_shape = model.layers[-1].get_input_shape_at(inidx)[1]
    out_channels = model.layers[-1].get_output_at(outidx)
    in_channels = model.layers[-1].get_input_at(inidx)'''
    if isinstance(model.output_shape, list):
        out_shape = model.output_shape[outidx][1]
    else:
        out_shape = model.output_shape[1]
    if isinstance(model.input_shape, list):
        in_shape = model.input_shape[inidx][1]
    else:
        in_shape = model.input_shape[1]
    out_channels = model.outputs[outidx]
    in_channels = model.inputs[inidx]

    grads = []
    for oidx in range(out_shape):
        if out_shape == 1 and outidx == 0:
            grad = Lambda(
                lambda output:
                KK.gradients(
                    output,
                    model.inputs[inidx]
                ),
                output_shape=(in_shape,)
            )(model.outputs[outidx])
        else:
            grad = Lambda(
                lambda output:
                KK.gradients(
                    output[batchidx][oidx],
                    model.inputs[inidx]
                ),
                output_shape=(in_shape,)
            )(model.outputs[outidx])
        grads.append(grad)

    if len(grads) > 1:
        grads = Concatenate(
            name='concat_'+str(inidx)+'_'+str(outidx)
        )(grads)

    output = Lambda(
        lambda result:
        KK.reshape(result, (batchsize, in_shape, out_shape)),
        output_shape=(None, in_shape, out_shape)
    )(grads)

    return output


def f_derivative_symb_test(model, inidx, outidx):
    output = f_derivative_symb(model, inidx, outidx)

    temp = Model(
        model.inputs,
        output
    )

    x_in_shape = model.input_shape[0][1]
    u_in_shape = model.input_shape[1][1]
    x_in = np.random.rand(1,x_in_shape).reshape(1,x_in_shape)
    u_in = np.random.rand(1,u_in_shape).reshape(1,u_in_shape)

    result = temp.predict([x_in, u_in])

    print('Result: ', result, '\tShape: ', result.shape)

    return result


def calc_grad_ends(sample, value, grad, deltaX):
    queryX = sample + deltaX
    estimate = value + np.dot(grad, np.transpose(deltaX))
    return np.concatenate(
        [queryX, estimate.reshape(1,1)],
        axis=1
    )


if __name__ == '__main__':
    x_symb = keras.layers.Input((2,))

    parabola = KK.sum(KK.square(x_symb))
    P = Lambda(lambda x: KK.sum(KK.square(x)), output_shape=(1,))
    M = Model([x_symb], [P(x_symb)])
    D = f_derivative_symb(M, 0, 0)
    deriv_model = Model(M.inputs, D)

    fn = KK.function([x_symb], [parabola])

    #fnd = KK.function([x_symb], 2.*x_symb)
    fnd = KK.function([x_symb], KK.gradients(parabola, x_symb))

    sample_point1 = np.matrix([-10,0])
    sample_point2 = np.matrix([-10,0])

    print('sample_point1: ', sample_point1)
    print('fn value: ', fn([sample_point1]))
    print('derivative value: ', fnd([sample_point1]))
    print('sample_point2: ', sample_point2)
    print('fn value: ', fn([sample_point2]))
    print('derivative value: ', fnd([sample_point2]))

    bound = 10
    xn = yn = 11
    x = np.linspace(-bound, bound, xn)
    y = np.linspace(-bound, bound, yn)
    xv, yv = np.meshgrid(x, y)
    xn2 = xn*6+1
    yn2 = yn*6+1
    x2 = np.linspace(-bound, bound, xn2)
    y2 = np.linspace(-bound, bound, yn2)
    xv2, yv2 = np.meshgrid(x2, y2)
    val2 = np.zeros((xn2, yn2))
    for yidx in np.arange(yn2):
        for xidx in np.arange(xn2):
            sample = np.matrix([xv2[xidx, yidx], yv2[xidx, yidx]])
            val2[xidx, yidx] = fn([sample])[0]

    val = np.zeros((xn, yn))
    grad_ends_l = np.zeros((xn, yn, 3))
    grad_ends_h = np.zeros((xn, yn, 3))
    nn_grad_ends_l = np.zeros((xn, yn, 3))
    nn_grad_ends_h = np.zeros((xn, yn, 3))
    for yidx in np.arange(yn):
        for xidx in np.arange(xn):
            sample = np.matrix([xv[xidx, yidx], yv[xidx, yidx]])
            val[xidx, yidx] = fn([sample])[0]
            deltaXL = -3*sample/np.linalg.norm(sample)
            deltaXH = 3*sample/np.linalg.norm(sample)

            grad = fnd([sample])
            grad_ends_l[xidx, yidx] = calc_grad_ends(
                sample,
                val[xidx, yidx],
                grad,
                deltaXL
            )
            grad_ends_h[xidx, yidx] = calc_grad_ends(
                sample,
                val[xidx, yidx],
                grad,
                deltaXH
            )

            nn_grad = deriv_model.predict(sample).reshape(1,2)
            nn_grad_ends_l[xidx, yidx] = calc_grad_ends(
                sample,
                val[xidx, yidx],
                nn_grad,
                deltaXL
            )
            nn_grad_ends_h[xidx, yidx] = calc_grad_ends(
                sample,
                val[xidx, yidx],
                nn_grad,
                deltaXH
            )

    plt.suptitle('2D projection of 3D parabola')
    plt.subplot(2, 1, 1)
    plt.title('x')
    xidx = xn2/2
    plt.scatter(xv2[xidx, :], val2[xidx, :])
    xidx = xn/2
    for yidx in np.arange(xn):
        plt.plot(
            [
                nn_grad_ends_l[xidx, yidx, 0],
                nn_grad_ends_h[xidx, yidx, 0]
            ],
            [
                nn_grad_ends_l[xidx, yidx, 2],
                nn_grad_ends_h[xidx, yidx, 2]
            ]
        )
    plt.xlabel('x')
    plt.ylabel('z')

    plt.subplot(2, 1, 2)
    plt.title('y')
    yidx = yn2/2
    plt.scatter(yv2[:, yidx], val2[:, yidx])
    yidx = yn/2
    for xidx in np.arange(xn):
        plt.plot(
            [
                nn_grad_ends_l[xidx, yidx, 1],
                nn_grad_ends_h[xidx, yidx, 1]
            ],
            [
                nn_grad_ends_l[xidx, yidx, 2],
                nn_grad_ends_h[xidx, yidx, 2]
            ]
        )
    plt.xlabel('y')
    plt.ylabel('z')
    plt.show()


    fig = plt.figure()
    plt.suptitle('3D parabola')
    ax = fig.add_subplot(2,2,1, projection='3d')
    plt.title('x = 0')
    yidx = yn2/2
    ax.scatter(
        xv2[:,yidx],
        yv2[:, yidx],
        val2[:, yidx],
        cmap=cm.coolwarm
    )
    yidx = yn/2
    for xidx in np.arange(xn):
        ax.plot(
            [
                nn_grad_ends_l[xidx, yidx, 0],
                nn_grad_ends_h[xidx, yidx, 0]
            ],
            [
                nn_grad_ends_l[xidx, yidx, 1],
                nn_grad_ends_h[xidx, yidx, 1]
            ],
            [
                nn_grad_ends_l[xidx, yidx, 2],
                nn_grad_ends_h[xidx, yidx, 2]
            ]
        )
        ax.plot(
            [
                grad_ends_l[xidx, yidx, 0],
                grad_ends_h[xidx, yidx, 0]
            ],
            [
                grad_ends_l[xidx, yidx, 1],
                grad_ends_h[xidx, yidx, 1]
            ],
            [
                grad_ends_l[xidx, yidx, 2],
                grad_ends_h[xidx, yidx, 2]
            ],
            linestyle='-.',
            color='k'
        )
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')


    ax = fig.add_subplot(2,2,2, projection='3d')
    plt.title('y = 0')
    xidx = xn2/2
    ax.scatter(
        xv2[xidx, :],
        yv2[xidx, :],
        val2[xidx, :],
        cmap=cm.coolwarm
    )
    xidx = 5
    for yidx in np.arange(yn):
        ax.plot(
            [
                nn_grad_ends_l[xidx, yidx, 0],
                nn_grad_ends_h[xidx, yidx, 0]
            ],
            [
                nn_grad_ends_l[xidx, yidx, 1],
                nn_grad_ends_h[xidx, yidx, 1]
            ],
            [
                nn_grad_ends_l[xidx, yidx, 2],
                nn_grad_ends_h[xidx, yidx, 2]
            ]
        )
        ax.plot(
            [
                grad_ends_l[xidx, yidx, 0],
                grad_ends_h[xidx, yidx, 0]
            ],
            [
                grad_ends_l[xidx, yidx, 1],
                grad_ends_h[xidx, yidx, 1]
            ],
            [
                grad_ends_l[xidx, yidx, 2],
                grad_ends_h[xidx, yidx, 2]
            ],
            linestyle='-.',
            color='k'
        )
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')


    ax = fig.add_subplot(2,2,3, projection='3d')
    plt.title('x = y')
    idx = np.array(range(xn2))
    ax.scatter(
        xv2[idx, idx],
        yv2[idx, idx],
        val2[idx, idx],
        cmap=cm.coolwarm
    )
    for idx in np.arange(yn):
        ax.plot(
            [
                nn_grad_ends_l[idx, idx, 0],
                nn_grad_ends_h[idx, idx, 0]
            ],
            [
                nn_grad_ends_l[idx, idx, 1],
                nn_grad_ends_h[idx, idx, 1]
            ],
            [
                nn_grad_ends_l[idx, idx, 2],
                nn_grad_ends_h[idx, idx, 2]
            ]
        )
        ax.plot(
            [
                grad_ends_l[idx, idx, 0],
                grad_ends_h[idx, idx, 0]
            ],
            [
                grad_ends_l[idx, idx, 1],
                grad_ends_h[idx, idx, 1]
            ],
            [
                grad_ends_l[idx, idx, 2],
                grad_ends_h[idx, idx, 2]
            ],
            linestyle='-.',
            color='k'
        )
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    ax = fig.add_subplot(2,2,4, projection='3d')
    plt.title('Full parabola')
    ax.scatter(xv2, yv2, val2, cmap=cm.coolwarm)

    for yidx in np.arange(yn):
        for xidx in np.arange(xn):
            ax.plot(
                [
                    nn_grad_ends_l[xidx, yidx, 0],
                    nn_grad_ends_h[xidx, yidx, 0]],
                [
                    nn_grad_ends_l[xidx, yidx, 1],
                    nn_grad_ends_h[xidx, yidx, 1]],
                [
                    nn_grad_ends_l[xidx, yidx, 2],
                    nn_grad_ends_h[xidx, yidx, 2]
                ]
            )
            ax.plot(
                [
                    grad_ends_l[xidx, yidx, 0],
                    grad_ends_h[xidx, yidx, 0]
                ],
                [
                    grad_ends_l[xidx, yidx, 1],
                    grad_ends_h[xidx, yidx, 1]
                ],
                [
                    grad_ends_l[xidx, yidx, 2],
                    grad_ends_h[xidx, yidx, 2]
                ],
                linestyle='-.', color='k'
            )
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.show()


def test():
    keras.activations.cos = KK.cos
    
    model = load_model('pendulum_500_1000_full_zeros')

    f_derivative_symb_test(model, 0, 0)
    f_derivative_symb_test(model, 0, 1)
    f_derivative_symb_test(model, 1, 0)
    f_derivative_symb_test(model, 1, 1)

