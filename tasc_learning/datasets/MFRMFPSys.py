'''
This file generates datasets for a differential drive robot

command to generate dataset1:
python3 MFRMFPSys.py -n 1000 -N 1000 -i dataset1_4Rob_params.pkl -I dataset1_4Rob_limits.pkl -o dataset1_4Rob_data.pkl

commadn to generate dataset2 (this dataset encompases only natural dyneamics):
python3 MFRMFPSys.py -n 1000 -N 1000 -i dataset2_4Rob_params.pkl -I dataset2_4Rob_limits.pkl -o dataset2_4Rob_data.pkl
'''

import numpy as np
from scipy import integrate
from scipy import linalg
import keras.backend as K
import getopt
import _pickle as pkl
import sys
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import os

class MFRMFPSys(object):
    home = os.path.dirname(os.path.realpath(__file__)) + '/../../data/MFRMFPSys/'
    datasets_path = 'datasets/'
    params_path = 'params/'

    def __init__(self, param_file, limits_file, home=None, datasets_path=None, params_path=None):
        if home != None:
            self.home = home
        if datasets_path != None:
            self.datasets_path = datasets_path
        if params_path != None:
            self.params_path = params_path

        self.theta_range, self.omega_range, self.p_range, self.v_range, self.u_range, self.m_range, self.c_range = pkl.load(open(self.home+self.params_path+limits_file, 'rb'))
        params = pkl.load(open(self.home+self.params_path+param_file, 'rb'))

        self.del_t = params[0]
        self.J = params[1]
        self.g = params[2]
        self.gammas_p = params[3]
        self.N = params[4]

        self.m = []
        self.gammas_i = []

        for i in range(self.N):
            self.m.append(params[5+i*2])
            self.gammas_i.append(params[6+i*2])

    def f_f(self, vel, i):
        if i == -1:
            gammas = self.gammas_p
        else:
            gammas = self.gammas_i[i]

        return gammas[0]*(np.tanh(gammas[1]*vel) - np.tanh(gammas[2]*vel)) + gammas[3]*np.tanh(gammas[4]*vel) + gammas[5]*vel

    def f_f_keras(self, vel, i):
        if i == -1:
            gammas = self.gammas_p
        else:
            gammas = self.gammas_i[i]

        return gammas[0]*(K.tanh(gammas[1]*vel) - K.tanh(gammas[2]*vel)) + gammas[3]*K.tanh(gammas[4]*vel) + gammas[5]*vel

    def f_s(self, xs, us):
        xs_dot = np.zeros(self.N*2)
        for i in range(self.N):
            xs_dot[i*2+0] = xs[i*2+1]
            xs_dot[i*2+1] = (us[i] - self.f_f(xs[i*2+1], i))/self.m[i]

        return xs_dot

    def f_s_keras(self, xs, us):
        xs_dot = []
        for i in range(self.N):
            p_i_dot = xs[:, i*2+1]
            v_i_dot = (us[:, i] - self.f_f_keras(xs[:, i*2+1], i))/self.m[i]

            p_i_dot = K.expand_dims(p_i_dot)
            v_i_dot = K.expand_dims(v_i_dot)

            xs_dot.append(K.concatenate([p_i_dot, v_i_dot]))

        return K.concatenate(xs_dot)

    def f_p(self, xp, xs):
        xp_dot = np.zeros(2)

        J_eff = self.J + self.m[0]*xs[0]**2
        tau = self.m[0]*xs[0]
        corriolis = self.m[0]*xs[0]*xs[1]

        for i in range(1, self.N):
            J_eff += self.m[i]*xs[i*2]**2
            tau += self.m[i]*xs[i*2]
            corriolis += self.m[i]*xs[i*2]*xs[i*2+1]

        xp_dot[0] = xp[1]
        xp_dot[1] = (-self.g*np.cos(xp[0])*tau - 2*xp[1]*corriolis - self.f_f(xp[1], -1))/J_eff

        return xp_dot

    def f_p_keras(self, xp, xs):
        J_eff = self.J
        tau = 0.0
        corriolis = 0.0

        for i in range(self.N):
            J_eff += self.m[i]*K.pow(xs[:, i*2], 2)
            tau += self.m[i]*xs[:, i*2]
            corriolis += self.m[i]*xs[:, i*2]*xs[:, i*2+1]

        theta_dot = xp[:, 1]
        omega_dot = (-self.g*K.cos(xp[:, 0])*tau - 2*xp[:, 1]* corriolis - self.f_f_keras(xp[:, 1], -1))/J_eff

        theta_dot = K.expand_dims(theta_dot)
        omega_dot = K.expand_dims(omega_dot)

        return K.concatenate([theta_dot, omega_dot])

    def f(self, t, X, U):
        xp = X[0:2]
        xs = X[2:]

        xs_dot = self.f_s(xs, U)
        xp_dot = self.f_p(xp, xs)

        X_dot = np.zeros(2+self.N*2)
        X_dot[0:2] = xp_dot
        X_dot[2:] = xs_dot

        return X_dot

    def f_disc(self, xp, xs, u):
        xs_dot1 = self.f_s(xs, u)
        xp_dot1 = self.f_p(xp, xs)

        xs_dot2 = self.f_s(xs + 0.5*self.del_t*xs_dot1, u)
        xp_dot2 = self.f_p(xp + 0.5*self.del_t*xp_dot1, xs + 0.5*self.del_t*xs_dot1)

        xs_dot3 = self.f_s(xs + 0.5*self.del_t*xs_dot2, u)
        xp_dot3 = self.f_p(xp + 0.5*self.del_t*xp_dot2, xs + 0.5*self.del_t*xs_dot2)

        xs_dot4 = self.f_s(xs + self.del_t*xs_dot3, u)
        xp_dot4 = self.f_p(xp + self.del_t*xp_dot3, xs + self.del_t*xs_dot3)

        xs_new = xs + self.del_t/6.0*(xs_dot1 + 2*xs_dot2 + 2*xs_dot3 + xs_dot4)
        xp_new = xp + self.del_t/6.0*(xp_dot1 + 2*xp_dot2 + 2*xp_dot3 + xp_dot4)

        return [xp_new, xs_new]

    def euler_keras(self, args):
        xp, xs, u = args

        xs_dot = self.f_s_keras(xs, u)
        xp_dot = self.f_s_keras(xp, xs)

        xs_new = xs + self.del_t*xs_dot
        xp_new = xp + self.del_t*xp_dot

        return K.concatenate([xs_new, xp_new])

    def RK4_keras(self, args):
        xp, xs, u = args

        xs_dot1 = self.f_s_keras(xs, u)
        xp_dot1 = self.f_p_keras(xp, xs)

        xs_dot2 = self.f_s_keras(xs + 0.5*self.del_t*xs_dot1, u)
        xp_dot2 = self.f_p_keras(xp + 0.5*self.del_t*xp_dot1, xs + 0.5*self.del_t*xs_dot1)

        xs_dot3 = self.f_s_keras(xs + 0.5*self.del_t*xs_dot2, u)
        xp_dot3 = self.f_p_keras(xp + 0.5*self.del_t*xp_dot2, xs + 0.5*self.del_t*xs_dot2)

        xs_dot4 = self.f_s_keras(xs + self.del_t*xs_dot3, u)
        xp_dot4 = self.f_p_keras(xp + self.del_t*xp_dot3, xs + self.del_t*xs_dot3)

        xs_new = xs + self.del_t/6.0*(xs_dot1 + 2*xs_dot2 + 2*xs_dot3 + xs_dot4)
        xp_new = xp + self.del_t/6.0*(xp_dot1 + 2*xp_dot2 + 2*xp_dot3 + xp_dot4)

        return K.cast(K.concatenate([xp_new, xs_new]), 'float32')

    def RK4_keras_onestate(self, args):
        x, u = args
        xp = x[:, :2]
        xs = x[:, 2:]

        xs_dot1 = self.f_s_keras(xs, u)
        xp_dot1 = self.f_p_keras(xp, xs)

        xs_dot2 = self.f_s_keras(xs + 0.5*self.del_t*xs_dot1, u)
        xp_dot2 = self.f_p_keras(xp + 0.5*self.del_t*xp_dot1, xs + 0.5*self.del_t*xs_dot1)

        xs_dot3 = self.f_s_keras(xs + 0.5*self.del_t*xs_dot2, u)
        xp_dot3 = self.f_p_keras(xp + 0.5*self.del_t*xp_dot2, xs + 0.5*self.del_t*xs_dot2)

        xs_dot4 = self.f_s_keras(xs + self.del_t*xs_dot3, u)
        xp_dot4 = self.f_p_keras(xp + self.del_t*xp_dot3, xs + self.del_t*xs_dot3)

        xs_new = xs + self.del_t/6.0*(xs_dot1 + 2*xs_dot2 + 2*xs_dot3 + xs_dot4)
        xp_new = xp + self.del_t/6.0*(xp_dot1 + 2*xp_dot2 + 2*xp_dot3 + xp_dot4)

        return K.cast(K.concatenate([xp_new, xs_new]), 'float32')

    def euler_swarm(self, x, u):
        return xs + self.del_t*self.f_s(x, u)

    def RK4_swarm(self, x, u):
        x_dot1 = self.f_s(x, u)
        x_dot2 = self.f_s(x + 0.5*self.del_t*x_dot1, u)
        x_dot3 = self.f_s(x + 0.5*self.del_t*x_dot2, u)
        x_dot4 = self.f_s(x + self.del_t*x_dot3, u)

        x_new = x + self.del_t/6.0*(x_dot1 + 2*x_dot2 + 2*x_dot3 + x_dot4)

        return x_new

    def euler_keras_swarm(self, args):
        xs, u = args

        return xs + self.del_t*self.f_s_keras(xs, u)

    def RK4_keras_swarm(self, args):
        xs, u = args

        xs_dot1 = self.f_s_keras(xs, u)
        xs_dot2 = self.f_s_keras(xs + 0.5*self.del_t*xs_dot1, u)
        xs_dot3 = self.f_s_keras(xs + 0.5*self.del_t*xs_dot2, u)
        xs_dot4 = self.f_s_keras(xs + self.del_t*xs_dot3, u)

        xs_new = xs + self.del_t/6.0*(xs_dot1 + 2*xs_dot2 + 2*xs_dot3 + xs_dot4)

        return xs_new

    def gen_swarm_states(self, N):
        xs_min = np.array([self.p_range[0], self.v_range[0]]*self.N).reshape(2*self.N, 1)
        xs_max = np.array([self.p_range[1], self.v_range[1]]*self.N).reshape(2*self.N, 1)
        return xs_min.repeat(N, 1) + (xs_max - xs_min).repeat(N, 1)*np.random.rand(2*self.N, N)

    def gen_parent_states(self, N):
        xp_min = np.array([self.theta_range[0], self.omega_range[0]]).reshape(2, 1)
        xp_max = np.array([self.theta_range[1], self.omega_range[1]]).reshape(2, 1)
        return xp_min.repeat(N, 1) + (xp_max - xp_min).repeat(N, 1)*np.random.rand(2, N)

    def gen_swarm_ctrls(self, N):
        u_min = np.array([self.u_range[0]]*self.N).reshape(self.N, 1)
        u_max = np.array([self.u_range[1]]*self.N).reshape(self.N, 1)

        return u_min.repeat(N, 1) + (u_max - u_min).repeat(N, 1)*np.random.rand(self.N, N)

    def gen_swarm_params(self, swarm_size, N):
        p_min = np.array([[[self.m_range[0]], [self.c_range[0]]]])
        p_max = np.array([[[self.m_range[1]], [self.c_range[1]]]])

        return p_min.repeat(N, 0).repeat(swarm_size, 2) + (p_max - p_min).repeat(N, 0).repeat(swarm_size, 2)*np.random.rand(N, 2, swarm_size)

    def generate_dataset(self, N1, N2):
        xp_min = np.array([self.theta_range[0], self.omega_range[0]]).reshape(2, 1)
        xp_max = np.array([self.theta_range[1], self.omega_range[1]]).reshape(2, 1)
        xs_min = np.array([self.p_range[0], self.v_range[0]]*self.N).reshape(2*self.N, 1)
        xs_max = np.array([self.p_range[1], self.v_range[1]]*self.N).reshape(2*self.N, 1)
        u_min = np.array([self.u_range[0]]*self.N).reshape(self.N, 1)
        u_max = np.array([self.u_range[1]]*self.N).reshape(self.N, 1)

        self.xs = np.zeros((2*self.N, 2*N1*N2))
        self.xp = np.zeros((2, 2*N1*N2))
        self.u = np.zeros((self.N, 2*N1*N2))
        self.xs_new = np.zeros((2*self.N, 2*N1*N2))
        self.xp_new = np.zeros((2, 2*N1*N2))

        xp_tmp = xp_min.repeat(N1, 1) + (xp_max-xp_min).repeat(N1, 1)*np.random.rand(2, N1)

        for i in range(N1):
            xs_tmp = xs_min.repeat(N2, 1) + (xs_max - xs_min).repeat(N2, 1)*np.random.rand(2*self.N, N2)
            u_tmp = u_min.repeat(N2, 1) + (u_max - u_min).repeat(N2, 1)*np.random.rand(self.N, N2)
            xs_new_tmp = np.zeros((2*self.N, N2))
            xp_new_tmp = np.zeros((2, N2))

            for j in range(N2):
                r = integrate.ode(self.f)
                r.set_integrator('dopri5')
                r.set_initial_value(np.concatenate([xp_tmp[:, i], xs_tmp[:, j]]), 0.0)
                r.set_f_params(u_tmp[:, j])
                r.integrate(self.del_t)
                xp_new_tmp[:, j] = r.y[0:2]
                xs_new_tmp[:, j] = r.y[2:]

            self.xp[:, i*N2:i*N2+N2] = xp_tmp[:, i].reshape(2, 1).repeat(N2, 1)
            self.xs[:, i*N2:i*N2+N2] = xs_tmp
            self.u[:, i*N2:i*N2+N2] = u_tmp
            self.xs_new[:, i*N2:i*N2+N2] = xs_new_tmp
            self.xp_new[:, i*N2:i*N2+N2] = xp_new_tmp

        xs_tmp = xs_min.repeat(N2, 1) + (xs_max - xs_min).repeat(N2, 1)*np.random.rand(2*self.N, N2)

        for i in range(N2):
            xp_tmp = xp_min.repeat(N1, 1) + (xp_max - xp_min).repeat(N1, 1)*np.random.rand(2, N1)
            u_tmp = u_min.repeat(N1, 1) + (u_max - u_min).repeat(N1, 1)*np.random.rand(self.N, N1)
            xs_new_tmp = np.zeros((2*self.N, N1))
            xp_new_tmp = np.zeros((2, N1))

            for j in range(N1):
                r = integrate.ode(self.f)
                r.set_integrator('dopri5')
                r.set_initial_value(np.concatenate([xp_tmp[:, j], xs_tmp[:, i]]), 0.0)
                r.set_f_params(u_tmp[:, j])
                r.integrate(self.del_t)
                xp_new_tmp[:, j] = r.y[0:2]
                xs_new_tmp[:, j] = r.y[2:]

            self.xp[:, i*N1+N1*N2:i*N1+N1+N1*N2] = xp_tmp
            self.xs[:, i*N1+N1*N2:i*N1+N1+N1*N2] = xs_tmp[:, i].reshape(2*self.N, 1).repeat(N1, 1)
            self.u[:, i*N1+N1*N2:i*N1+N1+N1*N2] = u_tmp
            self.xs_new[:, i*N1+N1*N2:i*N1+N1+N1*N2] = xs_new_tmp
            self.xp_new[:, i*N1+N1*N2:i*N1+N1+N1*N2] = xp_new_tmp

        self.xp = self.xp.T
        self.xs = self.xs.T
        self.u = self.u.T
        self.xp_new = self.xp_new.T
        self.xs_new = self.xs_new.T

    def save_data(self, file_name):
        os.makedirs(self.home+self.datasets_path, exist_ok=True)
        pkl.dump([self.xp, self.xs, self.u, self.xp_new, self.xs_new], open(self.home+self.datasets_path+file_name, 'wb'))

    def load_data(self, file_name):
        self.xp, self.xs, self.u, self.xp_new, self.xs_new = pkl.load(open(self.home+self.datasets_path+file_name, 'rb'))

    def set_home(self, home):
        self.home = home

def help():
    print('Usage: python3 MFRMFPSys.py [options]')
    print('-n [--n1]:         number of samples of parent state (100)')
    print('-N [--n2]:         number of samples of swarm state (100)')
    print('-h [--help]:       display this help')
    print('-i [--inputParam]: parameter input file, default is "dataset1_parameters.pkl"')
    print('-I [--inputLimit]: limit input file, default is "dataset1_limits.pkl"')
    print('-o [--output]:     desitination file, default is "dataset1.pkl"')
    print('all files are relative to %s'%MFRMFPSys.home)

if __name__=='__main__':
    N1 = 100
    N2 = 100
    param_file = "dataset1_parameters.pkl"
    limit_file = "dataset1_limits.pkl"
    output_file = "dataset1.pkl"

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'n:N:hi:I:o:', ['n1', 'n1', 'help', 'inputParam', 'inputLimit', 'output'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for [opt, arg] in opts:
        if opt in ['-n', '--n1']:
            N1 = int(arg)
        elif opt in ['-N', '--n2']:
            N2 = int(arg)
        elif opt in ['-h', '--help']:
            help()
            sys.exit(2)
        elif opt in ['-i', '--inputParam']:
            param_file = arg
        elif opt in ['-I', '--inputLimit']:
            limit_file = arg
        elif opt in ['-o', '--output']:
            output_file = arg

    print('generating %d samples...'%(2*N1*N2))

    data = MFRMFPSys(param_file, limit_file)
    data.generate_dataset(N1, N2)
    data.save_data(output_file)

    print('Done!')
