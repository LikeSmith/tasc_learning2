'''
This file handels datasets for the MFRMFP system with varying
swarm sizes

general:
N is swarm size
M is horizon
n is number of samples
'''

import numpy as np
from scipy import integrate
from scipy import linalg
import keras.backend as K
import getopt
import _pickle as pkl
import sys
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import os

class MFRMFPSys2(object):
    home = os.path.dirname(os.path.realpath(__file__)) + '/../../data/MFRMFPSys2/'
    datasets_path = 'datasets/'
    params_path = 'params/'

    def __init__(self, param_file, limits_file, home=None, datasets_path=None, params_path=None):
        if home != None:
            self.home = home
        if datasets_path != None:
            self.datasets_path = datasets_path
        if params_path != None:
            self.params_path = params_path

        self.theta_range, self.omega_range, self.p_range, self.v_range, self.u_range, self.m_range, self.f_l_range, self.f_s_range, self.f_c_range, self.g2_range, self.g3_range, self.g5_range = pkl.load(open(self.home+self.params_path+limits_file, 'rb'))
        self.del_t, self.J, self.g, self.gammas_p= pkl.load(open(self.home+self.params_path+param_file, 'rb'))

    def f_f(self, vel, gammas):
        return gammas[0]*(np.tanh(gammas[1]*vel) - np.tanh(gammas[2]*vel)) + gammas[3]*np.tanh(gammas[4]*vel) + gammas[5]*vel

    def f_f_keras(self, vel, gammas):
        return gammas[:, 0]*(K.tanh(gammas[:, 1]*vel) - K.tanh(gammas[:, 2]*vel)) + gammas[:, 3]*K.tanh(gammas[:, 4]*vel) + gammas[:, 5]*vel

    def f_si(self, xsi, usi, params):
        gammas = params[0:6]
        m = params[6]

        return np.array([xsi[1], (usi[0] - self.f_f(xsi[1], gammas))/m])

    def f_s(self, xs, us, N, params):
        xs_dot = np.zeros(N, 2)

        for i in range(N):
            xs_dot[i, :] = self.f_si(xs[i, :], us[i, :], params[i, :])

        return xs_dot

    def f_si_keras(self, xs, us, params):
        gammas = params[:, 0:6]
        m = params[:, 6]

        p = xs[:, 0]
        v = xs[:, 1]

        p_dot = v
        v_dot = (us[:, 0] - self.f_f_keras(v, gammas))*K.pow(m, -1)

        return K.concatenate([K.expand_dims(p_dot), K.expand_dims(v_dot)])

    def f_s_keras(self, xs, us, N, params):
        xs_dot = []

        for i in range(N):
            xs_dot.append(K.expand_dims(self.f_si_keras(xs[:, i, :], us[:, i, :], params[:, i, :]), axis=1))

        return KK.concatenate(xs_dot, axis=1)

    def f_p(self, xp, xs, N, params):
        m = params[:, 6]
        xp_dot = np.zeros(2)

        J_eff = self.J
        tau = 0.0
        corriolis = 0.0

        for i in range(N):
            J_eff += m[i]*xs[i*2]**2
            tau += m[i]*xs[i*2]
            corriolis += m[i]*xs[i*2]*xs[i*2+1]

        f_f = self.gammas_p[0]*(np.tanh(self.gammas_p[1]*xp[1]) - np.tanh(self.gammas_p[2]*xp[1])) + self.gammas_p[3]*np.tanh(self.gammas_p[4]*xp[1]) + self.gammas_p[5]*xp[1]

        xp_dot[0] = xp[1]
        xp_dot[1] = (-self.g*np.cos(xp[0])*tau - 2*xp[1]*corriolis - f_f)/J_eff

        return xp_dot

    def f_p_keras(self, xp, xs, N, params):
        m = params[:, :, 6]
        theta = xp[:, 0]
        omega = xp[:, 1]
        J_eff = K.ones_like(theta)*self.J
        tau = K.zeros_like(theta)
        corriolis = K.zeros_like(theta)

        for i in range(N):
            J_eff += m[:, i]*K.pow(xs[:, i, 0], 2)
            tau += m[:, i]*xs[:, i, 0]
            corriolis += m[:, i]*xs[:, i, 0]*xs[:, i, 1]

        f_f = self.gammas_p[0]*(K.tanh(self.gammas_p[1]*omega) - K.tanh(self.gammas_p[2]*omega)) + self.gammas_p[3]*K.tanh(self.gammas_p[4]*omega) + self.gammas_p[5]*omega

        theta_dot = omega
        omega_dot = (-self.g*K.cos(xp[:, 0])*tau - 2*xp[:, 1]* corriolis - f_f)/J_eff

        theta_dot = K.expand_dims(theta_dot)
        omega_dot = K.expand_dims(omega_dot)

        return K.concatenate([theta_dot, omega_dot])

    def f(self, t, X, U):
        xp = X[0:2]
        xs = X[2:]

        xs_dot = self.f_s(xs, U)
        xp_dot = self.f_p(xp, xs)

        X_dot = np.zeros(2+self.N*2)
        X_dot[0:2] = xp_dot
        X_dot[2:] = xs_dot

        return X_dot

    def gen_swarm_states(self, N, n):
        xs_min = np.tile(np.array([self.p_range[0], self.v_range[0]]).reshape(1, 1, 2), (n, N, 1))
        xs_max = np.tile(np.array([self.p_range[1], self.v_range[1]]).reshape(1, 1, 2), (n, N, 1))
        return xs_min + (xs_max - xs_min)*np.random.rand(n, N, 2)

    def gen_parent_states(self, n):
        xp_min = np.tile(np.array([self.theta_range[0], self.omega_range[0]]).reshape(1, 2), (n, 1))
        xp_max = np.tile(np.array([self.theta_range[1], self.omega_range[1]]).reshape(1, 2), (n, 1))
        return xp_min + (xp_max - xp_min)*np.random.rand(n, 2)

    def gen_swarm_ctrls(self, N, n):
        u_min = np.tile(np.array([self.u_range[0]]).reshape(1, 1, 1), (n, N, 1))
        u_max = np.tile(np.array([self.u_range[1]]).reshape(1, 1, 1), (n, N, 1))

        return u_min + (u_max - u_min)*np.random.rand(n, N, 1)

    def gen_swarm_params(self, N, n):
        p_min = np.tile(np.array([self.f_s_range[0], self.g2_range[0], self.g3_range[0], self.f_c_range[0], self.g5_range[0], self.f_l_range[0], self.m_range[0]]).reshape(1, 1, 7), (n, N, 1))
        p_max = np.tile(np.array([self.f_s_range[1], self.g2_range[1], self.g3_range[1], self.f_c_range[1], self.g5_range[1], self.f_l_range[1], self.m_range[1]]).reshape(1, 1, 7), (n, N, 1))

        p = p_min + (p_max - p_min)*np.random.rand(n, N, 7)

        p[:, :, 2] = p[:, :, 1] - p[:, :, 2]
        p[:, :, 3] = p[:, :, 0] - p[:, :, 3]

        return p

    def generate_dataset(self, N, n):
        self.xs_0 = self.gen_swarm_states(N, n)
        self.xp_0 = self.gen_parent_states(n)
        self.us = self.gen_swarm_ctrls(N, n)
        self.params = self.gen_swarm_params(N, n)

    def save_dataset(self, filename):
        try:
            os.makedirs(self.home+self.datasets_path, exist_ok=True)
        except TypeError:
            try:
                os.makedirs(self.home+self.datasets_path)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise
        pkl.dump([self.xs_0, self.xp_0, self.us, self.params], open(self.home+self.datasets_path+filename, 'wb'))

    def load_dataset(self, filename):
        [self.xs_0, self.xp_0, self.us, self.params] = pkl.load(open(self.home+self.datasets_path+filename, 'rb'))

    def check_dataset(self, filename):
        return os.path.isfile(self.home+self.datasets_path+filename)

    def autoload(self, N, n):
        dataset_file = self.gen_filename(N, n)
        if self.check_dataset(dataset_file):
            self.load_dataset(dataset_file)
            return False
        else:
            self.generate_dataset(N, n)
            return True

    def gen_filename(self, N, n):
        return 'dataset1_%dRobs_%dsamps.pkl'%(N, n)

    def set_home(self, home):
        self.home = home

def help():
    print('Usage: python3 MFRMFPSys.py [options]')
    print('-n [--n_samples]:  number of samples (10000)')
    print('-N [--n_swarm]:    size of swarm (4)')
    print('-h [--help]:       display this help')
    print('-i [--inputParam]: parameter input file ("dataset1_parameters.pkl")')
    print('-I [--inputLimit]: limit input file ("dataset1_limits.pkl")')
    print('-o [--output]:     desitination file ("dataset1.pkl")')
    print('-H [--home]:       home directory')
    print('-p [--param_path]: parameter directory')
    print('-d [--data_path]:  dataset directory')
    print("all files are relative to '%s' unless otherwise specified"%MFRMFPSys2.home)

if __name__=='__main__':
    n = 10000
    N = 4
    param_file = "dataset1_parameters.pkl"
    limit_file = "dataset1_limits.pkl"
    output_file = "dataset1.pkl"
    home = MFRMFPSys2.home
    param_path = MFRMFPSys2.params_path
    data_path = MFRMFPSys2.datasets_path

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'n:N:hi:I:o:H:p:d:', ['n_samples', 'n_swarm', 'help', 'inputParam', 'inputLimit', 'output', 'home', 'param_path', 'data_path'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for [opt, arg] in opts:
        if opt in ['-n', '--n_samples']:
            n = int(arg)
        elif opt in ['-N', '--n_swarm']:
            N = int(arg)
        elif opt in ['-h', '--help']:
            help()
            sys.exit(0)
        elif opt in ['-i', '--inputParam']:
            param_file = arg
        elif opt in ['-I', '--inputLimit']:
            limit_file = arg
        elif opt in ['-o', '--output']:
            output_file = arg
        elif opt in ['-H', '--home']:
            home = arg
        elif opt in ['-p', '--param_path']:
            param_path = arg
        elif opt in ['-d', '--data_path']:
            data_path = arg

    print('generating samples...')

    data = MFRMFPSys2(param_file, limit_file, home=home, params_path=param_path, datasets_path=data_path)
    data.generate_dataset(N, n)
    data.save_dataset(output_file)

    print('Done!')
