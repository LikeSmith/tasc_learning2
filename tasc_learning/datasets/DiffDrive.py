'''
This handels datasets for a differential drive type robot
'''

import numpy as np
import keras.backend as KK
import getopt
import _pickle as pkl
import sys
import os

class DiffDrive(object):
    home = os.path.dirname(os.path.realpath(__file__)) + '/../../data/DiffDrive/'
    datasets_path = 'datasets/'
    params_path = 'params/'

    def __init__(self, param_file, limits_file, home=None, datasets_path=None, params_path=None):
        if home != None:
            self.home = home
        if datasets_path != None:
            self.datasets_path = datasets_path
        if params_path != None:
            self.params_path = params_path

        self.del_t = pkl.load(open(self.home+self.params_path+param_file, 'rb'))
        self.px_range, self.py_range, self.theta_range, self.l_range, self.v_range = pkl.load(open(self.home+self.params_path+limits_file, 'rb'))

        self.input_limit = False

    def f_cont(self, x, u, p):
        if len(x.shape) == 1:
            x = np.reshape(x, (1, 3))
        if len(u.shape) == 1:
            u = np.reshape(u, (1, 2))
        if len(p.shape) == 1:
            p = np.reshape(p, (1, 1))

        assert u.shape[0] == x.shape[0] == p.shape[0]

        if self.input_limit:
            u = np.max(np.concatenate([np.expand_dims(u, axis=-1), np.ones(u.shape+(1,))*self.v_range[0]], axis=-1), axis=-1)
            u = np.min(np.concatenate([np.expand_dims(u, axis=-1), np.ones(u.shape+(1,))*self.v_range[1]], axis=-1), axis=-1)

        x_dot = np.zeros((x.shape))
        x_dot[:, 0] = 0.5*(u[:, 0] + u[:, 1])*np.cos(x[:, 2])
        x_dot[:, 1] = 0.5*(u[:, 0] + u[:, 1])*np.sin(x[:, 2])
        x_dot[:, 2] = (u[:, 0] - u[:, 1])/p[:, 1]

        return x_dot

    def f_disc(self, x, u, p):
        if len(x.shape) == 1:
            x = np.reshape(x, (1, 3))
        if len(u.shape) == 1:
            u = np.reshape(u, (1, 2))
        if len(p.shape) == 1:
            p = np.reshape(p, (1, 1))

        assert u.shape[0] == x.shape[0] == p.shape[0]

        if self.input_limit:
            u = np.max(np.concatenate([np.expand_dims(u, axis=-1), np.ones(u.shape+(1,))*self.v_range[0]], axis=-1), axis=-1)
            u = np.min(np.concatenate([np.expand_dims(u, axis=-1), np.ones(u.shape+(1,))*self.v_range[1]], axis=-1), axis=-1)

        x_new = np.zeros((x.shape))
        x_new[:, 0] = x[:, 0] + self.del_t*0.5*(u[:, 0] + u[:, 1])*np.cos(x[:, 2])
        x_new[:, 1] = x[:, 1] + self.del_t*0.5*(u[:, 0] + u[:, 1])*np.sin(x[:, 2])
        x_new[:, 2] = x[:, 2] + np.arctan(self.del_t*(u[:, 0] - u[:, 1])/p[:, 0])

        return x_new

    def f_cont_keras(self, args):
        x = args[0]
        u = args[1]
        p = args[2]

        if self.input_limit:
            v_min = KK.constant(self.v_range[0])
            v_max = KK.constant(self.v_range[1])

            u = KK.max(KK.concatenate([KK.expand_dims(u, axis=-1), KK.expand_dims(KK.ones_like(u), axis=-1)*v_min], axis=-1), axis=-1)
            u = KK.min(KK.concatenate([KK.expand_dims(u, axis=-1), KK.expand_dims(KK.ones_like(u), axis=-1)*v_max], axis=-1), axis=-1)

        l = p[:, 0]
        px = x[:, 0]
        py = x[:, 1]
        theta = x[:, 2]
        vl = u[:, 0]
        vr = u[:, 1]

        px_dot = 0.5*(vl + vr)*KK.cos(theta)
        py_dot = 0.5*(vl + vr)*KK.sin(theta)
        theta_dot = (vl - vr)/l

        return KK.concatenate([KK.expand_dims(px_dot), KK.expand_dims(py_dot), KK.expand_dims(theta_dot)], axis=-1)

    def f_disc_keras(self, args):
        x = args[0]
        u = args[1]
        p = args[2]

        if self.input_limit:
            v_min = KK.constant(self.v_range[0])
            v_max = KK.constant(self.v_range[1])

            u = KK.max(KK.concatenate([KK.expand_dims(u, axis=-1), KK.expand_dims(KK.ones_like(u), axis=-1)*v_min], axis=-1), axis=-1)
            u = KK.min(KK.concatenate([KK.expand_dims(u, axis=-1), KK.expand_dims(KK.ones_like(u), axis=-1)*v_max], axis=-1), axis=-1)

        px = x[:, 0]
        py = x[:, 1]
        theta = x[:, 2]
        vl = u[:, 0]
        vr = u[:, 1]
        l = p[:, 0]
        del_t = KK.constant(self.del_t)

        px_new = px + del_t*0.5*(vl + vr)*KK.cos(theta)
        py_new = py + del_t*0.5*(vl + vr)*KK.sin(theta)
        theta_new = theta + del_t*(vl - vr)/l

        return KK.concatenate([KK.expand_dims(px_new), KK.expand_dims(py_new), KK.expand_dims(theta_new)], axis=-1)

    def gen_states(self, n):
        x_min = np.tile(np.array([self.px_range[0], self.py_range[0], self.theta_range[0]]).reshape(1, 3), (n, 1))
        x_max = np.tile(np.array([self.px_range[1], self.py_range[1], self.theta_range[1]]).reshape(1, 3), (n, 1))

        return x_min + (x_max - x_min)*np.random.rand(n, 3)

    def gen_inputs(self, n):
        u_min = np.tile(np.array([self.v_range[0], self.v_range[0]]).reshape(1, 2), (n, 1))
        u_max = np.tile(np.array([self.v_range[1], self.v_range[1]]).reshape(1, 2), (n, 1))

        return u_min + (u_max - u_min)*np.random.rand(n, 2)

    def gen_params(self, n):
        p_min = np.tile(np.array([self.l_range[0]]).reshape(1, 1), (n, 1))
        p_max = np.tile(np.array([self.l_range[0]]).reshape(1, 1), (n, 1))

        return p_min + (p_max - p_min)*np.random.rand(n, 1)

    def generate_dataset(self, n, subsamp_n=None, subsamp_r=0.0):
        self.x_0 = self.gen_states(n)
        self.u = self.gen_inputs(n)
        self.p = self.gen_params(n)

        if subsamp_n is not None:
            x_0_subsamp = 2.0*np.random.rand(subsamp_n, 3) - 1.0
            x_0_subsamp[:, 0:2] *= subsamp_r
            x_0_subsamp[:, 2] *= np.pi
            self.x_0 = np.concatenate((self.x_0, x_0_subsamp), axis=0)
            self.u = np.concatenate((self.u, self.gen_inputs(subsamp_n)), axis=0)
            self.p = np.concatenate((self.p, self.gen_params(subsamp_n)), axis=0)

    def save_dataset(self, filename):
        try:
            os.makedirs(self.home+self.datasets_path, exist_ok=True)
        except TypeError:
            try:
                os.makedirs(self.home+self.datasets_path)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise
        pkl.dump([self.x_0, self.u, self.p], open(self.home+self.datasets_path+filename, 'wb'))

    def load_dataset(self, filename):
        [self.x_0, self.u, self.p] = pkl.load(open(self.home+self.datasets_path+filename, 'rb'))

    def check_dataset(self, filename):
        return os.path.isfile(self.home+self.datasets_path+filename)

    def autoload(self, n, dataset_no, subsamp_n=None, subsamp_r=0.0):
        dataset_file = self.gen_filename(n, dataset_no, subsamp_n, subsamp_r)
        if self.check_dataset(dataset_file):
            self.load_dataset(dataset_file)
            return False
        else:
            self.generate_dataset(n, subsamp_n, subsamp_r)
            return True

    def gen_filename(self, n, dataset_no, subsamp_n=None, subsamp_r=0.0):
        fd = 'dataset%d_%dsamps'%(dataset_no, n)
        if subsamp_n is not None:
            fd += '_%dsubsamps_r%f'%(subsamp_n, subsamp_r)
            fd = fd.replace('.', '-')
        return fd+'.pkl'

    def set_home(self, home):
        self.home = home

    def shuffle(self):
        perm = np.random.permutation(self.x_0.shape[0])
        np.take(self.x_0, perm, axis=0, out=self.x_0)
        np.take(self.u, perm, axis=0, out=self.u)
        np.take(self.p, perm, axis=0, out=self.p)

    def limit_inputs(self):
        self.input_limit = True

    def unlimit_inputs(self):
        self.input_limit = False

    def set_input_limit(self, input_limit):
        self.input_limit = input_limit

def help():
    print('Usage: python3 MFRMFPSys.py [options]')
    print('-n [--n_samples]:  number of samples (10000)')
    print('-h [--help]:       display this help')
    print('-i [--inputParam]: parameter input file ("dataset1_parameters.pkl")')
    print('-I [--inputLimit]: limit input file ("dataset1_limits.pkl")')
    print('-o [--output]:     desitination file ("dataset1.pkl")')
    print('-H [--home]:       home directory')
    print('-p [--param_path]: parameter directory')
    print('-d [--data_path]:  dataset directory')
    print("all files are relative to '%s' unless otherwise specified"%DiffDrive.home)

if __name__=='__main__':
    n = 10000
    param_file = "dataset1_parameters.pkl"
    limit_file = "dataset1_limits.pkl"
    output_file = "dataset1.pkl"
    home = MFRMFPSys2.home
    param_path = MFRMFPSys2.params_path
    data_path = MFRMFPSys2.datasets_path

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'n:hi:I:o:H:p:d:', ['n_samples', 'help', 'inputParam', 'inputLimit', 'output', 'home', 'param_path', 'data_path'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for [opt, arg] in opts:
        if opt in ['-n', '--n_samples']:
            n = int(arg)
        elif opt in ['-h', '--help']:
            help()
            sys.exit(0)
        elif opt in ['-i', '--inputParam']:
            param_file = arg
        elif opt in ['-I', '--inputLimit']:
            limit_file = arg
        elif opt in ['-o', '--output']:
            output_file = arg
        elif opt in ['-H', '--home']:
            home = arg
        elif opt in ['-p', '--param_path']:
            param_path = arg
        elif opt in ['-d', '--data_path']:
            data_path = arg

    print('generating samples...')

    data = DiffDrive(param_file, limit_file, home=home, params_path=param_path, datasets_path=data_path)
    data.generate_dataset(n)
    data.save_dataset(output_file)

    print('Done!')
