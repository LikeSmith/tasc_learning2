'''
Lyapunov Optimal Learning test, prescribed controller
'''

import numpy as np
import matplotlib.pyplot as plt

def sys_dyn(x, u, params):
    if len(x.shape) == 1:
        x = np.reshape(x, (1, 3))
    if len(u.shape) == 1:
        u = np.reshape(u, (1, 2))

    del_t = params['del_t']
    l = params['l']

    assert u.shape[0] == x.shape[0]

    x_new = np.zeros(x.shape)

    x_new[:, 0] = x[:, 0] + del_t*0.5*(u[:, 0] + u[:, 1])*np.cos(x[:, 2])
    x_new[:, 1] = x[:, 1] + del_t*0.5*(u[:, 0] + u[:, 1])*np.sin(x[:, 2])
    x_new[:, 2] = x[:, 2] + np.arctan(del_t/l*(u[:, 1] - u[:, 0]))

    return x_new

def ctrl_policy(x, gains):
    if len(x.shape) == 1:
        x = np.reshape(x, (1, 3))

    K_p = gains['K_p']
    K_theta = gains['K_theta']

    u = np.zeros((x.shape[0], 2))

    del_v = K_theta*(x[:, 0]*np.sin(x[:, 2]) - x[:, 1]*np.cos(x[:, 2]))
    ave_v = -K_p*(x[:, 0]*np.cos(x[:, 2]) + x[:, 1]*np.sin(x[:, 1]))

    u[:, 0] = 0.5*del_v + ave_v
    u[:, 1] = -0.5*del_v + ave_v

    return u

if __name__ == '__main__':
    grid_lim = 1
    grid_size = 0.5

    side_length = int(2*grid_lim/grid_size)+1

    x_0 = np.zeros((side_length**2, 3))

    for i in range(side_length):
        for j in range(side_length):
            x_0[i*side_length+j, 0] = i*grid_size - grid_lim
            x_0[i*side_length+j, 1] = j*grid_size - grid_lim

    #x_0 = np.array([1, 1, 0])

    del_t = 0.01
    t_f = 100
    l = 0.25

    K_p = 1.0
    K_theta = 1.0

    params = {'del_t':del_t, 'l':l}
    gains = {'K_p':K_p, 'K_theta':K_theta}

    if len(x_0.shape) == 1:
        x_0 = np.reshape(x_0, (1, 3))

    t = np.arange(0.0, t_f, del_t)
    x = np.zeros((x_0.shape[0], t.shape[0], 3))
    u = np.zeros((x_0.shape[0], t.shape[0], 2))

    x[:, 0, :] = x_0

    for i in range(t.shape[0]-1):
        u[:, i, :] = ctrl_policy(x[:, i, :], gains)
        x[:, i+1, :] = sys_dyn(x[:, i, :], u[:, i, :], params)

    plt.figure()
    plt.plot(t, x[:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('x pos (m)')

    plt.figure()
    plt.plot(t, x[:, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('y pos (m)')

    plt.figure()
    plt.plot(t, x[:, :, 2].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')

    plt.figure()
    plt.plot(t, u[:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('left vel (m/s)')

    plt.figure()
    plt.plot(t, u[:, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('right vel (m)')

    plt.figure()
    plt.plot(x[:, :, 0].T, x[:, :, 1].T)
    plt.xlabel('y pos (m)')
    plt.ylabel('x pos (m)')

    plt.show()
