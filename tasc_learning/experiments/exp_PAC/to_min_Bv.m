function a = to_min_Bv(lambda, verts, d2)
	n = size(verts, 2);
	x = zeros(n, 1);
	
	for i = 1:n+1
		x = x + lambda(i)*verts(i, :)';
	end
	
	d2_arr = zeros(n, n, n);
	
	for i = 1:n
		d2_arr(i, :, :) = abs(d2{i}(x));
	end
	
	a = -max(reshape(d2_arr, [1, n^3]));
end