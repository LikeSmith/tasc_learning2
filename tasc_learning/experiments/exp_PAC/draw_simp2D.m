function draw_simp2D(verts, color)
	if nargin() == 1
		color = '';
	end
	
	x = [verts(1, 1), verts(2, 1), verts(3, 1), verts(1, 1)];
	y = [verts(1, 2), verts(2, 2), verts(3, 2), verts(1, 2)];
	
	plot(x, y, color);
end