function [verts, simps] = subsamp_barycent(verts)
	n = size(verts, 2);
	[verts, simps, ~] = subsamp_barycent_helper(verts, [], 1:n+1, 1, 1, []);
end

function [verts, simps, ind] = subsamp_barycent_helper(verts, simps, face, ind, layer, sofar)
	n = size(verts, 2);
	
	if layer == n+1
		simps(ind, :) = [face, sofar];
		ind = ind + 1;
	else
		new_vert = mean(verts(face, :));
		new_vert_i = find_vert(new_vert, verts);
		if new_vert_i == 0
			verts(end+1, :) = new_vert;
			new_vert_i = size(verts, 1);
		end
		
		subfaces = nchoosek(face, n-layer+1);
		
		for i = 1:size(subfaces, 1)
			[verts, simps, ind] = subsamp_barycent_helper(verts, simps, subfaces(i, :), ind, layer+1, [sofar, new_vert_i]);
		end
	end
end

function ind = find_vert(vert, verts)
	ind = 0;
	for i = 1:size(verts, 1)
		if norm(vert - verts(i, :)) == 0
			ind = i;
			break;
		end
	end
end