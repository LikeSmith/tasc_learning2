function a = to_min_Gv(lambda, verts, d1)
	n = size(verts, 2);
	x = zeros(n, 1);
	
	for i = 1:n+1
		x = x + lambda(i)*verts(i, :)';
	end
	
	a = -max(reshape(abs(d1(x)), [1, n^2]));
end