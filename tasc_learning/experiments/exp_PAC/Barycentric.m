%
% Barycentric subdivision
%

clear all
close all

verts = [0 0 0; 1 1 0; 1 -1 0; 0.5, 0, 1];
simps = [1, 2, 3, 4];

for i = 1:2
	n_old = size(simps, 1);
	for j = 1:size(simps, 1)
		[new_verts, new_simps] = subsamp_barycent(verts(simps(j, :), :));
		inds = find_verts(new_verts, verts);
		for k = 1:length(inds)
			if inds(k) == 0
				verts(end+1, :) = new_verts(k, :);
				inds(k) = size(verts, 1);
			end
		end
		
		for k = 1:size(new_simps, 1)
			simps(end+1, :) = inds(new_simps(k, :));
		end
	end
	simps(1:n_old, :) = [];
end

figure();
hold on;
grid on;

plot3(verts(:, 1), verts(:, 2), verts(:, 3), 'b*');
draw_simp3D(verts(1:4, :), 'b');
for i = 1:size(simps, 1)
	draw_simp3D(verts(simps(i, :), :), 'r');
end

verts = [0.5 0; -0.5 0; 0 1];
simps = [1, 2, 3];

for i = 1:4
	n_old = size(simps, 1);
	for j = 1:size(simps, 1)
		[new_verts, new_simps] = subsamp_barycent(verts(simps(j, :), :));
		inds = find_verts(new_verts, verts);
		for k = 1:length(inds)
			if inds(k) == 0
				verts(end+1, :) = new_verts(k, :);
				inds(k) = size(verts, 1);
			end
		end
		
		for k = 1:size(new_simps, 1)
			simps(end+1, :) = inds(new_simps(k, :));
		end
	end
	simps(1:n_old, :) = [];
end

figure();
hold on;
grid on;

plot(verts(:, 1), verts(:, 2), 'b*');
for i = 1:size(simps, 1)
	draw_simp2D(verts(simps(i, :), :), 'b');
end
