function [g_f, d1_f, d2_f] = get_dirs(g_sym, x)
	n = size(x, 1);
	
	g_f = matlabFunction(g_sym, 'vars', {x});
	d1 = jacobian(g_sym, x);
	d2_f = {};
	for i = 1:n
		d2 = jacobian(d1(:, i), x);
		d2_f{i} = matlabFunction(d2, 'vars', {x});
	end
	
	d1_f = matlabFunction(d1, 'vars', {x});
end