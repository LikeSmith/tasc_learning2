function inds = find_verts(tofind, verts)
	inds = zeros(size(tofind, 1), 1);
	for i = 1:size(tofind, 1)
		for j = 1:size(verts, 1)
			if norm(tofind(i, :) - verts(j, :)) == 0
				inds(i) = j;
				break;
			end
		end
	end
end