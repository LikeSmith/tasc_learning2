function verts = NDGrid(varargin)
	if length(varargin) == 1
		varargin = varargin{1};
	end
	
	n = length(varargin);
	shape = zeros(1, n);
	
	for i = 1:n
		shape(i) = length(varargin{i});
	end
	
	verts = zeros(prod(shape), n);
	
	for i = 1:n
		if i < n
			reps1 = prod(shape(i+1:end));
		else
			reps1= 1;
		end
		
		if  i > 1
			reps2 = prod(shape(1:i-1));
		else
			reps2 = 1;
		end
		
		for j = 1:reps2
			for k = 1:shape(i)
				for l = 1:reps1
					verts((j-1)*reps1*shape(i)+(k-1)*reps1+l, i) = varargin{i}(k);
				end
			end
		end
	end
end