%
% Pac example
%

clear all
close all

% define system symbolicaly
n = 2;
syms x1 x2 real;
x = [x1; x2];
g_sym = [0.5*x1 + x1^2 - x2^2; -0.5*x2 + x1^2];

% set triangulation parameters
K = 0;
F_size = 0.033;
N_I = 2;
N_O = 10;
N_D = 12;

% set lyapunov variables
Q = eye(n);

[g, d1, d2] = get_dirs(g_sym, x);

A_eq = d1(zeros(n, 1));

P = dlyap(A_eq, Q);

[verts, simps, F, I, O] = triangulate(2, K, F_size, N_I, N_O, N_D, 1);

I_F = I;
O_F = O;

for i = 1:length(F)
	I_F(find(I_F == F(i))) = [];
	O_F(find(O_F == F(i))) = [];
end

figure
hold on

for i = 1:size(verts, 1)
	plot(verts(i, 1), verts(i, 2), 'b*')
end
for i = 1:size(simps, 1)
	draw_simp2D(verts(simps(i, :), :), 'b');
end
for i = 1:length(O)
	draw_simp2D(verts(simps(O(i), :), :), 'r');
end
for i = 1:length(I)
	draw_simp2D(verts(simps(I(i), :), :), 'g');
end
for i = 1:length(F)
	draw_simp2D(verts(simps(F(i), :), :), 'K');
end

shape_mat = zeros(size(simps, 1), 2, 2);

for i = 1:size(simps, 1)
	for j = 1:n
		shape_mat(i, j, :) = verts(simps(i, j+1), :) - verts(simps(i, 1), :);
	end
end

% calculate values
y_simp = zeros(size(verts, 1), 1);
y_bary_coords = zeros(size(verts, 1), n+1);

f = waitbar(0, 'finding destination simpicies');
for i = 1:size(verts, 1)
	y = g(verts(i, :)')';
	[cont_simps, bary_coords] = get_bary_coord(verts, simps, y);
	
	y_simp(i) = cont_simps(1);
	y_bary_coords(i, :) = bary_coords(1, :);
	waitbar(i/size(verts, 1), f, sprintf('finding destination simplicies (%d/%d)', i, size(verts, 1)));
end
close(f);

alpha = sqrt(min(eig(Q))/max(eig(P)))/8;
H_max = (max(eig(P))/sqrt(min(eig(P))))*(1 + max(eig(P))/min(eig(P)));
h_v = zeros(size(simps, 1), 1);
B_v = zeros(size(simps, 1), 1);
G_v = zeros(size(simps, 1), 1);

A_leq = -eye(n+1);
b_leq = zeros(n+1, 1);
A_eq = ones(1, n+1);
b_eq = 1;
lambda0 = ones(n+1, 1)*1/(n+1);

f = waitbar(0, 'calculating bounding constants');
for i = 1:size(simps, 1)
	inds = nchoosek(1:n+1, 2);
	for j = 1:size(inds, 1)
		new_h_v = norm(verts(simps(i, inds(j, 1)), :) - verts(simps(i, inds(j, 2)), :));
		if new_h_v > h_v(i)
			h_v(i) = new_h_v;
		end
	end
	
	to_min = @(lambda)to_min_Bv(lambda, verts(simps(i, :), :), d2);
	lambda_Bv = fmincon(to_min, lambda0, A_leq, b_leq, A_eq, b_eq);
	B_v(i) = -n*to_min(lambda_Bv);
	
	to_min = @(lambda)to_min_Gv(lambda, verts(simps(i, :), :), d1);
	lambda_Gv = fmincon(to_min, lambda0, A_leq, b_leq, A_eq, b_eq);
	G_v(i) = -n*to_min(lambda_Gv);
	
	waitbar(i/size(simps, 1), f, sprintf('Calculating Bounding Constants (%d/%d)', i, size(simps, 1)));
end
close(f);

h_IF = max(G_v(I_F));
G_F = max(G_v(F));
h_dfP = 0;

F_border = unique(reshape(simps(F, 2:end), [n*length(F), 1]));
perms = nchoosek(F_border, 2);
for i = 1:size(perms, 1)
	new_val = (verts(perms(i, 1), :) - verts(perms(i, 2)))*P*(verts(perms(i, 1), :) - verts(perms(i, 2)))';
	
	if new_val > h_dfP
		h_dfP = new_val;
	end
end

E_f = G_F*max([H_max*h_IF^2/F_size, 2*h_dfP]);

% setup linear constraints
A = [];
b = [];
A_eq = [];
b_eq = [];
n_verts = size(verts, 1);
n_simps = size(simps, 1);

I_verts = unique(reshape(simps(I, :), [length(I)*(n+1), 1]));

f = waitbar(0, 'setting up constants I and II');
for i = 1:n_verts
	if isempty(find(I_verts == i))
		A_eq(end+1, :) = zeros(1, n_verts+n_simps*n+1);
		A_eq(end, i) = 1;
		b_eq(end+1) = verts(i, :)*P*verts(i, :)';
	else
		A(end+1, :) = zeros(1, n_verts+n_simps*n+1);
		A(end, i) = -1;
		b(end+1) = -verts(i, :)*P*verts(i, :)';
	end
	waitbar(i/n_verts, f, sprintf('setting up constants I and II (%d/%d)', i, n_verts));
end
close(f);

f = waitbar(0, 'setting up constraints III');
for i = 1:n_simps
	X_inv = inv(squeeze(shape_mat(i, :, :)));
	
	for j = 1:n
		A(end+1, :) = zeros(1, n_verts+n_simps*n+1);
		del_v_pos = size(A, 1);
		A(end+1, :) = zeros(1, n_verts+n_simps*n+1);
		del_v_neg = size(A, 1);
		
		A(del_v_pos, n_verts+(i-1)*n+j) = 1;
		A(del_v_neg, n_verts+(i-1)*n+j) = 1;
		
		b(end+1) = 0;
		b(end+1) = 0;
		
		for k = 1:n
			A(del_v_pos, simps(i, 1)) = A(del_v_pos, simps(i, 1)) - X_inv(j, k);
			A(del_v_neg, simps(i, 1)) = A(del_v_neg, simps(i, 1)) + X_inv(j, k);
			A(del_v_pos, simps(i, k+1)) = X_inv(j, k);
			A(del_v_neg, simps(i, k+1)) = -X_inv(j, k);
		end
		
		waitbar(((i-1)*n+j)/(n_simps*n), f, sprintf('setting up constraints III (%d/%d)', (i-1)*n+j, n_simps*n));
	end
	
	A(end+1, :) = zeros(1, n_verts+n_simps*n+1);
	A(end, (n_verts+(i-1)*n+1):(n_verts+i*n)) = ones(1, n);
	A(end, end) = -1;
	b(end+1) = 0;
end
close(f);

f = waitbar(0, 'setting up constraints IVa');
for i = 1:length(F)
	for j = 1:n
		A(end+1, :) = zeros(1, n_verts+n_simps*n+1);
		for k = 1:n+1
			A(end, simps(y_simp(simps(F(i), j+1)), k)) = y_bary_coords(simps(F(i), j+1), k);
		end
		A(end, simps(F(i), j+1)) = -1;
		A(end, end) = norm(verts(simps(F(i), j+1), :))*B_v(F(i))*h_v(F(i));
		b(end+1) = E_f - alpha*verts(simps(F(i), j+1), :)*Q*verts(simps(F(i), j+1), :)';
		
		waitbar(((i-1)*(n+1)+j)/(length(F)*(n+1)), f, sprintf('setting up constraints IVa (%d/%d)', (i-1)*(n+1)+j, length(F)*(n+1)));
	end
end
close(f);

f = waitbar(0, 'setting up constraints IVb');
for i = 1:length(O_F)
	for j = 1:n+1
		A(end+1, :) = zeros(1, n_verts+n_simps*n+1);
		for k = 1:n+1
			A(end, simps(y_simp(simps(O_F(i), j)), k)) = y_bary_coords(simps(O_F(i), j), k);
		end
		A(end, simps(O_F(i), j)) = -1;
		A(end, end) = G_v(O_F(i))*h_v(O_F(i));
		b(end+1) = -alpha*verts(simps(O_F(i), j), :)*Q*verts(simps(O_F(i), j), :)';
		
		waitbar(((i-1)*n+j)/(length(O_F)*n), f, sprintf('setting up constraints IVb (%d/%d)', (i-1)*n+j, length(O_F)*n));
	end
end
close(f);

%%

sol = linprog(zeros(n_verts+n_simps*n+1, 1), A, b, A_eq, b_eq);

figure()
plot3(verts(:, 1), verts(:, 2), sol(1:size(verts, 1)), 'b*');
