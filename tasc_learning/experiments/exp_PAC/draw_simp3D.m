function draw_simp3D(verts, color)
	if nargin() == 1
		color = '';
	end
	
	edges = nchoosek(1:4, 2);
	
	for i = 1:size(edges, 1)
		plot3(verts(edges(i, :), 1), verts(edges(i, :), 2), verts(edges(i, :), 3), color);
	end
end