function [simps_cont, bary_coord] = get_bary_coord(verts, simps, x)
	simps_cont = [];
	bary_coord = [];
	n = size(verts, 2);
	eps = 1e-10;
	
	for i = 1:size(simps, 1)
		X = zeros(n, n);
		for j = 1:n
			X(:, j) = (verts(simps(i, j+1), :) - verts(simps(i, 1), :))';
		end
		total_vol = abs(det(X)/factorial(n));
		
		sum_vol = 0;
		
		perms = nchoosek(1:n+1, n);
		
		for j = 1:size(perms, 1)
			for k = 1:n
				X(:, k) = (verts(simps(i, perms(j, k)), :) - x);
			end
			sum_vol = sum_vol + abs(det(X)/factorial(n));
		end
		
		if abs(total_vol - sum_vol) < eps
			A = [verts(simps(i, :), :)'; ones(1, n+1)];
			b = [x 1]';
			bc = A\b;
			if min(bc) >= 0
				simps_cont(end+1) = i;
				bary_coord(end+1, :) = (A\b)';
			end
		end
	end
end