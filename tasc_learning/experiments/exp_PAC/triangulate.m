function [verts, simps, F, I, O] = triangulate(n, K, F_size, N_I, N_O, N_D, use_waitbar)
	if nargin() == 6
		use_waitbar = 0;
	end
	
	N = N_D*2^K;
    N_F = 2^K;
	N_I = N_I*2^K;
	N_O = N_O*2^K;
	
	for i = 1:n
		vals1{i} = -N:N;
		vals2{i} = -(N_F-1):(N_F-1);
		vals3{i} = -N_F:N_F; 
	end
	
	verts = NDGrid(vals1);
	verts_F_border = NDGrid(vals3);
	verts_F = NDGrid(vals2);
    
	toremove = find_verts(verts_F, verts);
	verts(toremove, :) = [];
	toremove = find_verts(verts_F, verts_F_border);
	verts_F_border(toremove, :) = [];
	
	verts(end+1, :) = zeros(1, n);
	i_0 = size(verts, 1);
	
	basis = eye(n);
	S_n = perms(1:n);
	
	ind = 1;
	ind_F = 1;
	ind_I = 1;
	ind_O = 1;
	for i = 1:n
		combs = nchoosek(1:n, i);
		for j = 1:size(combs, 1)
			J{ind} = combs(j, :);
			ind = ind + 1;
		end
	end
	
	simps = zeros(1, n+1);
	F = zeros(1, 1);
	I = zeros(1, 1);
	O = zeros(1, 1);
	
	ind = 1;
	
	if use_waitbar == 1
		f = waitbar(0, 'generating triangulation');
	end
	
	for i = 1:size(verts, 1)
		z = verts(i, :);
		for j = 1:size(S_n, 1)
			sigma = S_n(j, :);
			for k = 1:size(J, 1)
				simp_verts = zeros(n+1, n);
				for l = 1:n+1
					y = zeros(1, n);
					for m = 2:l
						y = y + basis(sigma(m-1), :);
					end
					simp_verts(l, :) = R_J(z+y, J{k});
				end
				
				inds = find_verts(simp_verts, verts);
				inds_border = find_verts(simp_verts, verts_F_border);
				
				if ~any(inds == 0) && sum(inds==i_0)==0 && sum(~(inds_border==0)) ~= n+1
					if sum(~(inds_border==0)) == n
						inds_fan = inds;
						inds_fan(find(inds_border==0)) = i_0;
						simps(ind, :) = inds_fan;
						F(ind_F) = ind;
						I(ind_I) = ind;
						O(ind_O) = ind;
						ind = ind + 1;
						ind_F = ind_F + 1;
						ind_I = ind_I + 1;
						ind_O = ind_O + 1;
					end
					
					simps(ind, : ) = inds;
					
					if max(max(abs(verts(inds, :)))) <= N_I
						I(ind_I) = ind;
						ind_I = ind_I + 1;
					end
					if max(max(abs(verts(inds, :)))) <= N_O
						O(ind_O) = ind;
						ind_O = ind_O + 1;
					end
					
					ind = ind + 1;
				end
				
				if use_waitbar == 1
					item = (i-1)*size(S_n, 1)*size(J, 1) + (j-1)*size(J, 1) + k;
					count = size(verts, 1)*size(S_n, 1)*size(J, 1);
					msg = sprintf('generating triangulation: %i /%i', [item, count]);
					waitbar(item/count, f, msg);
				end
			end
		end
	end
	
	if use_waitbar == 1
		close(f);
	end
	
	for i = 1:size(simps, 1)
		verts_s = verts(simps(i, :), :);
		norms = vecnorm(verts_s');
		sort_i = zeros(1, n+1);
		lockout = zeros(1, n+1);
		
		for j = 1:n+1
			search_i = find(lockout==0);
			smallest_i = search_i(1);
			for k = 2:length(search_i)
				if norms(search_i(k)) < norms(smallest_i)
					smallest_i = search_i(k);
				end
			end
			
			sort_i(j) = simps(i, smallest_i);
			lockout(smallest_i) = 1;
		end
		
		simps(i, :) = sort_i;
	end
	verts = verts * F_size/2^K;
end

function y = R_J(x, J)
	n = length(x);
	y = zeros(size(x));
	basis = eye(n);
	
	for i = 1:n
		if ismember(i, J)
			mult = -1;
		else
			mult = 1;
		end
		y = y + x(i)*basis(i, :)*mult;
	end
end
