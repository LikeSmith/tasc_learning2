#!/bin/bash

# set output and error output filenames
#SBATCH -o outputs/train_tasc_MFRMFP2_%j.out
#SBATCH -e outputs/train_tasc_MFRMFP2_%j.err

# email me when job starts and stops
#SBATCH --mail-type=ALL
#SBATCH --mail-user=crandallk@gwu.edu

# set up which queue should be joined
#SBATCH -N 1
#SBATCH -p defq

# set the correct directory
#SBATCH -D /home/crandallk/TASC_Learning2/tasc_learning/experiments/exp_tasc_modular

# name job
#SBATCH -J train_tasc_MFRMFP2

# set time limit
#SBATCH -t 14-00:00:00

# load necesary modules
module load anaconda

# setup environmental variables
export PYTHONPATH=/home/crandallk/TASC_Learning2:$PYTHONPATH

# generate dataset
# python3 ../../datasets/MFRMFPSys2.py -n 10000 -N 4 -i dataset1_params.pkl -I dataset1_limits.pkl -o dataset1_4Robs_10000samps.pkl

# run task
#THEANO_FLAGS=optimizer=fast_compile,optimizer_including=fusion python3 train_tasc_modular.py
python3 train_tasc_modular.py -L -e 1
python3 test_tasc_modular.py -e 1 -t 1 -S -T 10.0
