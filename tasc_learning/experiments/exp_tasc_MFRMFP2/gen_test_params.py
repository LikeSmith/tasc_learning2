'''
Generates parameters for experiment
'''

import numpy as np
import os
import errno

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

test_no = 1

swarm_size = 4
del_t = 0.1
xp_0 = np.array([[0.1, 0]])
xs_0 = np.array([[[0.25, 0.0], [-0.25, 0.0], [0.25, 0.0], [-0.25, 0.0]]])
ps = -1

test_params = [xp_0, xs_0, swarm_size, ps, del_t]

try:
    os.makedirs('params/', exist_ok=True)
except TypeError:
    try:
        os.makedirs('params/')
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

pkl.dump(test_params, open('params/test%d_params.pkl'%(test_no,), 'wb'))
