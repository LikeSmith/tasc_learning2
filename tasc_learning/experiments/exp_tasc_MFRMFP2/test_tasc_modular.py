'''
Test the trained networks
'''

import numpy as np
from tasc_learning.models import tasc_modular, analytic_model, mlp, abstract_dynamics
import tasc_learning.datasets.MFRMFPSys2 as data
import matplotlib.pyplot as plt
import matplotlib
import os
import sys
import getopt

def help():
    print('Usage: python3 test_tasc_modular.py [options]')
    print('tests trained tasc_modular networks on the MFRMFP2 system')
    print('Options:')
    print('-r [--res_dir]:    results direcotry (../../../data/MFRMFPSys2/results)')
    print('-d [--dataset_no]: dataset number (1)')
    print('-e [--exp_no]:     experiment number (1)')
    print('-t [--test_no]:    test number (1)')
    print('-p [--for_print]:  format figures for print (don\'t use funny fonts)')
    print('-S [--save_figs]:  save figures')
    print('-s [--show_figs]:  show figures')
    print('-T [--sim_time]:   time for simulation (10)')
    print('-h [--help]:       display this help')

if __name__ == '__main__':
    # input/output info
    results_dir = '../../../data/MFRMFPSys2/results/'
    dataset_no = 1
    exp_no = 1
    test_no = 1
    for_print = False
    save_figs = False
    show_figs = False

    # Simulation parameters
    xp_0 = np.array([0.1, 0])
    swarm_size = 4
    xs_0 = np.array([[0.25, -0.25, 0.25, -0.25], [0.0, 0.0, 0.0, 0.0]])
    ps = -1
    tf = 10.0
    del_t = 0.1

    # handle command line options
    try:
        opts, args, = getopt.getopt(sys.argv[1:], 'r:d:e:t:pSsT:h', ['res_dir=', 'dataset_no=', 'exp_no', 'test_no=', 'for_print', 'save_figs', 'show_figs', 'sim_time=', 'help'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for [opt, arg] in opts:
        if opt in ['-r', '--res_dir']:
            results_dir = arg
        elif opt in ['-d', '--dataset_no']:
            dataset_no = int(arg)
        elif opt in ['-e', '--exp_no']:
            exp_no = int(arg)
        elif opt in ['-t', '--test_no']:
            test_no = int(arg)
        elif opt in ['-p', '--for_print']:
            for_print = True
        elif opt in ['-S', '--save_figs']:
            save_figs = True
        elif opt in ['-s', '--show_figs']:
            show_figs = True
        elif opt in ['-T', '--sim_time']:
            tf = float(arg)
        elif opt in ['-h', '--help']:
            help()
            sys.exit(0)

    # generate file names
    parent_ctrl_file = 'MFRMFP_dataset%d_parent_ctrl_mdl%d.h5'%(dataset_no, exp_no)
    parent_ctrl_param = 'MFRMFP_dataset%d_arent_ctrl_params%d.pkl'%(dataset_no, exp_no)
    swarm_ctrl_file = 'MFRMFP_dataset%d_swarm_ctrl_mdl%d.h5'%(dataset_no, exp_no)
    swarm_ctrl_param = 'MFRMFP_dataset%d_swarm_ctrl_params%d.pkl'%(dataset_no, exp_no)
    abs_enc_file = 'MFRMFP_dataset%d_abs_enc_mdl%d.h5'%(dataset_no, exp_no)
    abs_enc_param = 'MFRMFP_dataset%d_abs_enc_params%d.pkl'%(dataset_no, exp_no)
    abs_dyn_file = 'MFRMFP_dataset%d_abs_dyn_mdl%d.h5'%(dataset_no, exp_no)
    abs_dyn_param = 'MFRMFP_dataset%d_abs_dyn_params%d.pkl'%(dataset_no, exp_no)
    aux_enc_file = 'MFRMFP_dataset%d_aux_enc_mdl%d.h5'%(dataset_no, exp_no)
    aux_enc_param = 'MFRMFP_dataset%d_aux_enc_params%d.pkl'%(dataset_no, exp_no)
    xp_fig = 'MFRMFP_dataset%d_exp%d_test%d_parent_state_fig.png'%(dataset_no, exp_no, test_no)
    pos_s_fig = 'MFRMFP_dataset%d_exp%d_test%d_swarm_pos_fig.png'%(dataset_no, exp_no, test_no)
    vel_s_fig = 'MFRMFP_dataset%d_exp%d_test%d_swarm_vel_fig.png'%(dataset_no, exp_no, test_no)
    abs_fig = 'MFRMFP_dataset%d_exp%d_test%d_abs_state_fig.png'%(dataset_no, exp_no, test_no)
    xp_prd_fig = 'MFRMFP_dataset%d_exp%d_test%d_parent_prediction_fig.png'%(dataset_no, exp_no, test_no)

    # load all necessary models
    abs_enc = tasc_modular.Abstract_Contrib(param_file=abs_enc_param, net_file=abs_enc_file, home=results_dir)
    aux_enc = tasc_modular.Abstract_Contrib(param_file=aux_enc_param, net_file=aux_enc_file, home=results_dir)
    parent_ctrl = mlp.MLP(param_file=parent_ctrl_param, net_file=parent_ctrl_file, home=results_dir)
    swarm_ctrl = tasc_modular.Node_Controller(param_file=swarm_ctrl_param, net_file=swarm_ctrl_file, home=results_dir)
    abs_dyn = abstract_dynamics.Abstract_Dynamics(param_file=abs_dyn_param, net_file=abs_dyn_file, home=results_dir)

    abs_size = abs_enc.abs_size
    aux_size = aux_enc.abs_size
    input_size = swarm_ctrl.swarm_input
    xp_size = parent_ctrl.input_size
    xs_size = swarm_ctrl.swarm_state

    # load datasets
    training_params = 'dataset%d_params.pkl'%(dataset_no,)
    training_limits = 'dataset%d_limits.pkl'%(dataset_no,)
    data_manager = data.MFRMFPSys2(training_params, training_limits)

    # set up forward dynamics
    forward_dyn = analytic_model.Analytic_Model_Parent_Swarm(
        n_parent_states=xp_size,
        n_swarm_states=xs_size,
        n_swarm_inputs=input_size,
        n_swarm_params=7,
        n_swarm_members=swarm_size,
        cont_parent_fun_keras=data_manager.f_p_keras,
        cont_parent_fun=data_manager.f_p,
        cont_swarm_fun_keras=data_manager.f_s_keras,
        cont_swarm_fun=data_manager.f_s,
        method='RK4',
        del_t=del_t,
        name='MFRMFP_analytic'
        )

    if ps == -1:
        ps = data_manager.gen_swarm_params(swarm_size, 1).reshape(swarm_size, 7)

    # setup Simulation
    t = np.arange(0, tf, del_t)
    n = t.shape[0]
    xp_cur = np.zeros((xp_size, n))
    xp_des = np.zeros((xp_size, n))
    xp_prd = np.zeros((xp_size, n))
    xs = np.zeros((xs_size, swarm_size, n))
    a_cur = np.zeros((abs_size, n))
    a_des = np.zeros((abs_size, n))
    a_aux = np.zeros((aux_size, n))
    u = np.zeros((input_size, swarm_size, n))

    xp_cur[:, 0] = xp_0
    xs[:, :, 0] = xs_0

    for i in range(n):
        # calc abstract states
        a_cur[:, i] = np.sum(abs_enc.eval(xs[:, :, i], ps), 1)
        a_aux[:, i] = np.sum(aux_enc.eval(xs[:, :, i], ps), 1)

        # calc desired abstract state
        a_des[:, i] = parent_ctrl.eval(xp_cur[:, i])

        # calc swarm inputs
        u[:, :, i] = swarm_ctrl.eval(xs[:, :, i], ps,
            a_cur[:, i].reshape(abs_size, 1).repeat(swarm_size, 1),
            a_des[:, i].reshape(abs_size, 1).repeat(swarm_size, 1),
            a_aux[:, i].reshape(aux_size, 1).repeat(swarm_size, 1))

        # predict new parent state
        xp_prd[:, i] = abs_dyn.eval(xp_cur[:, i], a_cur[:, i], u[:, :, i].reshape(input_size*swarm_size))

        if i < n-1:
            # calculate forward dynamics
            xp_1, xs_1 = forward_dyn.eval(xp_cur[:, i], xs[:, :, i].T.reshape(xs_size*swarm_size), u[:, :, i].T.reshape(input_size*swarm_size), ps)

            xp_cur[:, i+1] = xp_1
            xs[:, :, i+1] = xs_1.reshape(swarm_size, xs_size).T

    # plot results
    if for_print:
        matplotlib.rcParams['pdf.fonttype'] = 42
        matplotlib.rcParams['ps.fonttype'] = 42
    if save_figs:
        os.makedirs(results_dir+'figs/', exist_ok=True)

    abs_legend = []
    for i in range(abs_size):
        abs_legend.append(r'$a_%d$'%(i,))

    plt.figure()
    plt.plot(t, xp_cur.T)
    plt.gca().set_prop_cycle(None)
    plt.plot(t, xp_des.T, ':')
    plt.xlabel('time (s)')
    plt.legend([r'$\theta$ (rad)', r'$\dot{\theta}$ (rad/s)'])

    if save_figs:
        plt.savefig(results_dir+'figs/'+xp_fig)

    plt.figure()
    plt.plot(t, xs[0, :, :].T)
    plt.xlabel('time (s)')
    plt.ylabel('robot positions (m)')

    if save_figs:
        plt.savefig(results_dir+'figs/'+pos_s_fig)

    plt.figure()
    plt.plot(t, xs[1, :, :].T)
    plt.xlabel('time (s)')
    plt.ylabel('robot velocities (m)')

    if save_figs:
        plt.savefig(results_dir+'figs/'+vel_s_fig)

    plt.figure()
    plt.plot(t, a_cur.T)
    plt.gca().set_prop_cycle(None)
    plt.plot(t, a_des.T, ':')
    plt.xlabel('time (s)')
    plt.legend(abs_legend)

    if save_figs:
        plt.savefig(results_dir+'figs/'+abs_fig)

    plt.figure()
    plt.plot(t, xp_prd.T)
    plt.gca().set_prop_cycle(None)
    plt.plot(t, xp_cur.T, ':')
    plt.xlabel('time (s)')
    plt.legend([r'$\theta$ (rad)', r'$\dot{\theta}$ (rad/s)', r'$\theta_{prd}$ (rad)', r'$\dot{\theta}_{prd}$ (rad/s)'])

    if save_figs:
        plt.savefig(results_dir+'figs/'+xp_prd_fig)

    if show_figs:
        plt.show()
