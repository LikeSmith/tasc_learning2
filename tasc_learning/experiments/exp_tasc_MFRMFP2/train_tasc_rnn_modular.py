'''
This script will train the recurrent version of the full tasc
learner

options can be set manually in script or loaded from file
'''

import numpy as np
from tasc_learning.models.recurrent.tasc_rnn_modular import TASC_RNN_Modular
from tasc_learning.models.recurrent.recurrent import TASC_System_Dynamics
import tasc_learning.datasets.MFRMFPSys2 as data
import sys
import getopt

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

def help():
    print('Usage: python3 test_tasc_rnn_modular.py [options]')
    print('trains tasc_rnn_modular networks on the MFRMFP2 system')
    print('Options:')
    print('-r [--res_dir]:    results direcotry (../../../data/MFRMFPSys2/results)')
    print('-d [--dataset_no]: dataset number (1)')
    print('-e [--exp_no]:     experiment number (1)')
    print('-L [--load]:       load test data from file')
    print('-g [--gen_force]:  force a new dataset to be generated')
    print('-G [--gen_save]:   save any dataset generated')
    print('-n [--name]:       name for model (TASC_RNN_mod)')
    print('-h [--help]:       display this help')

if __name__ == '__main__':
    # output info
    results_dir = '../../../data/MFRMFPSys2/results/'
    dataset_no = 1
    exp_no = 1
    name = 'TASC_RNN_mod'

    # experimental parameters
    parent_state_size = 2
    swarm_state_size = 2
    swarm_param_size = 7
    swarm_input_size = 1
    swarm_size = 4
    abs_size = 3
    aux_size = 5
    abs_lstm_size = 10
    abs_layers = [50]
    abs_activ = 'relu'
    aux_lstm_size = 10
    aux_layers = [50]
    aux_activ = 'relu'
    parent_ctrl_layers = [20]
    parent_ctrl_activ = 'relu'
    swarm_ctrl_layers = [75]
    swarm_ctrl_activ = 'relu'
    abs_dyn_layers = [50]
    abs_dyn_activ = 'relu'
    Q_p = np.array([[10.0, 0.0], [0.0, 1.0]])
    Q_si = np.array([[1.0, 0.0], [0.0, 1.0]])
    weight_dyn = 1.0
    weight_abs = 1.0
    horizon = 100
    del_t = 0.1
    samples = 100000
    epochs = 1000

    # handle command line options
    try:
        opts, args, = getopt.getopt(sys.argv[1:], 'r:d:e:LgGn:h', ['res_dir=', 'dataset_no=', 'exp_no=', 'load', 'gen_force', 'gen_save', 'name=', 'help'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    load = False
    gen_force = False
    gen_save = False
    for [opt, arg] in opts:
        if opt in ['-r', '--res_dir']:
            results_dir = arg
        elif opt in ['-d', '--dataset_no']:
            dataset_no = int(arg)
        elif opt in ['-e', '--exp_no']:
            exp_no = int(arg)
        elif opt in ['-L', '--load']:
            load = True
        elif opt in ['-g', '--gen_force']:
            gen_force = True
        elif opt in ['-G', '--gen_save']:
            gen_save = True
        elif opt in ['-n', '--name']:
            name = arg
        elif opt in ['-h', '--help']:
            help()
            sys.exit(0)

    if load:
        parent_state_size, swarm_state_size, swarm_param_size, swarm_input_size, swarm_size, abs_size, aux_size, abs_lstm_size, abs_layers, abs_activ, aux_lstm_size, aux_layers, aux_activ, parent_ctrl_layers, parent_ctrl_activ, swarm_ctrl_layers, swarm_ctrl_activ, abs_dyn_layers, abs_dyn_activ, Q_p, Q_si, weight_dyn, weight_abs, horizon, del_t, samples, epochs = pkl.load(open('params/exp%d_params.pkl'%(exp_no,), 'rb'))

    # generate output file n_swarm_members
    param_file = 'MFRMFP2_dataset%d_tasc_rnn_mod_params%d.h5'%(dataset_no, exp_no)
    loss_file = 'MFRMFP2_dataset%d_tasc_rnn_mod_loss%d.pkl'%(dataset_no, exp_no)
    states_file = 'MFRMFP2_dataset%d_tasc_rnn_mod_states%d.h5'%(dataset_no, exp_no)
    training_params = 'dataset%d_params.pkl'%(dataset_no,)
    training_limits = 'dataset%d_limits.pkl'%(dataset_no,)

    # load training data
    data_manager = data.MFRMFPSys2(training_params, training_limits)

    if gen_force:
        data_manager.generate_dataset(swarm_size, samples)
        if gen_save:
            data_manager.save_dataset(data_manager.gen_filename(swarm_size, samples))
    else:
        if data_manager.autoload(swarm_size, samples) and gen_save:
            data_manager.save_dataset(data_manager.gen_filename(swarm_size, samples))

    # setup forward dynamics
    sys_dyn = TASC_System_Dynamics(data_manager.f_s, data_manager.f_p, data_manager.f_s_keras, data_manager.f_p_keras, del_t, 2)

    # setup RNN
    tasc_rnn = TASC_RNN_Modular(
        parent_state_size = parent_state_size,
        swarm_state_size = swarm_state_size,
        swarm_param_size = swarm_param_size,
        swarm_input_size = swarm_input_size,
        swarm_size = swarm_size,
        abs_size = abs_size,
        aux_size = aux_size,
        abs_lstm_size = abs_lstm_size,
        abs_layers = abs_layers,
        abs_activ = abs_activ,
        aux_lstm_size = aux_lstm_size,
        aux_layers = aux_layers,
        aux_activ = aux_activ,
        parent_ctrl_layers = parent_ctrl_layers,
        parent_ctrl_activ = parent_ctrl_activ,
        swarm_ctrl_layers = swarm_ctrl_layers,
        swarm_ctrl_activ = swarm_ctrl_activ,
        abs_dyn_layers = abs_dyn_layers,
        abs_dyn_activ = abs_dyn_activ,
        sys_dyn = sys_dyn,
        Q_p = Q_p,
        Q_si = Q_si,
        weight_dyn = weight_dyn,
        weight_abs = weight_abs,
        home=results_dir,
        name=name
        )

    # save params
    tasc_rnn.save_params(param_file)

    # generate desired parent states
    xp_d = np.zeros((samples, horizon, parent_state_size))

    # train network
    tasc_rnn.train(xp_d, data_manager.xp_0, data_manager.xs_0, data_manager.params, val_split=0.2, epochs=epochs, batch_size=128, verbosity=2, patience=100, checkpoint=False, loss_file=loss_file, states_file=states_file)

    # save results
    tasc_rnn.save(loss_file, states_file)
