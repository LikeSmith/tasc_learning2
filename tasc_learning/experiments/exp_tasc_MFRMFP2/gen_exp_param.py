'''
Generates experimental parameters for training
'''

import numpy as np
import os
import errno

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

exp_no = 0

parent_state_size = 2
swarm_state_size = 2
swarm_param_size = 7
swarm_input_size = 1
swarm_size = 4
abs_size = 3
aux_size = 5
abs_lstm_size = 10
abs_layers = [50]
abs_activ = 'relu'
aux_lstm_size = 10
aux_layers = [50]
aux_activ = 'relu'
parent_ctrl_layers = [20]
parent_ctrl_activ = 'relu'
swarm_ctrl_layers = [75]
swarm_ctrl_activ = 'relu'
abs_dyn_layers = [30]
abs_dyn_activ = 'relu'
Q_p = np.array([[10.0, 0.0], [0.0, 1.0]])
Q_si = np.array([[1.0, 0.0], [0.0, 1.0]])
weight_dyn = 1.0
weight_abs = 1.0
horizon = 5
del_t = 0.1
samples = 100
epochs = 10

train_params = [parent_state_size, swarm_state_size, swarm_param_size, swarm_input_size, swarm_size, abs_size, aux_size, abs_lstm_size, abs_layers, abs_activ, aux_lstm_size, aux_layers, aux_activ, parent_ctrl_layers, parent_ctrl_activ, swarm_ctrl_layers, swarm_ctrl_activ, abs_dyn_layers, abs_dyn_activ, Q_p, Q_si, weight_dyn, weight_abs, horizon, del_t, samples, epochs]

try:
    os.makedirs('params/', exist_ok=True)
except TypeError:
    try:
        os.makedirs('params/')
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

pkl.dump(train_params, open('params/exp%d_params.pkl'%(exp_no,), 'wb'))
