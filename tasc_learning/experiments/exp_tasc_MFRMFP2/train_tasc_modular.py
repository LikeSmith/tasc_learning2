'''
This script will train the full context network
'''

import numpy as np
from tasc_learning.models import tasc_modular, analytic_model
import tasc_learning.datasets.MFRMFPSys2 as data
import _pickle as pkl
import os
import sys
import getopt

def help():
    print('Usage: python3 test_tasc_modular.py [options]')
    print('trains tasc_modular networks on the MFRMFP2 system')
    print('Options:')
    print('-r [--res_dir]:    results direcotry (../../../data/MFRMFPSys2/results)')
    print('-d [--dataset_no]: dataset number (1)')
    print('-e [--exp_no]:     experiment number (1)')
    print('-L [--load]:       load test data from file')
    print('-g [--gen_force]:  force a new dataset to be generated')
    print('-G [--gen_save]:   save any dataset generated')
    print('-h [--help]:       display this help')

if __name__ == '__main__':
    # output info
    results_dir = '../../../data/MFRMFPSys2/results/'
    dataset_no = 1
    exp_no = 1

    # experimental parameters
    parent_state = 2
    swarm_state = 2
    swarm_input = 1
    swarm_param = 7
    swarm_size = 4
    abs_size = 3
    aux_size = 5
    abs_lstm_size = 10
    abs_layers = [50]
    abs_activ = ['tanh']
    abs_activ_final = 'linear'
    aux_lstm_size = 10
    aux_layers = [50]
    aux_activ = ['tanh']
    aux_activ_final = 'linear'
    swarm_ctrl_layers = [50]
    swarm_ctrl_activ = ['tanh']
    swarm_ctrl_activ_final = 'linear'
    parent_ctrl_layers = [10]
    parent_ctrl_activ = ['tanh']
    parent_ctrl_activ_final = 'linear'
    abs_dyn_layers = [20]
    abs_dyn_activ = ['tanh']
    abs_dyn_activ_final = 'linear'
    Q_p = np.array([[10.0, 0.0], [0.0, 1.0]])
    Q_pf = Q_p
    Q_si = np.array([[1.0, 0.0], [0.0, 0.0]])
    weight_dyn = 1.0
    weight_abs = 1.0
    horizon = 100
    del_t = 0.1
    n = 10000
    epochs = 1000
    patience = 100

    # handle command line options
    try:
        opts, args, = getopt.getopt(sys.argv[1:], 'r:d:e:LgGh', ['res_dir=', 'dataset_no=', 'exp_no=', 'load', 'gen_force', 'gen_save', 'help'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    load = False
    gen_force = False
    gen_save = False
    for [opt, arg] in opts:
        if opt in ['-r', '--res_dir']:
            results_dir = arg
        elif opt in ['-d', '--dataset_no']:
            dataset_no = int(arg)
        elif opt in ['-e', '--exp_no']:
            exp_no = int(arg)
        elif opt in ['-L', '--load']:
            load = True
        elif opt in ['-g', '--gen_force']:
            gen_force = True
        elif opt in ['-G', '--gen_save']:
            gen_save = True
        elif opt in ['-h', '--help']:
            help()
            sys.exit(0)

    if load:
        parent_state, swarm_state, swarm_input, swarm_param, swarm_size, abs_size, aux_size, abs_lstm_size, abs_layers, abs_activ, abs_activ_final, aux_lstm_size, aux_layers, aux_activ, aux_activ_final, swarm_ctrl_layers, swarm_ctrl_activ, swarm_ctrl_activ_final, parent_ctrl_layers, parent_ctrl_activ, parent_ctrl_activ_final, abs_dyn_layers, abs_dyn_activ, abs_dyn_activ_final, Q_p, Q_pf, Q_si, wheight_dyn, weight_abs, horizon, del_t, n, epochs, patience = pkl.load(open('params/exp%d_params.pkl'%(exp_no,), 'rb'))

    # setup directories
    os.makedirs(results_dir, exist_ok=True)

    # generate output file n_swarm_members
    parent_ctrl_file = 'MFRMFP_dataset%d_parent_ctrl_mdl%d.h5'%(dataset_no, exp_no)
    parent_ctrl_param = 'MFRMFP_dataset%d_arent_ctrl_params%d.pkl'%(dataset_no, exp_no)
    swarm_ctrl_file = 'MFRMFP_dataset%d_swarm_ctrl_mdl%d.h5'%(dataset_no, exp_no)
    swarm_ctrl_param = 'MFRMFP_dataset%d_swarm_ctrl_params%d.pkl'%(dataset_no, exp_no)
    abs_enc_file = 'MFRMFP_dataset%d_abs_enc_mdl%d.h5'%(dataset_no, exp_no)
    abs_enc_param = 'MFRMFP_dataset%d_abs_enc_params%d.pkl'%(dataset_no, exp_no)
    abs_dyn_file = 'MFRMFP_dataset%d_abs_dyn_mdl%d.h5'%(dataset_no, exp_no)
    abs_dyn_param = 'MFRMFP_dataset%d_abs_dyn_params%d.pkl'%(dataset_no, exp_no)
    aux_enc_file = 'MFRMFP_dataset%d_aux_enc_mdl%d.h5'%(dataset_no, exp_no)
    aux_enc_param = 'MFRMFP_dataset%d_aux_enc_params%d.pkl'%(dataset_no, exp_no)
    training_params = 'dataset%d_params.pkl'%(dataset_no,)
    training_limits = 'dataset%d_limits.pkl'%(dataset_no,)

    # load training data
    data_manager = data.MFRMFPSys2(training_params, training_limits)

    if gen_force:
        data_manager.generate_dataset(swarm_size, n)
        if gen_save:
            data_manager.save_dataset(data_manager.gen_filename(swarm_size, n))
    else:
        if data_manager.autoload(swarm_size, n) and gen_save:
            data_manager.save_dataset(data_manager.gen_filename(swarm_size, n))

    # set up networks
    forward_dyn = analytic_model.Analytic_Model_Parent_Swarm(
        n_parent_states=parent_state,
        n_swarm_states=swarm_state,
        n_swarm_inputs=swarm_input,
        n_swarm_params=swarm_param,
        n_swarm_members=swarm_size,
        cont_parent_fun_keras=data_manager.f_p_keras,
        cont_parent_fun=data_manager.f_p,
        cont_swarm_fun_keras=data_manager.f_s_keras,
        cont_swarm_fun=data_manager.f_s,
        method='RK4',
        del_t=del_t,
        name='MFRMFP_analytic'
        )

    train_model = tasc_modular.Modular_Tasc_Trainer(
        parent_state=parent_state,
        swarm_state=swarm_state,
        swarm_input=swarm_input,
        swarm_param=swarm_param,
        swarm_size=swarm_size,
        abs_size=abs_size,
        aux_size=aux_size,
        abs_lstm_size=abs_lstm_size,
        abs_layers=abs_layers,
        abs_activ=abs_activ,
        abs_activ_final=abs_activ_final,
        aux_lstm_size=aux_lstm_size,
        aux_layers=aux_layers,
        aux_activ=aux_activ,
        aux_activ_final=aux_activ_final,
        swarm_ctrl_layers=swarm_ctrl_layers,
        swarm_ctrl_activ=swarm_ctrl_activ,
        swarm_ctrl_activ_final=swarm_ctrl_activ_final,
        parent_ctrl_layers=parent_ctrl_layers,
        parent_ctrl_activ=parent_ctrl_activ,
        parent_ctrl_activ_final=parent_ctrl_activ_final,
        abs_dyn_layers=abs_dyn_layers,
        abs_dyn_activ=abs_dyn_activ,
        abs_dyn_activ_final=abs_dyn_activ_final,
        forward_dyn=forward_dyn,
        Q_p=Q_p,
        Q_pf=Q_pf,
        Q_si=Q_si,
        weight_dyn=weight_dyn,
        weight_abs=weight_abs,
        horizon=horizon,
        home=results_dir,
        name='tasc_trainer'
        )

    # train models
    train_model.save_abs_enc_params(abs_enc_param)
    train_model.save_aux_enc_params(aux_enc_param)
    train_model.save_swarm_ctrl_params(swarm_ctrl_param)
    train_model.save_parent_ctrl_params(parent_ctrl_param)
    train_model.save_abs_dyn_params(abs_dyn_param)

    sys.setrecursionlimit(1000000)

    train_model.set_net_save_paths(abs_enc_net=abs_enc_file, aux_enc_net=aux_enc_file, swarm_ctrl_net=swarm_ctrl_file, parent_ctrl_net=parent_ctrl_file, abs_dyn_net=abs_dyn_file)

    xp_des = np.zeros((n, 2, horizon))

    train_model.train(data_manager.xp_0, data_manager.xs_0, xp_des, data_manager.params, val_split=0.2, epochs=epochs, batch_size=10, verbosity=2, patience=patience, checkpoint=True)
