"""
plot iLQR results
"""

import scipy.io as io
import matplotlib.pyplot as plt
import numpy as np
import matplotlib

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

mat = io.loadmat('iLQR_results.mat')

x1 = mat['x1']
x2 = mat['x2']

xs1 = x1[0:2, :]
xs2 = x2[0:2, :]

p1 = np.zeros((4, x1.shape[1]))
p2 = np.zeros((4, x1.shape[1]))
v1 = np.zeros((4, x2.shape[1]))
v2 = np.zeros((4, x2.shape[1]))

t = np.arange(0, 7.01, 0.01)

for i in range(4):
    p1[i, :] = x1[2+2*i, :]
    p2[i, :] = x2[2+2*i, :]
    v1[i, :] = x1[3+2*i, :]
    v2[i, :] = x2[3+2*i, :]

plt.figure()
plt.subplot(211)
plt.plot(t, xs1.T)
plt.xlabel('time (s)')
plt.legend([r'$\theta$ (rad)', r'$\dot{\theta}$ (rad/s)'])
plt.subplot(212)
plt.plot(t, xs2.T)
plt.xlabel('time (s)')
plt.legend([r'$\theta$ (rad)', r'$\dot{\theta}$ (rad/s)'])
plt.savefig('iLQR_parent_state.pdf')

plt.figure()
plt.subplot(211)
plt.plot(t, p1.T)
plt.xlabel('time (s)')
plt.ylabel('robot pos (m)')
plt.subplot(212)
plt.plot(t, p2.T)
plt.xlabel('time (s)')
plt.ylabel('robot pos (m)')
plt.savefig('iLQR_swarm_positions.pdf')

plt.figure()
plt.subplot(211)
plt.plot(t, v1.T)
plt.xlabel('time (s)')
plt.ylabel('robot vel (m/s)')
plt.subplot(212)
plt.plot(t, v2.T)
plt.xlabel('time (s)')
plt.ylabel('robot vel (m/s)')
plt.savefig('iLQR_swarm_velocities.pdf')

plt.show()

