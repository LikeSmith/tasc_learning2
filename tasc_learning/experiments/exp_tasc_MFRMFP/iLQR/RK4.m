function x_new = RK4(x, u, f_c, del_t)
x_dot1 = f_c(x, u);
x_dot2 = f_c(x + 0.5*del_t*x_dot1, u);
x_dot3 = f_c(x + 0.5*del_t*x_dot2, u);
x_dot4 = f_c(x + del_t*x_dot3, u);

x_new = x + del_t/6*(x_dot1 + 2*x_dot2 + 2*x_dot3 + x_dot4);
end