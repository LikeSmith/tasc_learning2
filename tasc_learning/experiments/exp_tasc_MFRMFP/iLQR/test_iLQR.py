'''
Uses a trained forward model to perform iLQR.
'''

import numpy as np
from keras.layers import Input, Lambda
from keras.models import Model
from keras.callbacks import EarlyStopping
import keras.backend as KK
import tasc_learning.models.nonlin_forward as f_model
import tasc_learning.models.analytic_model as a_model
import tasc_learning.models.mlp as mlp
import tasc_learning.datasets.MFRMFPSys as dset
import tasc_learning.utils.DNN_derivatives as dnnd
import _pickle as pkl
import matplotlib.pyplot as plt

class iLQR(object):
    def __init__(self, x_dim, u_dim, h_dim, forward_model_path,
                 Q, Q_f, R, horizon=100, del_t=0.1,
                 from_dynamics=False, f=None
    ):
        self.x_dim = x_dim
        self.u_dim = u_dim
        self.h_dim = h_dim

        self.Q = Q
        self.R = R
        self.Q_f = Q_f

        self.horizon = horizon

        if from_dynamics:
            self.fwd_mdl = a_model.Analytic_Model_single(
                self.x_dim,
                self.u_dim,
                f
            )
        else:
            self.fwd_mdl = f_model.Nonlin_Forward(
                self.x_dim,
                self.u_dim,
                self.h_dim,
                from_file=True,
                file_path=forward_model_path
            )

        self.get_grads = KK.function(
            self.fwd_mdl.mdl.inputs,
            [
                dnnd.f_derivative_symb(self.fwd_mdl.mdl, 0, 0),
                KK.transpose(
                    dnnd.f_derivative_symb(self.fwd_mdl.mdl, 1, 0)
                )
            ]
        )


    def linearize(self, x, u):
        A, B = self.get_grads([x, u])
        return np.squeeze(A), np.squeeze(B,-1)


    def calc_cost_to_go(self, xd, X, U):
        V = np.zeros((self.horizon, 1))
        diff = np.expand_dims(X[-1]-xd, 0)
        V[-1] = np.dot(
            diff,
            np.dot(self.Q_f, np.transpose(diff))
        )
        for tidx in np.arange(self.horizon-2, -1, -1):
            diff = X[tidx]-xd
            u = np.expand_dims(U[tidx], 0)
            V[tidx] = np.dot(
                diff,
                np.dot(self.Q, np.transpose(diff))
            ) + np.dot(
                u,
                np.dot(self.R, np.transpose(u))
            ) + V[tidx+1]

        return V


    def calc_k(self, A, B, p, x, u):
        pA = np.dot(p, A)
        BtPA = np.dot(np.transpose(B), pA)
        BtPB = np.dot(np.transpose(B), np.dot(p, B))
        RBtPBinv = np.linalg.inv(self.R + BtPB)
        p_ = self.Q \
             + np.dot(np.transpose(A), pA) \
             - np.dot(
                 np.transpose(pA),
                 np.dot(
                     B,
                     np.dot(
                         RBtPBinv,
                         np.dot(
                             np.transpose(B),
                             pA
                         )
                     )
                 )
             )
        k =  -np.dot(RBtPBinv, BtPA)

        return k, p_


    def backward_phase(self, X, U):
        As = np.zeros((X.shape[0], X.shape[1], X.shape[1]))
        Bs = np.zeros((X.shape[0], X.shape[1], U.shape[1]))
        K = np.zeros((X.shape[0], U.shape[1], X.shape[1]))
        P = np.zeros((self.horizon, self.x_dim, self.x_dim))
        P[-1] = self.Q_f

        for tidx in range(self.horizon-2, -1, -1):
            A, B = self.linearize(
                np.expand_dims(X[tidx], 0),
                np.expand_dims(U[tidx], 0)
            )
            k, p_ = self.calc_k(A, B, P[tidx+1], X[tidx], U[tidx])
            As[tidx] = A
            Bs[tidx] = B
            K[tidx] = k
            ABK = A + np.dot(B, k)
            P[tidx] = p_

        return K, P, As, Bs


    def forward_phase(self, x0, xd, U, K, alpha):
        X_prime = np.zeros((U.shape[0]+1, self.x_dim))
        U_prime = np.zeros(U.shape)
        X_prime[0] = x0
        for tidx in range(self.horizon-1):
            u = alpha*np.dot(
                np.expand_dims(K[tidx], 0),
                (X_prime[tidx] - xd).reshape(self.x_dim,1)
            )
            U_prime[tidx] = u[:, 0]
            temp = self.fwd_mdl.mdl.predict([
                np.expand_dims(X_prime[tidx], 0),
                u[:, :, 0]
            ])

            X_prime[tidx+1] = temp
        return X_prime, U_prime


    def execute(self, iters, x0, xd, U):
        convergence_threshold = 1e-15
        alpha_threshold = 1e-15
        max_alpha = 0.00001
        alpha = max_alpha

        As = np.zeros((self.horizon, x_dim, x_dim))
        Bs = np.zeros((self.horizon, x_dim, u_dim))
        K = np.zeros((self.horizon, u_dim, x_dim))
        U = np.zeros((self.horizon-1, u_dim))
        X, U = self.forward_phase(x0, xd, U, K, alpha)
        V = self.calc_cost_to_go(xd, X, U)
        cost = np.sum(V)

        for idx in np.arange(iters):
            K_prime, P_prime, As_prime, Bs_prime = self.backward_phase(X, U)
            X_prime, U_prime = self.forward_phase(x0, xd, U, K_prime, alpha)
            V_prime = self.calc_cost_to_go(xd, X_prime, U)

            cost_prime = np.sum(V_prime)
            print("alpha: ", alpha, "cost: ", cost, "cost_prime: ", cost_prime)
            if cost_prime < cost:
                if (np.abs(cost_prime - cost)/cost) < convergence_threshold:
                    break
                As = As_prime
                Bs = Bs_prime
                X = X_prime
                U = U_prime
                V = V_prime
                K = K_prime
                cost = cost_prime
                alpha = max_alpha
                #alpha *= 1.1
            elif alpha < alpha_threshold:
                break
            else:
                alpha /= 2.

        return X, U, V

if __name__ == '__main__':
    training_params = 'dataset2_4Rob_params.pkl'
    training_limits = 'dataset2_4Rob_limits.pkl'
    data = dset.MFRMFPSys(training_params, training_limits)
    mdl_path = '../../../data/pendSys/results/nonlin_forward_mdl1.h5'

    x_dim = 10
    u_dim = 4
    forward_model_path = mdl_path
    h_dim = 30

    Q = np.diag([10, 1, 1, 1, 1, 1, 1, 1, 1, 1])
    Q_f = Q
    R = np.eye(4)*0.0

    t_f = 10.0
    t = np.arange(0.0, t_f, data.del_t)
    horizon = len(t)
    from_dynamics = True
    if from_dynamics:
        f = data.RK4_keras_onestate
    else:
        f = None

    del_t=data.del_t

    ilqr = iLQR(x_dim, u_dim, h_dim, forward_model_path=mdl_path,
                Q=Q, Q_f=Q_f, R=R, horizon=horizon, del_t=0.1,
                from_dynamics=from_dynamics, f=f)

    x0 = np.array([0.0, 0.0, 0.01, 0.0, -0.01, 0.0, 0.01, 0.0, -0.01, 0.0])
    xd = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,])
    U = np.zeros((horizon-1, u_dim))

    X, U, V = ilqr.execute(1000, x0, xd, U)

    title = 'iLQR'
    if from_dynamics:
        title += ' (analytic model)'
    else:
        title += ' (DNN model)'

    xp = X[:, :2]
    p = np.zeros((horizon, 4))
    v = np.zeros((horizon, 4))

    for i in range(4):
        p[:, i] = X[:, 2+i*2]
        v[:, i] = X[:, 3+i*2]

    plt.figure()
    plt.plot(t, xp)
    plt.xlabel('time (s)')
    plt.legend([r'$\theta$ (rad)', r'$\dot{\theta}$ (rad/s)'])
    plt.figure()
    plt.plot(t, p)
    plt.xlabel('time (s)')
    plt.ylabel('robot positions (m)')
    plt.figure()
    plt.plot(t, v)
    plt.xlabel('time (s)')
    plt.ylabel('robot velocities (m/s)')
    plt.show()

    '''
    plt.suptitle(title)
    plt.subplot(2,1,1)
    plt.plot(X[:, 0], 'k-.')
    plt.plot(X[:, 1], 'k--')
    plt.plot(U, 'b-')
    plt.plot([0, horizon], [x0[0], x0[0]], 'r-.')
    plt.plot([0, horizon], [xd[0], xd[0]], 'g-.')
    plt.legend(['X', 'X\'', 'U', 'x_0[0]', 'x_0[1]', 'x_d[0]', 'x_d[1]'])
    plt.title('State and Control Trajectories')

    plt.subplot(2,1,2)
    plt.plot(V)
    plt.legend(['V'])
    plt.title('Cost-to-go')
    plt.xlabel('t')
    plt.show()
    '''
