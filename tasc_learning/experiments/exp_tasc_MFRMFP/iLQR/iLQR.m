%
% iLQR for MFRMFP
%

clear all
close all

del_t = 0.01;
J = 0.75;
g = 9.81;
gamma_p = [0.01 1000 700 0.02 1000 1];
m = [0.59948524340888498 0.92535874349963065 1.4847497437580131 0.53836400564020959];
gamma_s = [0 1000 700 0 1000 1.6086576856156043;
    0 1000 700 0 1000 1.1073133803156212;
    0 1000 700 0 1000 1.9673202554867713;
    0 1000 700 0 1000 1.3929104402316732];

f_f = @(v, gamma)gamma(1)*(tanh(gamma(2)*v) - tanh(gamma(3)*v)) + gamma(4)*tanh(gamma(5)*v) + gamma(6)*v;

f_si = @(xsi, us, i)[xsi(2); (us - f_f(xsi(2), gamma_s(i, :)))/m(i)];
f_s = @(xs, us)[f_si(xs(1:2), us(1), 1); f_si(xs(3:4), us(2), 2); f_si(xs(5:6), us(3), 3); f_si(xs(7:8), us(4), 4)];
f_p = @(xp, xs)[xp(2); (-g*cos(xp(1))*(m(1)*xs(1) + m(2)*xs(3) + m(3)*xs(5) + m(4)*xs(7)) - 2*xp(2)*(m(1)*xs(1)*xs(2) + m(2)*xs(3)*xs(4) + m(3)*xs(5)*xs(6) + m(4)*xs(7)*xs(8)) - f_f(xp(2), gamma_p))/(J + m(1)*xs(1)^2 + m(2)*xs(3)^2 + m(3)*xs(5)^2 + m(4)*xs(7)^2)];

f_c = @(x, u)[f_p(x(1:2), x(3:10)); f_s(x(3:10), u)];

f_d = @(x, u)RK4(x, u, f_c, del_t);

Q = diag([10, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
Q_f = Q;
R = eye(4)*0.0;

x0 = [0.2 0 0.5 0 -0.5 0 0.5 0 -0.5 0]';
t_f = 7;

del_x = ones(10, 1)*0.001;
del_u = ones(4, 1)*0.001;

t = 0:del_t:t_f;
n = length(t);
u = zeros(4, n-1);
x = zeros(10, n);
x(:, 1) = x0;

running = true;
cost = inf;
K = zeros(4, 10, n-1);

while running
	for i = 1:n-1
		u(:, i) = -K(:, :, i)*x(:, i);
		x(:, i+1) = f_d(x(:, i), u(:, i));
	end
	
	J = zeros(1, n);
	J(n) = x(:, end)'*Q_f*x(:, end);
	
	P = zeros(10, 10, n);
	P(:, :, end) = Q_f;
	
	for i = n-1:-1:1
		[A, B] = num_diff(f_d, x(:, i), u(:, i), del_x, del_u);
		J(i) = J(i+1) + x(:, i)'*Q*x(:, i) + u(:, i)'*R*u(:, i);
		K(:, :, i) = (R + B'*P(:, :, i+1)*B)\(B'*P(:, :, i+1)*A);
		P(:, :, i) = A'*P(:, :, i+1)*A - (A'*P(:, :, i+1)*B)*K(:, :, i) + Q;
	end
	
	figure(1)
	plot(t, x(1:2, :))
	xlabel('time (s)');
	h1 = legend('$\theta$ (rad)', '$\dot{\theta}$ (rad/s)');
	set(h1, 'Interpreter','latex')
	
	figure(2)
	plot(t, x(3:2:9, :))
	xlabel('time(s)');
	ylabel('robot positions (m)');
	
	figure(3)
	plot(t, x(4:2:10, :))
	xlabel('time(s)');
	ylabel('robot velocicties (m/s)');
	
	drawnow;
	
	cost_prime = max(J);
	
	if (abs(cost - cost_prime) < 0.01)
		running = false;
	end
	
	fprintf('cost: %f cost prime: %f\n', cost, cost_prime);
	cost = cost_prime;
end

x1 = zeros(10, n);
x2 = zeros(10, n);
u1 = zeros(4, n);
u2 = zeros(4, n);

x1(:, 1) = [0.2 0 0.5 0 -0.5 0 0.5 0 -0.5 0]';
x2(:, 1) = [0.2 0 0.5 0 -0.5 0 0.5 0 -0.5 0]';

for i = 1:n-1
	u1(:, i) = -K(:, :, i)*x1(:, i);
	u2(:, i) = -K(:, :, i)*x2(:, i);
	
	if t(i) == 3
		u2(:, i) = u2(:, i) + ones(size(u(:, i)))*100;
	end
	
	x1(:, i+1) = f_d(x1(:, i), u1(:, i));
	x2(:, i+1) = f_d(x2(:, i), u2(:, i));
end

figure(1)
subplot(2, 1, 1)
plot(t, x1(1:2, :))
xlabel('time (s)');
h1 = legend('$\theta$ (rad)', '$\dot{\theta}$ (rad/s)');
set(h1, 'Interpreter','latex')
subplot(2, 1, 2)
plot(t, x2(1:2, :))
xlabel('time (s)');
h1 = legend('$\theta$ (rad)', '$\dot{\theta}$ (rad/s)');
set(h1, 'Interpreter','latex')

figure(2)
subplot(2, 1, 1)
plot(t, x1(3:2:9, :))
xlabel('time(s)');
ylabel('robot positions (m)');
subplot(2, 1, 2)
plot(t, x2(3:2:9, :))
xlabel('time(s)');
ylabel('robot positions (m)');

figure(3)
subplot(2, 1, 1)
plot(t, x1(4:2:10, :))
xlabel('time(s)');
ylabel('robot velocicties (m/s)');
subplot(2, 1, 2)
plot(t, x2(4:2:10, :))
xlabel('time(s)');
ylabel('robot velocicties (m/s)');
