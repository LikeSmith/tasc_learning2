function [A, B] = num_diff(f, x, u, del_x, del_u)
A = zeros(length(x), length(x));
B = zeros(length(x), length(u));

for i = 1:length(x)
	x_shift = x;
	x_shift(i) = x_shift(i) + del_x(i);
	
	A(:, i) = (f(x_shift, u) - f(x, u))/del_x(i);
end

for i = 1:length(u)
	u_shift = u;
	u_shift(i) = u_shift(i) + del_u(i);
	
	B(:, i) = (f(x, u_shift) - f(x, u))/del_u(i);
end
end