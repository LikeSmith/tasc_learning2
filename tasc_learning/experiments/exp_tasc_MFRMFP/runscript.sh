#!/bin/bash

# set output and error output filenames
#SBATCH -o outputs/policy_full_context_train_%j.out
#SBATCH -e outputs/policy_full_context_train_%j.err

# email me when job starts and stops
#SBATCH --mail-type=ALL
#SBATCH --mail-user=crandallk@gwu.edu

# set up which queue should be joined
#SBATCH -N 1
#SBATCH -p short

# set the correct directory
#SBATCH -D /home/crandallk/TASC_Learning2/tasc_learning/experiments/exp_policy_learning

# name job
#SBATCH -J policy_full_context

# set time limit
#SBATCH -t 2-00:00:00

# load necesary modules
module load anaconda

# setup environmental variables
export PYTHONPATH=/home/crandallk/TASC_Learning2:$PYTHONPATH

# run task
THEANO_FLAGS=optimizer=fast_compile,optimizer_including=fusion python3 train_full_context.py
