'''
This script tests the full context controllers`
'''

import numpy as np
from tasc_learning.models import tasc, mlp
import tasc_learning.datasets.MFRMFPSys as data
import matplotlib.pyplot as plt
import matplotlib
import _pickle as pkl

if __name__ == '__main__':
    results_dir = '../../../data/MFRMFPSys/results/'
    parent_ctrl_file = 'MFRMFP_parent_ctrl_mdl1.h5'
    parent_ctrl_param = 'MFRMFP_parent_ctrl_params1.pkl'
    swarm_ctrl_file = 'MFRMFP_swarm_ctrl%d_mdl1.h5'
    swarm_ctrl_param = 'MFRMFP_swarm_ctrl%d_params1.pkl'
    abs_enc_file = 'MFRMFP_abs_enc_mdl1.h5'
    abs_enc_param = 'MFRMFP_abs_enc_params1.pkl'
    abs_dyn_file = 'MFRMFP_abs_dyn_mdl1.h5'
    abs_dyn_param = 'MFRMFP_abs_dyn_params1.pkl'
    aux_enc_file = 'MFRMFP_aux_enc_mdl1.h5'
    aux_enc_param = 'MFRMFP_aux_enc_params1.pkl'
    training_params = 'dataset1_4Rob_params.pkl'
    training_limits = 'dataset1_4Rob_limits.pkl'
    parent_state_fig = 'figs/parent_states.pdf'
    swarm_pos_fig = 'figs/swarm_pos.pdf'
    swarm_vel_fig = 'figs/swarm_vel.pdf'
    abs_state_fig = 'figs/abs_state.pdf'
    sim_results = 'results_sim.pkl'

    # test parameters
    parent_state_size = 2
    swarm_state_size = 2
    swarm_input_size = 1
    swarm_size = 4
    abs_size = 3
    aux_size = 5
    xp1_0 = np.array([0.2, 0.0])
    xp2_0 = np.array([0.2, 0.0])
    xs1_0 = np.array([0.5, 0.0, -0.5, 0.0, 0.5, 0.0, -0.5, 0.0])
    xs2_0 = np.array([0.5, 0.0, -0.5, 0.0, 0.5, 0.0, -0.5, 0.0])
    t_f = 7.01

    # load stuff
    train_dat = data.MFRMFPSys(training_params, training_limits)

    parent_ctrl = mlp.MLP(from_file=True, file_path=parent_ctrl_file, params_path=parent_ctrl_param, home=results_dir)

    abs_enc = mlp.MLP(from_file=True, file_path=abs_enc_file, params_path=abs_enc_param, home=results_dir)

    aux_enc = mlp.MLP(from_file=True, file_path=aux_enc_file, params_path = aux_enc_param, home=results_dir)

    swarm_ctrls = []

    for i in range(swarm_size):
        swarm_ctrls.append(tasc.Swarm_Member_Policy(from_file=True, net_file=swarm_ctrl_file%i, param_file=swarm_ctrl_param%i, name='full_context_trainer_member%d_policy'%i, home=results_dir))

    # setup result arrays
    t = np.arange(0.0, t_f, train_dat.del_t)
    n = t.shape[0]
    xp1 = np.zeros((parent_state_size, n))
    xp2 = np.zeros((parent_state_size, n))
    xs1 = np.zeros((swarm_state_size*swarm_size, n))
    xs2 = np.zeros((swarm_state_size*swarm_size, n))
    a1 = np.zeros((abs_size, n))
    a2 = np.zeros((abs_size, n))
    a_d1 = np.zeros((abs_size, n))
    a_d2 = np.zeros((abs_size, n))
    a_aux1 = np.zeros((aux_size, n))
    a_aux2 = np.zeros((aux_size, n))
    u1 = np.zeros((swarm_input_size*swarm_size, n))
    u2 = np.zeros((swarm_input_size*swarm_size, n))

    # run sim
    xp1[:, 0] = xp1_0
    xp2[:, 0] = xp2_0
    xs1[:, 0] = xs1_0
    xs2[:, 0] = xs2_0

    for i in range(n):
        a1[:, i] = abs_enc.eval(xs1[:, i])
        a2[:, i] = abs_enc.eval(xs2[:, i])
        a_aux1[:, i] = aux_enc.eval(xs1[:, i])
        a_aux2[:, i] = aux_enc.eval(xs2[:, i])
        a_d1[:, i] = parent_ctrl.eval(xp1[:, i])
        a_d2[:, i] = parent_ctrl.eval(xp2[:, i])

        for j in range(swarm_size):
            u1[swarm_input_size*j:swarm_input_size*(j+1), i] = swarm_ctrls[j].eval(xs1[swarm_state_size*j:swarm_state_size*(j+1), i], a1[:, i], a_d1[:, i], a_aux1[:, i])
            u2[swarm_input_size*j:swarm_input_size*(j+1), i] = swarm_ctrls[j].eval(xs2[swarm_state_size*j:swarm_state_size*(j+1), i], a2[:, i], a_d2[:, i], a_aux2[:, i])

        if t[i] == 3:
            u2[:, i] = u2[:, i] + np.ones(swarm_input_size*swarm_size)*100

        if i < n-1:
            xp1[:, i+1], xs1[:, i+1] = train_dat.f_disc(xp1[:, i], xs1[:, i], u1[:, i])
            xp2[:, i+1], xs2[:, i+1] = train_dat.f_disc(xp2[:, i], xs2[:, i], u2[:, i])

    p1 = np.zeros((swarm_size, n))
    p2 = np.zeros((swarm_size, n))
    v1 = np.zeros((swarm_size, n))
    v2 = np.zeros((swarm_size, n))

    for i in range(swarm_size):
        p1[i, :] = xs1[2*i, :]
        p2[i, :] = xs2[2*i, :]
        v1[i, :] = xs1[2*i+1, :]
        v2[i, :] = xs2[2*i+1, :]

    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42

    plt.figure()
    plt.subplot(211)
    plt.plot(t, xp1.T)
    plt.xlabel('time (s)')
    plt.legend([r'$\theta$ (rad)', r'$\dot{\theta}$ (rad/s)'])
    plt.subplot(212)
    plt.plot(t, xp2.T)
    plt.xlabel('time (s)')
    plt.legend([r'$\theta$ (rad)', r'$\dot{\theta}$ (rad/s)'])
    plt.savefig(results_dir+parent_state_fig)
    plt.figure()
    plt.subplot(211)
    plt.plot(t, p1.T)
    plt.xlabel('time (s)')
    plt.ylabel('robot positions (m)')
    plt.subplot(212)
    plt.plot(t, p2.T)
    plt.xlabel('time (s)')
    plt.ylabel('robot positions (m)')
    plt.savefig(results_dir+swarm_pos_fig)
    plt.figure()
    plt.subplot(211)
    plt.plot(t, v1.T)
    plt.xlabel('time (s)')
    plt.ylabel('robot velocity (m/s)')
    plt.subplot(212)
    plt.plot(t, v2.T)
    plt.xlabel('time (s)')
    plt.ylabel('robot velocity (m/s)')
    plt.savefig(results_dir+swarm_vel_fig)
    plt.figure()
    plt.plot(t, a1.T)
    plt.gca().set_prop_cycle(None)
    plt.plot(t, a_d1.T, ':')
    plt.xlabel('time (s)')
    plt.legend(['$a_1$', '$a_2$', '$a_3$', '$a_{d1}$', '$a_{d2}$', '$a_{d3}$'])
    plt.ylim([-0.04, 0.04])
    plt.savefig(results_dir+abs_state_fig)
    plt.show()

    pkl.dump([t, xp1, xp2, xs1, xs2, a1, a2, a_d1, a_d2, a_aux1, a_aux2, u1, u2], open(results_dir+sim_results, 'wb'))
