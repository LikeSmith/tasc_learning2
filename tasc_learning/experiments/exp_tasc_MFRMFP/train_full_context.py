'''
This script will train the full context network
'''

import numpy as np
import tasc_learning.models.tasc as NN
import tasc_learning.datasets.MFRMFPSys as data
import os
import sys

if __name__ == '__main__':
    results_dir = '../../../data/MFRMFPSys/results/'
    parent_ctrl_file = 'MFRMFP_parent_ctrl_mdl1.h5'
    parent_ctrl_param = 'MFRMFP_parent_ctrl_params1.pkl'
    swarm_ctrl_file = 'MFRMFP_swarm_ctrl%d_mdl1.h5'
    swarm_ctrl_param = 'MFRMFP_swarm_ctrl%d_params1.pkl'
    abs_enc_file = 'MFRMFP_abs_enc_mdl1.h5'
    abs_enc_param = 'MFRMFP_abs_enc_params1.pkl'
    abs_dyn_file = 'MFRMFP_abs_dyn_mdl1.h5'
    abs_dyn_param = 'MFRMFP_abs_dyn_params1.pkl'
    aux_enc_file = 'MFRMFP_aux_enc_mdl1.h5'
    aux_enc_param = 'MFRMFP_aux_enc_params1.pkl'
    training_params = 'dataset2_4Rob_params.pkl'
    training_limits = 'dataset2_4Rob_limits.pkl'

    # experiment params
    parent_state_size = 2
    swarm_state_size = 2
    swarm_input_size = 1
    abs_size = 3
    aux_size = 5
    n_steps = 2
    parent_ctrl_layers = [10]
    parent_ctrl_activ = ['tanh']
    parent_ctrl_activ_final = 'linear'
    swarm_ctrl_layers = [30]
    swarm_ctrl_activ = ['tanh']
    swarm_ctrl_activ_final = 'linear'
    abs_enc_layers = [10]
    abs_enc_activ = ['tanh']
    abs_enc_activ_final = 'linear'
    aux_enc_layers = [10]
    aux_enc_activ = ['tanh']
    aux_enc_activ_final = 'linear'
    abs_dyn_layers = [10]
    abs_dyn_activ = ['tanh']
    abs_dyn_activ_final = 'linear'
    abs_err_gain = 1.0
    name = 'full_context_trainer'
    indep_swarm_ctrls = True

    Q_p = np.array([[10, 0], [0, 1]])
    Q_pf = Q_p
    Q_si = np.array([[1, 0], [0, 1]])

    N = 100

    sys.setrecursionlimit(100000)

    # setup directories
    os.makedirs(results_dir, exist_ok=True)

    # load training data
    train_dat = data.MFRMFPSys(training_params, training_limits)
    sys_update = train_dat.RK4_keras

    # train network
    network = NN.TASC_Policy_Trainer(
        parent_state_size=parent_state_size,
        swarm_state_size=swarm_state_size,
        swarm_input_size=swarm_input_size,
        swarm_size=train_dat.N,
        abs_size=abs_size,
        aux_size=aux_size,
        n_steps=n_steps,
        parent_ctrl_layers=parent_ctrl_layers,
        parent_ctrl_activ=parent_ctrl_activ,
        parent_ctrl_activ_final=parent_ctrl_activ_final,
        swarm_ctrl_layers=swarm_ctrl_layers,
        swarm_ctrl_activ=swarm_ctrl_activ,
        swarm_ctrl_activ_final=swarm_ctrl_activ_final,
        abs_enc_layers=abs_enc_layers,
        abs_enc_activ=abs_enc_activ,
        abs_enc_activ_final=abs_enc_activ_final,
        aux_enc_layers=aux_enc_layers,
        aux_enc_activ=aux_enc_activ,
        aux_enc_activ_final=aux_enc_activ_final,
        abs_dyn_layers=abs_dyn_layers,
        abs_dyn_activ=abs_dyn_activ,
        abs_dyn_activ_final=abs_dyn_activ_final,
        sys_update=sys_update,
        Q_p=Q_p,
        Q_pf=Q_pf,
        Q_si=Q_si,
        abs_err_gain=abs_err_gain,
        name=name,
        home=results_dir,
        indep_swarm_ctrls=indep_swarm_ctrls
        )

    network.set_net_files(parent_ctrl_file, abs_enc_file, aux_enc_file, abs_dyn_file, swarm_ctrl_file)
    network.save_all_params(parent_ctrl_param, abs_enc_param, aux_enc_param, abs_dyn_param, swarm_ctrl_param)

    xs_0 = train_dat.gen_swarm_states(N)
    xp_0 = train_dat.gen_parent_states(N)
    xp_goal = np.zeros((2, N))
    xs_nom = np.zeros((train_dat.N*2, N))
    xe_nom = np.zeros((2, N))
    ae_nom = np.zeros((abs_size, N))

    network.train([xp_0.T, xs_0.T], [xp_goal.T, xs_nom.T, xe_nom.T, ae_nom.T], val_split=0.2, epochs=1000, batch_size=10, verbosity=2, patience=10, checkpoint=False)

    # save trained networks
    network.save_parent_ctrl(parent_ctrl_file, parent_ctrl_param)
    network.save_swarm_ctrl(swarm_ctrl_file, swarm_ctrl_param)
    network.save_abs_enc(abs_enc_file, abs_enc_param)
    network.save_abs_dyn(abs_dyn_file, abs_dyn_param)
    network.save_aux_enc(aux_enc_file, aux_enc_param)
