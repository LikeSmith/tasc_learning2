'''
train LTSM to learn specific function
'''

import numpy as np
from keras.layers import Input, Concatenate, LSTM
from keras.models import Model
from keras.callbacks import EarlyStopping
from tasc_learning.models import mlp
import keras.backend as KK
import matplotlib.pyplot as plt

def func(x, y, output_size):
    assert x.shape[0:2] == y.shape[0:2]
    assert output_size >= 2
    ret = np.zeros((n, output_size))
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            for k in range(j+1, x.shape[1]):
                ret[i, 0] += (y[i, j, 0]+y[i, k, 0])*(x[i, j, 0]-x[i, k, 0])
            ret[i, 1] = y[i, j, 0]*x[i, j, 0]

    return ret

def mod_se(y_true, y_pred):
    err = y_true[:, 0:2] - y_pred[:, 0:2]
    return KK.batch_dot(err, err, axes=1)

if __name__ == '__main__':
    N = 10
    x_size = 1
    y_size = 1
    output_size = 2
    hidden_size = 50
    layers = [200, 100, 50, 20, 10]
    activ = ['relu', 'relu', 'relu', 'relu', 'relu']
    final_activ = 'linear'
    n = 100000
    x_min = -10
    x_max = 10
    y_min = 0
    y_max = 1

    x_in = Input(shape=(None, x_size,))
    y_in = Input(shape=(None, y_size,))
    full_state_in = Concatenate()([x_in, y_in])
    encoder = LSTM(hidden_size, dropout=0.0, recurrent_dropout=0.0)
    decoder = mlp.MLP(input_size=hidden_size, output_size=output_size, hidden_size=layers, hidden_activation=activ, output_activation=final_activ)
    hidden = encoder(full_state_in)
    f_out = decoder(hidden)

    mdl = Model([x_in, y_in], f_out)
    mdl.compile(optimizer='adam', loss='mse')

    x_dat = x_min + (x_max - x_min)*np.random.rand(n, N, x_size)
    y_dat = y_min + (y_max - y_min)*np.random.rand(n, N, y_size)
    f_dat = func(x_dat, y_dat, output_size)

    cb = [EarlyStopping(monitor='val_loss', patience=100)]
    hist = mdl.fit([x_dat, y_dat], f_dat, batch_size=128, epochs=1000, validation_split=0.2, verbose=1, callbacks=cb)

    print('Lowest val loss: %f'%min(hist.history['val_loss']))
    print('Lowest loss: %f'%min(hist.history['loss']))

    plt.figure()
    plt.plot(np.array([hist.history['loss'], hist.history['val_loss']]).T)
    plt.xlabel('epoch')
    plt.legend(['loss', 'val loss'])
    plt.show()
