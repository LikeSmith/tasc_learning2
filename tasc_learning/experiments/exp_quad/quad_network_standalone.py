'''
Here is the example for setting up a neural network to learn a
quadratic function
'''

import numpy as np
from keras.layers import Input, Dense
from keras.models import Model
from keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # network hyperparameters
    n_hidden_nodes = 10
    # set up layers
    x = Input(shape=(1,), name='quad_learner_x')
    h = Dense(n_hidden_nodes, activation='tanh', name='quad_learner_h')(x)
    y = Dense(1, activation='linear', name='quad_learner_y')(h)

    # build model
    mdl = Model(x, y)
    mdl.compile(optimizer='rmsprop', loss='mse')

    # set up training data
    x_data = -10.0 + 20.0*np.random.rand(10000, 1)
    y_data = np.power(x_data, 2)

    # train network
    es = EarlyStopping(monitor='val_loss', patience=100)
    mdl.fit(x_data, y_data, batch_size=10, epochs=1000, validation_split=0.2, callbacks=[es])

    # plot results
    x_pred = np.linspace(-15, 15, 100)
    y_pred = mdl.predict(x_pred.reshape(100, 1))

    plt.figure()
    plt.plot(x_data, y_data, 'rx')
    plt.plot(x_pred, y_pred, 'b')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

    # save network
    mdl.save('quad_network_standalone.h5')
