'''
This script tries to learn a quadratic equation
'''

import numpy as np
import tasc_learning.models.mlp as NN
import matplotlib.pyplot as plt

if __name__ == '__main__':
    net = NN.MLP(1, 1, 1, [10], ['tanh'])

    x = -10.0 + 20.0*np.random.rand(10000, 1)
    y = np.power(x, 2)

    net.train(x, y, val_split=0.2, epochs=10000, verbosity=1, patience=100)

    x_p = np.linspace(-15, 15, 100)
    y_p = net.get(x_p.reshape(100, 1))

    plt.figure()
    plt.plot(x, y, 'rx')
    plt.plot(x_p, y_p)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

    net.save('../../..//data/Quadratic/results/quad_aprox5.h5')
