'''
evaluate the trained network
'''

import numpy as np
import tasc_learning.models.mlp as NN
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

if __name__ == '__main__':
    net = NN.MLP(1, 1, 1, [5], ['tanh'], from_file=True, file_path='../../..//data/Quadratic/results/quad_aprox.h5')

    x = np.linspace(-15, 15, 100)
    y_a = x**2
    y_p = net.get(x.reshape(100, 1))

    plt.figure()
    plt.plot(x, y_a, 'rx')
    plt.plot(x, y_p)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

    W1 = net.mdl.layers[1].get_weights()[0].T
    b1 = net.mdl.layers[1].get_weights()[1].T
    W2 = net.mdl.layers[2].get_weights()[0].T
    b2 = net.mdl.layers[2].get_weights()[1].T

    y_in = np.array([4.0])

    U1, Sig1, V1 = np.linalg.svd(W1)
    U2, Sig2, V2 = np.linalg.svd(W2)

    u_11 = np.expand_dims(U1[:, 0], 1)
    M = Sig1[0]*np.eye(W1.shape[0]) - W1*u_11.T

    U, S, V = np.linalg.svd(M)
    ker = V[:, -1]

    x_c2 = np.dot(U2[:, 0], y_in - b2)/Sig2[0]*V2[:, 0]

    f = lambda c: np.tanh(c[9]*ker + b1) - x_c2 - np.dot(V2[:, 1:], c[0:9])
    c0 = np.zeros(10)
    c = fsolve(f, c0)

    y1 = c[9]*ker + b1s
    #import pdb; pdb.set_trace()
