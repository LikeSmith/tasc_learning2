'''
Script for testing learned LOL controller
'''

import numpy as np
from tasc_learning.models import Basic_CTRL
import tasc_learning.datasets.DiffDrive as data
from mpl_toolkits.mplot3d import axis3d
import matplotlib.pyplot as plt
import matplotlib
import os
import sys
import getopt

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

def help():
    print('Usage: python3 test_LOL_DiffDrive.py [options]')
    print('tests trained using LOL on the DiffDrive system')
    print('Options:')
    print('-r [--res_dir]:    results direcotry (../../../data/DiffDrive/results)')
    print('-d [--dataset_no]: dataset number (1)')
    print('-e [--exp_no]:     experiment number (1)')
    print('-t [--test_no]:    test number (1)')
    print('-L [--load]:       load test parameters from file')
    print('-l [--limit]:      apply hard velocity limits to inputs, limits specified in dataset')
    print('-p [--for_print]:  format figures for print (don\'t use funny fonts)')
    print('-S [--save_figs]:  save figures')
    print('-s [--show_figs]:  show figures')
    print('-T [--sim_time]:   time for simulation (10)')
    print('-v [--vecfield]:   plot vector field, provide density')
    print('-h [--help]:       display this help')

if __name__ == '__main__':
    # input/output info
    results_dir = '../../../data/DiffDrive/results/'
    dataset_no = 1
    exp_no = 1
    test_no = 1
    for_print = False
    save_figs = False
    show_figs = False
    load_param = False
    input_limit = False

    # simulation parameters
    tf = 10.0
    p = -1
    x_0 = np.array([[1, 1, 0]])
    use_grid = False
    grid_size = np.array([1, 1, 0])
    grid_n = np.array([4, 4, 1])
    vec_dens = -1

    # handle command line options
    try:
        opts, args, = getopt.getopt(sys.argv[1:], 'r:d:e:t:LlpSsT:v:h', ['res_dir=', 'dataset_no=', 'exp_no', 'test_no=', 'load', 'limit', 'for_print', 'save_figs', 'show_figs', 'sim_time=', 'vecfield=' 'help'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    for [opt, arg] in opts:
        if opt in ['-r', '--res_dir']:
            results_dir = arg
        elif opt in ['-d', '--dataset_no']:
            dataset_no = int(arg)
        elif opt in ['-e', '--exp_no']:
            exp_no = int(arg)
        elif opt in ['-t', '--test_no']:
            test_no = int(arg)
        elif opt in ['-L', '--load']:
            load_param = True
        elif opt in ['-l', '--limit']:
            input_limit = True
        elif opt in ['-p', '--for_print']:
            for_print = True
        elif opt in ['-S', '--save_figs']:
            save_figs = True
        elif opt in ['-s', '--show_figs']:
            show_figs = True
        elif opt in ['-T', '--sim_time']:
            tf = float(arg)
        elif opt in ['-v', '--vecfield']:
            vec_dens = int(arg)
        elif opt in ['-h', '--help']:
            help()
            sys.exit(0)

    # Load test parameters if necessary
    if load_param:
        print('Loading test params...')
        tf, p, x_0, use_grid, grid_size, grid_n = pkl.load(open('params/test%d_params.pkl'%(test_no,), 'rb'))

    # geberate file names
    param_file = 'DiffDrive_dataset%d_exp%d_ctrl_param.pkl'%(dataset_no, exp_no)
    netwk_file = 'DiffDrive_dataset%d_exp%d_ctrl_model.h5'%(dataset_no, exp_no)
    training_params = 'dataset%d_params.pkl'%(dataset_no,)
    training_limits = 'dataset%d_limits.pkl'%(dataset_no,)

    # figure files
    fig_pos_filename = 'pos_exp%d_test%d.png'%(exp_no, test_no)
    fig_ang_filename = 'ang_exp%d_test%d.png'%(exp_no, test_no)
    fig_vel_filename = 'vel_exp%d_test%d.png'%(exp_no, test_no)
    fig_xyp_filename = 'xyp_exp%d_test%d.png'%(exp_no, test_no)

    # load dataset
    data_manager = data.DiffDrive(training_params, training_limits)
    data_manager.set_input_limit(input_limit)

    # load controller
    ctrl = Basic_CTRL(param_file=param_file, net_file=netwk_file, home=results_dir)

    # generate grid in necessary
    if use_grid:
        x_pos = np.linspace(-grid_size[0], grid_size[0], grid_n[0])
        y_pos = np.linspace(-grid_size[1], grid_size[1], grid_n[1])
        t_pos = np.linspace(-grid_size[2], grid_size[2], grid_n[2])

        x_0 = np.zeros((grid_n[0]*grid_n[1]*grid_n[2], 3))

        for i in range(grid_n[0]):
            for j in range(grid_n[1]):
                for k in range(grid_n[2]):
                    x_0[i*grid_n[1]*grid_n[2]+j*grid_n[2]+k, :] = np.array([x_pos[i], y_pos[j], t_pos[k]])

    # generate p in necessary
    if len(x_0.shape) == 1:
        x_0 = np.expand_dims(x_0)

    if p == -1:
        p = data_manager.gen_params(x_0.shape[0])

    assert p.shape[0] == x_0.shape[0]

    # setup simulation
    t = np.arange(0.0, tf, data_manager.del_t)
    x = np.zeros((x_0.shape[0], t.shape[0], 3))
    u = np.zeros((x_0.shape[0], t.shape[0], 2))

    x[:, 0, :] = x_0

    for i in range(t.shape[0]-1):
        u[:, i, :] = ctrl.eval(x[:, i, :], p)
        x[:, i+1, :] = data_manager.f_disc(x[:, i, :], u[:, i, :], p)

    if save_figs:
        if for_print:
            matplotlib.rcParams['pdf.fonttype'] = 42
            matplotlib.rcParams['ps.fonttype'] = 42

        try:
            os.makedirs(results_dir+'figs/', exist_ok=True)
        except TypeError:
            try:
                os.makedirs(results_dir+'figs/')
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise

    plt.figure()
    plt.plot(t, np.concatenate([x[:, :, 0], x[:, :, 1]]).T)
    plt.xlabel('time (s)')
    plt.ylabel('pos (m)')
    plt.legend(['x pos', 'y_pos'])
    if save_figs:
        plt.savefig(results_dir+'figs/'+fig_pos_filename)

    plt.figure()
    plt.plot(t, x[:, :, 2].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')
    if save_figs:
        plt.savefig(results_dir+'figs/'+fig_ang_filename)

    plt.figure()
    plt.plot(t, np.concatenate([u[:, :, 0], u[:, :, 1]]).T)
    plt.xlabel('time (s)')
    plt.ylabel('vel (m/s)')
    plt.legend(['left vel', 'right vel'])
    if save_figs:
        plt.savefig(results_dir+'figs/'+fig_vel_filename)

    plt.figure()
    plt.plot(x[:, :, 0].T, x[:, :, 1].T)
    plt.xlabel('y pos (m)')
    plt.ylabel('x pos (m)')
    if save_figs:
        plt.savefig(results_dir+'figs/'+fig_xyp_filename)


    # plot vector field
    if vec_dens > 0:
        pts = np.array(np.meshgrid(np.linspace(-1, 1, vec_dens)*grid_size[0], np.linspace(-1, 1, vec_dens)*grid_size[1], np.linspace(-1, 1, vec_dens)*grid_size[2]))
        vecs = np.zeros(pts.shape)

        for i in range(vec_dens):
            for j in range(vec_dens):
                for k in range(vec_dens):
                    x0 = pts[:, i, j, k]
                    u = ctrl.eval(x0, np.array([0.1]))
                    x1 = data_manager.f_disc(x0, u, np.array([0.1]))
                    vecs[:, i, j, k] = x1 - x0

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.quiver(pts[0, :, :], pts[1, :, :], pts[2, :, :], vecs[0, :, :], vecs[1, :, :], vecs[2, :, :], length=0.1)
        #ax.scatter(pts[0, :, :], pts[1, :, :], pts[2, :, :])

    if show_figs:
        plt.show()
