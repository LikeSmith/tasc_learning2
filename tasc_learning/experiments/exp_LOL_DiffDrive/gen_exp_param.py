'''
Generates experimental parameters for training
'''

import numpy as np
import os
import errno

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

exp_no = 3

# parameters
state_size = 3
input_size = 2
param_size = 1

# controller parameters
ctrl_layers = [100, 50, 10]
ctrl_activ = 'leakyRelu0_01'
ctrl_activ_final = 'tanh'

# loss parameters
gamma_neg = 100.0
Q = np.diag([0.0, 0.0, 0.0])
R = np.eye(2)*0.0

# training params
samples = 50000
subsamples = 50000
subsample_r = 0.5
epochs = 1000
patience = 2
batch_size = 256
horizon = 100


train_params = [state_size, input_size, param_size, ctrl_layers, ctrl_activ, ctrl_activ_final, gamma_neg, Q, R, samples, subsamples, subsample_r, epochs, patience, batch_size, horizon]

try:
    os.makedirs('params/', exist_ok=True)
except TypeError:
    try:
        os.makedirs('params/')
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

pkl.dump(train_params, open('params/exp%d_params.pkl'%(exp_no,), 'wb'))
