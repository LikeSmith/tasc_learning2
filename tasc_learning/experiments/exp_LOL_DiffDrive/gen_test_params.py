'''
Generates parameters for experiment
'''

import numpy as np
import os
import errno

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

test_no = 2

tf = 5.0
p = -1
x_0 = np.array([[1, 1, 0]])
use_grid = True
grid_size = np.array([1, 1, np.pi])
grid_n = np.array([5, 5, 5])

test_params = [tf, p, x_0, use_grid, grid_size, grid_n]

try:
    os.makedirs('params/', exist_ok=True)
except TypeError:
    try:
        os.makedirs('params/')
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

pkl.dump(test_params, open('params/test%d_params.pkl'%(test_no,), 'wb'))
