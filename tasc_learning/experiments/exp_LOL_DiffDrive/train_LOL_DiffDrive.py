'''
trains controller using Lyapunov Optimized learning
'''

import numpy as np
from tasc_learning.models import Basic_CTRL, LOL_Trainer
import tasc_learning.datasets.DiffDrive as data
import keras.backend as KK
import matplotlib.pyplot as plt

import os
import sys
import getopt

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

def help():
    print('Usage: python3 test_tasc_modular.py [options]')
    print('uses LOL to train controller on the DiffDrive system')
    print('Options:')
    print('-r [--res_dir]:    results direcotry (../../../data/DiffDrive/results)')
    print('-d [--dataset_no]: dataset number (1)')
    print('-e [--exp_no]:     experiment number (1)')
    print('-L [--load]:       load experimental params from file')
    print('-l [--limit]:      apply hard velocity limits to inputs, limits specified in dataset')
    print('-c [--comp]:       use lyapunov function that compensates for singular points')
    print('-g [--gen_force]:  force a new dataset to be generated')
    print('-G [--gen_save]:   save any dataset generated')
    print('-s [--shuffle]:    shuffle data before training')
    print('-n [--name]:       name for model (TASC_RNN_mod)')
    print('-h [--help]:       display this help')

def lyap1(x):
    p_x = x[:, 0]
    p_y = x[:, 1]
    return KK.square(p_x) + KK.square(p_y)

def lyap2(x):
    p_x = KK.expand_dims(x[:, 0])
    p_y = KK.expand_dims(x[:, 1])
    theta = KK.expand_dims(x[:, 2])
    return KK.square(p_x) + KK.square(p_y) + KK.square(KK.batch_dot(p_x, KK.sin(theta)) - KK.batch_dot(p_y, KK.cos(theta)))

if __name__ == '__main__':
    # Experimetn Info
    results_dir = '../../../data/DiffDrive/results/'
    dataset_no = 1
    exp_no = 1
    name = 'LOL_DiffDrive'
    shuf = False
    input_limit = False

    # Experiment Params
    state_size = 3
    input_size = 2
    param_size = 1

    # controller parameters
    ctrl_layers = [10]
    ctrl_activ = 'relu'
    ctrl_activ_final = 'linear'

    # loss parameters
    gamma_neg = 1.0
    Q = np.diag([10.0, 1.0, 0.0])
    R = np.eye(2)*1.0

    # training params
    samples = 10000
    subsamples = 10000
    subsample_r = 0.5
    epochs = 100
    patience = 10
    batch_size = 128
    horizon = 10
    lyap = lyap1
    u_lims = None

    try:
        opts, args, = getopt.getopt(sys.argv[1:], 'r:d:e:LlcgGsn:h', ['res_dir=', 'dataset_no=', 'exp_no=', 'load', 'limit', 'comp', 'gen_force', 'gen_save', 'shuffle', 'name=', 'help'])
    except getopt.GetoptError as err:
        print('Error reading opts...')
        print(err.msg)
        help()
        sys.exit(2)

    load = False
    gen_force = False
    gen_save = False

    for [opt, arg] in opts:
        if opt in ['-r', '--res_dir']:
            results_dir = arg
        elif opt in ['-d', '--dataset_no']:
            dataset_no = int(arg)
        elif opt in ['-e', '--exp_no']:
            exp_no = int(arg)
        elif opt in ['-L', '--load']:
            load = True
        elif opt in ['-l', '--limit']:
            input_limit = True
        elif opt in ['-c', '--comp']:
            lyap = lyap2
        elif opt in ['-g', '--gen_force']:
            gen_force = True
        elif opt in ['-G', '--gen_save']:
            gen_save = True
        elif opt in ['-s', '--shuffle']:
            shuf = True
        elif opt in ['-n', '--name']:
            name = arg
        elif opt in ['-h', '--help']:
            help()
            sys.exit(0)

    if load:
        print('Loading experiment parameters...')
        state_size, input_size, param_size, ctrl_layers, ctrl_activ, ctrl_activ_final, gamma_neg, Q, R, samples, subsamples, subsample_r, epochs, patience, batch_size, horizon = pkl.load(open('params/exp%d_params.pkl'%(exp_no,), 'rb'))

    # filename generation
    param_file = 'DiffDrive_dataset%d_exp%d_ctrl_param.pkl'%(dataset_no, exp_no)
    netwk_file = 'DiffDrive_dataset%d_exp%d_ctrl_model.h5'%(dataset_no, exp_no)
    training_params = 'dataset%d_params.pkl'%(dataset_no,)
    training_limits = 'dataset%d_limits.pkl'%(dataset_no,)

    # load dataset
    data_manager = data.DiffDrive(training_params, training_limits)

    data_manager.set_input_limit(input_limit)

    print('Getting training dataset...')
    if gen_force:
        data_manager.generate_dataset(samples, subsamples, subsample_r)
        if gen_save:
            data_manager.save_dataset(data_manager.gen_filename(samples, dataset_no, subsamples, subsample_r))
    elif (data_manager.autoload(samples, dataset_no, subsamples, subsample_r) and gen_save):
        data_manager.save_dataset(data_manager.gen_filename(samples, dataset_no, subsamples, subsample_r))

    if shuf:
        data_manager.shuffle()

    #plt.figure()
    #plt.plot(data_manager.x_0[:, 0], data_manager.x_0[:, 1], 'b*')
    #plt.figure()
    #plt.plot(data_manager.x_0[:, 1], data_manager.x_0[:, 2], 'b*')
    #plt.figure()
    #plt.plot(data_manager.x_0[:, 2], data_manager.x_0[:, 0], 'b*')

    #plt.show()

    print('setting up neural networks...')

    # Set up controller
    ctrl = Basic_CTRL(state_size=state_size, input_size=input_size, param_size=param_size, layers=ctrl_layers, activ=ctrl_activ, final_activ=ctrl_activ_final, home=results_dir, name=name+'_ctrl')

    # Set up Trainer
    trainer = LOL_Trainer(state_size, input_size, param_size, data_manager.f_disc_keras, ctrl, lyap, gamma_neg=gamma_neg, aux_loss=LOL_Trainer.loss_aux_lqr(Q, R), name=name+'_LOL_trainer', home=results_dir, ctrl_net_save=netwk_file, ctrl_par_save=param_file)

    print('Training network...')
    # Train network
    trainer.train(data_manager.x_0, data_manager.p, horizon=horizon, val_split=0.2, epochs=epochs, batch_size=batch_size, verbosity=1, patience=patience, checkpoint=False)

    trainer.save_ctrl_par()
    trainer.save_ctrl_mdl()

    print('Training Complete, Results saved.')
