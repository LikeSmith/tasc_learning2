%
% Sliding Mode controller on inverted pendulum
%

clear all
close all

m = 10;
l = 1;
g = 9.81;
b = 1.0;

beta_0 = 0.1;
k = 1.5;
k2 = 1;

theta_0 = pi;
omega_0 = 0.0;

t_f = 10.0;
del_t = 0.01;

t = 0.0:del_t:t_f;
x = zeros(length(t), 2);
s = zeros(length(t));
u = zeros(length(t));

x(1, :) = [theta_0, omega_0];

for i=1:length(t)-1
	s(i) = m*g*l*(sin(x(i, 1))-1) + 0.5*m*l^2*x(i, 2)^2;
	u(i) = -(k*(m*g*sqrt(1+l^2)*abs(x(i, 2))) + beta_0)*tanh(k2*s(i));
	x(i+1, 2) = x(i, 2) + del_t*(g/l*sin(x(i, 1)) - b*x(i, 2) + u(i)/(m*l^2));
	x(i+1, 1) = x(i, 1) + del_t*x(i+1, 2);
end

s(end) = m*g*l*(sin(x(end, 1))-1) + 0.5*m*l^2*x(end, 2)^2;
u(end) = -(k*(m*g*sqrt(1+l^2)*abs(x(end, 2))) + beta_0)*tanh(k2*s(end));

figure(1);
plot(t, x);
xlabel('time (s)');
legend('theta', 'omega');

figure(2);
plot(t, s);
xlabel('time (s)');
ylabel('s');

figure(3);
plot(t, u);
xlabel('time (s)');
ylabel('u');

