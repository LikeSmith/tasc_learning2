'''
test rnn utilities
'''

import numpy as np
from keras.layers import RNN, Input, Layer
from keras.models import Model
from keras.callbacks import EarlyStopping
import keras.backend as KK
from tasc_learning.models.mlp import MLP

def func(x, A, B):
    y = np.zeros([x.shape[0], x.shape[1]+1, B.shape[0]])

    for i in range(x.shape[1]):
        y[:, i+1, :] = np.dot(x[:, i, :], A) + np.dot(y[:, i, :], B)

    return y[:, -1, :]

class MLP_Cell(Layer):
    def __init__(self,
        input_size=1,
        layers=[10],
        activ='relu',
        name='mlp_recur',
        **kwargs):
        self.name = name

        self.input_size = input_size
        self.layers_1 = layers
        self.activ_1 = activ
        self.state_size = self.layers_1[-1]

        if not isinstance(self.activ_1, list):
            self.activ = [self.activ_1]*len(self.layers_1)

        super(MLP_Cell, self).__init__(**kwargs)

    def build(self, input_shape):
        assert input_shape[-1] == self.input_size

        # set up MLP
        self.mlp = MLP(input_size=self.input_size+self.layers_1[-1], output_size=self.layers_1[-1], hidden_size=self.layers_1[:-1], hidden_activation=self.activ_1[:-1], output_activation=self.activ_1[-1], name='%s_mlp'%self.name)

        self.trainable_weights = self.mlp.mdl.weights

        self.built = True

    def call(self, inputs, states):
        combined_state = KK.concatenate([inputs, states[0]])

        out = self.mlp(combined_state)

        return out, [out]

if __name__ == '__main__':
    input_size = 5
    layers = [2]
    activ = ['linear']
    n = 100000
    horizon = 2

    cell = MLP_Cell(input_size=input_size, layers=layers, activ=activ, name='test_mlp')
    rnn = RNN(cell)

    x_in = Input(shape=(None, input_size), name='x_in')
    y_out = rnn(x_in)

    mdl = Model(x_in, y_out)
    mdl.compile(optimizer='adam', loss='mse')

    # generate test data:
    state_size = layers[-1]
    A = 10*np.random.rand(input_size, state_size) - 5
    B = 10*np.random.rand(state_size, state_size) - 5
    x = 10*np.random.rand(n, horizon, input_size) - 5
    y = func(x, A, B)

    mdl.fit(x, y, batch_size=128, epochs=100, verbose=1, callbacks=[EarlyStopping(monitor='val_loss', patience=10)], validation_split=0.2)

    print(A)
    print(B)
    print(mdl.layers[1].cell.mlp.mdl.get_weights())
