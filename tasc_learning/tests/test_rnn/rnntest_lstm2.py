'''
test rnn utilities
'''

import numpy as np
from keras.layers import RNN, Input, Lambda, LSTM, Layer, Concatenate
from keras.models import Model
from keras.callbacks import EarlyStopping
import keras.backend as KK
from tasc_learning.models.mlp import MLP

def func(x, A1, B1, A2, B2):
    y = np.zeros([x.shape[0], x.shape[1]+1, B2.shape[0]])

    for i in range(x.shape[1]):
        h = np.zeros([x.shape[0], x.shape[2]+1, B1.shape[0]])
        for j in range(x.shape[2]):
            h[:, j+1, :] = np.dot(x[:, i, j, :], A1) + np.dot(h[:, j, :], B1)

        y[:, i+1, :] = np.dot(h[:, -1, :], A2) + np.dot(y[:, i, :], B2)

    return y[:, -1, :]

class LSTM_Cell(Layer):
    def __init__(self,
        state_size=1,
        swarm_size=1,
        lstm_output_size=1,
        layers=[10],
        activ='relu',
        name='lstm_recur',
        **kwargs):
        self.name = name
        self.state_size_1 = state_size
        self.swarm_size = swarm_size
        self.lstm_output_size = lstm_output_size
        self.layers_1 = layers
        self.activ_1 = activ
        self.state_size = self.layers_1[-1]

        if not isinstance(self.activ_1, list):
            self.activ = [self.activ_1]*len(self.layers_1)

        super(LSTM_Cell, self).__init__(**kwargs)

    def build(self, input_shape):
        # set up LSTM
        self.lstm_lay = LSTM(lstm_output_size)

        # set up MLP
        self.mlp_lay = MLP(input_size=self.lstm_output_size+self.layers_1[-1], output_size=self.layers_1[-1], hidden_size=self.layers_1[:-1], hidden_activation=self.activ_1[:-1], output_activation=self.activ_1[-1], name='%s_mlp'%self.name)

        # set up unpacking layer
        self.upack_lay = Lambda(lambda state: LSTM_Cell.unpack_state(state, self.state_size_1), output_shape=(self.swarm_size, self.state_size_1))

        # set up model
        self.x_pack = Input(shape=(self.state_size_1*self.swarm_size,))
        self.states = Input(shape=(self.layers_1[-1],))
        self.x_upack = self.upack_lay(self.x_pack)
        self.lstm_out = self.lstm_lay(self.x_upack)
        self.concat_in = Concatenate()([self.lstm_out, self.states])
        self.y = self.mlp_lay(self.concat_in)

        self.mdl = Model([self.x_pack, self.states], self.y)
        self.mdl.compile(optimizer='adam', loss='mse')

        self.trainable_weights = self.mdl.weights

        self.built = True

    def call(self, inputs, states):
        out = self.mdl([inputs, states[0]])
        return out, [out]

    @staticmethod
    def pack_state(states_upack, swarm_size):
        return KK.concatenate([states_upack[:, :, i, :] for i in range(swarm_size)])

    @staticmethod
    def unpack_state(states_pack, state_size):
        z = KK.expand_dims(states_pack)
        return KK.concatenate([z[:, i::state_size, :] for i in range(state_size)])

if __name__ == '__main__':
    state_size = 5
    lstm_output_size = 4
    layers = [10, 2]
    activ = ['relu', 'linear']
    n = 10000
    horizon1 = 10
    horizon2 = 4

    cell = LSTM_Cell(state_size=state_size, swarm_size=horizon2, lstm_output_size=lstm_output_size, layers=layers, activ=activ, name='test_mlp')
    rnn = RNN(cell)
    pack = Lambda(lambda x: LSTM_Cell.pack_state(x, horizon2), output_shape=(None, None, horizon2*state_size))

    x_in = Input(shape=(None, None, state_size), name='x_in')
    x_pack = pack(x_in)
    y_out = rnn(x_pack)

    mdl = Model(x_in, y_out)
    mdl.compile(optimizer='adam', loss='mse')

    # generate test data:
    output_size = layers[-1]
    A1 = 10*np.random.rand(state_size, lstm_output_size) - 5
    B1 = 10*np.random.rand(lstm_output_size, lstm_output_size) - 5
    A2 = 10*np.random.rand(lstm_output_size, output_size) - 5
    B2 = 10*np.random.rand(output_size, output_size) - 5
    x = 10*np.random.rand(n, horizon1, horizon2, state_size) - 5
    y = func(x, A1, B1, A2, B2)

    mdl.fit(x, y, batch_size=128, epochs=1000, verbose=1, callbacks=[EarlyStopping(monitor='val_loss', patience=100)], validation_split=0.2)
