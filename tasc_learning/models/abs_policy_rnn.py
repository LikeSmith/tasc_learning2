'''
This program is for training an RNN without unrolling it as is 
commonly done with back prop through time. This approach is to
calculate the gradient of the error with respect to the input,
use that to calculate the error at the previous timestep, and then
calculate the update for the weights of the network.


'''

import functools
from operator import mul
import numpy as np
import keras.backend as KK
from keras.layers.merge import concatenate
from keras.models import Model
from keras.layers import Input, Lambda, Dense
import tasc_learning.models.mlp as mlp
import _pickle as pkl
import os
import tasc_learning.utils.DNN_derivatives as dnnd


backend='tensorflow'
backend='theano'


class AbsPolicy(object):
    def __init__(
            self, parent_state_size, swarm_state_size, abs_state_size,
            abs_enc_layers=[10], abs_enc_activ=['tanh'],
            abs_enc_activ_end='linear', abs_enc_activ_start='tanh',
            from_file=False,
            abs_enc_file='abs_enc.h5',
            abs_enc_params='abs_enc_params.pkl',
            name='abstraction_encoder', home=None,
            force_abs_state=False
    ):
        home = os.path.dirname(
            os.path.realpath(__file__)
        ) + '/../../data/results/'

        self.parent_state_size = parent_state_size
        self.swarm_state_size = swarm_state_size
        self.abs_state_size = abs_state_size
        self.name = name
        self.force_abs_state = force_abs_state

        if from_file:
            if home is not None:
                self.home = home

            self.abs_enc = mlp(
                input_size=swarm_state_size,
                output_size=abs_state_size,
                n_hidden=len(self.abs_enc_layers),
                hidden_size=self.abs_enc_layers,
                hidden_activation=self.abs_enc_activ,
                output_activation=self.abs_enc_activ_end,
                name=self.name+'abs_enc',
                from_file=True,
                file_path=self.home+abs_enc_file
            )
            (
                self.abs_dyn_layers,
                self.abs_dyn_activ,
                self.abs_enc_activ_end
            )= pkl.load(open(self.home+abs_dyn_params, 'rb'))

            self.abs_enc = mlp(
                input_size=swarm_state_size,
                output_size=abs_state_size,
                n_hidden=len(self.abs_enc_layers),
                hidden_size=self.abs_enc_layers,
                hidden_activation=self.abs_enc_activ,
                output_activation=self.abs_enc_activ_end,
                name=self.name+'abs_enc',
                from_file=True,
                file_path=self.home+abs_enc_file
            )
            self.abs_dyn = mlp(
                input_size=abs_state_size+parent_state_size,
                output_size=parent_state_size,
                n_hidden=len(self.abs_dyn_layers),
                hidden_size=self.abs_dyn_layers,
                hidden_activation=self.abs_dyn_activ,
                output_activation=self.abs_dyn_activ_end,
                name=self.name+'abs_dyn',
                from_file=True,
                file_path=self.home+abs_dyn_file
            )
        else:
            self.abs_enc_layers = abs_enc_layers
            self.abs_enc_activ = abs_enc_activ
            self.abs_enc_activ_end = abs_enc_activ_end
            self.abs_enc_activ_start = abs_enc_activ_start
            self.abs_dyn_layers = abs_dyn_layers
            self.abs_dyn_activ = abs_dyn_activ
            self.abs_dyn_activ_end = abs_dyn_activ_end

            self.abs_enc = mlp.MLP(
                input_size=swarm_state_size,
                output_size=abs_state_size,
                n_hidden=len(self.abs_enc_layers),
                hidden_size=self.abs_enc_layers,
                hidden_activation=self.abs_enc_activ,
                output_activation=self.abs_enc_activ_end,
                initial_activation=self.abs_enc_activ_start,
                name=self.name+'_abs_enc'
            )
            self.abs_dyn = mlp.MLP(
                input_size=abs_state_size+parent_state_size,
                output_size=parent_state_size,
                n_hidden=len(self.abs_dyn_layers),
                hidden_size=self.abs_dyn_layers,
                hidden_activation=self.abs_dyn_activ,
                output_activation=self.abs_dyn_activ_end,
                name=self.name+'_abs_dyn'
            )

        self.xp_0 = Input(
            shape=(self.parent_state_size,),
            name=self.name+'_xp_0'
        )
        self.abs_0 = Input(
            shape=(self.abs_state_size,),
            name=self.name+'_abs_0'
        )

        self.abs = self.abs_enc.mdl(self.xs_0)
        self.dyn_in = Concatenate(
            name=self.name+'_concat'
        )([self.xp_0, self.abs])
        self.xp_t = self.abs_dyn.mdl(self.dyn_in)
'''
        
class abs_policy_trainer(object):
    pass
'''

def quad(x, A, batch_size, transpose=True):
    batch_size = x.shape[0]

    x_l = x
    x_r = KK.transpose(x)

    if not transpose:
        a = x_l
        x_l = x_r
        x_r = a

    return KK.batch_dot(x, KK.transpose(KK.dot(A, KK.transpose(x))), 1)


def LQR_loss(x_a, x_d, u, Q, R, batch_size):
    return quad(x_a - x_d, Q, batch_size) + quad(u, R, batch_size)


def print_shape(obj, oidx=0):
    temp = obj
    if backend == 'tensorflow':
        temp = obj[oidx]
    print(temp.shape)


def init_mem(batch_size, shp):
    return np.zeros((batch_size, functools.reduce(mul, shp)))


def train_on_batch(model, grad_fn, abs_policy, horizon, batch_size,
                   parent_state_size, abs_state_size, x0_parent):
    wb1 = abs_policy.mdl.layers[1].get_weights()
    prev_w1_net_corr = init_mem(parent_state_size, wb1[0].shape)
    prev_b1_net_corr = init_mem(parent_state_size, wb1[1].shape)
    total_w1_cost_corr = init_mem(1, wb1[0].shape)
    total_b1_cost_corr = init_mem(1, wb1[1].shape)

    wb2 = abs_policy.mdl.layers[2].get_weights()
    prev_w2_net_corr = init_mem(parent_state_size, wb2[0].shape)
    prev_b2_net_corr = init_mem(parent_state_size, wb2[1].shape)
    total_w2_cost_corr = init_mem(1, wb2[0].shape)
    total_b2_cost_corr = init_mem(1, wb2[1].shape)

    # Create storage space for results
    xt_parents = np.zeros((horizon, batch_size, parent_state_size))
    xt_parents[0] = x0_parent
    xt_parent = x0_parent

    abs_controls = np.zeros((horizon-1, batch_size, abs_state_size))

    losses = np.zeros((horizon, batch_size, 1))
    total_loss = 0

    for hidx in np.arange(horizon-1):
        #print("hidx: ", hidx)

        xt_parent = xt_parents[hidx]

        #print("State: ", xt_parent)

        # Calculate control, predict next state, and calculate loss
        abs_control, x1_parent, loss = model.predict([xt_parent])

        #print("Control: ", abs_control)

        #print("Loss: ", loss)

        # Store results
        xt_parents[hidx+1] = x1_parent
        abs_controls[hidx] = abs_control
        losses[hidx] = loss
        total_loss += np.sum(loss)

        # Calculate gradients
        (grads_w1, grads_b1, grads_w2, grads_b2,
         grads_cost, grads_state_jac) = grad_fn([xt_parent])

        # Calculate gradient contribution of previous time steps
        w1_net_corr = grads_w1 + np.dot(grads_state_jac, prev_w1_net_corr)
        b1_net_corr = grads_b1 + np.dot(grads_state_jac, prev_b1_net_corr)
        w2_net_corr = grads_w2 + np.dot(grads_state_jac, prev_w2_net_corr)
        b2_net_corr = grads_b2 + np.dot(grads_state_jac, prev_b2_net_corr)

        '''
        print(w1_net_corr)
        print(w2_net_corr)
        print(b1_net_corr)
        print(b2_net_corr)
        '''

        # Calculate gradient of cost for this time step
        w1_cost_corr = np.dot(grads_cost, w1_net_corr)
        b1_cost_corr = np.squeeze(np.dot(grads_cost, b1_net_corr))
        w2_cost_corr = np.dot(grads_cost, w2_net_corr)
        b2_cost_corr = np.squeeze(np.dot(grads_cost, b2_net_corr))

        '''
        print(w1_cost_corr)
        print(w2_cost_corr)
        print(b1_cost_corr)
        print(b2_cost_corr)
        '''

        # Accumlate gradient values
        prev_w1_net_corr += w1_net_corr
        prev_b1_net_corr += b1_net_corr
        prev_w2_net_corr += w2_net_corr
        prev_b2_net_corr += b2_net_corr
        total_w1_cost_corr += w1_cost_corr
        total_b1_cost_corr += b1_cost_corr
        total_w2_cost_corr += w2_cost_corr
        total_b2_cost_corr += b2_cost_corr

    #print("total_w1_cost_corr: ", total_w1_cost_corr)

    alpha = 0.0000000001
    model.layers[1].layers[1].set_weights([
        wb1[0] - alpha*total_w1_cost_corr.reshape(wb1[0].shape),
        wb1[1] - alpha*total_b1_cost_corr.reshape(wb1[1].shape)
    ])
    model.layers[1].layers[2].set_weights([
        wb2[0] - alpha*total_w2_cost_corr.reshape(wb2[0].shape),
        wb2[1] - alpha*total_b2_cost_corr.reshape(wb2[1].shape)
    ])

    return xt_parents, abs_controls, losses, total_loss


if __name__ == '__main__':
    avg_jac = True

    results_dir = '../../data/MFRMFPSys/results/'
    abs_dyn_file = 'MFRMFP_abs_dyn_mdl1.h5'
    abs_dyn_params = 'MFRMFP_abs_dyn_params1.pkl'

    batch_size = 10
    parent_state_size = 2
    abs_state_size = 3
    teimsteps = 100
    theta_min = -0.16
    theta_max = 0.16
    omega_min = -0.5
    omega_max = 0.5

    (
        abs_dyn_layers,
        abs_dyn_activ,
        abs_dyn_activ_end
    ) = pkl.load(open(results_dir+abs_dyn_params, 'rb'))
    abs_dyn = mlp.MLP(
        input_size        = abs_state_size+parent_state_size,
        output_size       = parent_state_size,
        n_hidden          = len(abs_dyn_layers),
        hidden_size       = abs_dyn_layers,
        hidden_activation = abs_dyn_activ,
        output_activation = abs_dyn_activ_end,
        name              = 'abstraction_encoder_abs_dyn',
        from_file         = True,
        file_path         = results_dir+abs_dyn_file,
    )
    abs_dyn.lock()
    
    abs_policy = mlp.MLP(
        input_size        = parent_state_size,
        output_size       = abs_state_size,
        n_hidden          = 1,
        hidden_size       = [10],
        hidden_activation = ['tanh'],
        output_activation = 'linear',
        name              = 'abs_policy',
        from_file         = False,
    )

    #xp_des = np.array([[0, 0]]).astype('float32')
    xp_des = np.zeros((batch_size, 2)).astype('float32')
    #xp_act = np.random.rand(batch_size, 2).astype('float32')
    x0_parent = np.random.rand(
        batch_size, 2
    )*np.array(
        [theta_max - theta_min, omega_max - omega_min]
    ) + np.array([theta_min, omega_min])
    x0_parent = x0_parent.astype('float32')

    Q = np.eye(parent_state_size)
    Q_f = Q*0.1
    R = np.eye(abs_state_size)

    '''
    print("abs_dyn.trainable?")
    for layer in abs_dyn.mdl.layers:
        print(layer.name, ' ', layer.trainable)
    '''

    x0_parent_var = Input(
        shape = (parent_state_size,),
        name='x0_parent'
    )
    u_abs = abs_policy.mdl(x0_parent_var)
    x_combined = concatenate([x0_parent_var, u_abs])
    x1_parent = abs_dyn.mdl(x_combined)

    '''
    model = Model([x0_parent_var], [x1_parent])
    test = model.predict([xp_act])
    print(test)
    fn = KK.function([x0_parent_var], [dnnd.f_derivative_symb(model, 0, 0, batch_size)])
    print(fn([xp_act]))
    import ipdb; ipdb.set_trace()
    fn = KK.function([x0_parent_var], [dnnd.f_jacobian_symb(model, 0, 0, batch_size)])
    print(fn([xp_act]))
    import ipdb; ipdb.set_trace()
    '''

    #outputs = [u_abs, x1_parent]
    #return Model([x0_parent_var], outputs)

    #policy = build_model()

    #weights = policy.layers[1].layers[1].get_weights() \
    #          + policy.layers[1].layers[1].get_weights()

    u_act = np.random.rand(batch_size, 1)
    #u_, xp_ = policy([x0_parent_var])
    xp_d = KK.variable(xp_des)

    #import ipdb; ipdb.set_trace()

    def LQR_loss_callable(x1_parent):
        return LQR_loss(
            #xp_,
            x1_parent,
            xp_d,
            #u_,
            u_abs,
            KK.variable(Q),
            KK.variable(R),
            batch_size
        )

    loss_fn_abs = LQR_loss_callable(x0_parent_var)

    loss_fn = LQR_loss(
        #xp_,
        x1_parent,
        xp_d,
        #u_,
        u_abs,
        KK.variable(Q),
        KK.variable(R),
        batch_size
    )

    #print("LQR_immediate_loss")
    fn = KK.function(
        [x0_parent_var],#, xp_d],
        #policy.outputs + [loss_fn] + KK.gradients(loss_fn, xp_)
        [
            u_abs,
            x1_parent,
            loss_fn,
        ]
        # + KK.gradients(LQR_loss_callable(x0_parent_var), x0_parent_var) #+ [KK.gradients(abs_dyn.mdl.outputs, abs_dyn.mdl.layers[1].weights[0])]
    )
    #res = fn([xp_act])
    #print(res)

    fn = KK.function(
        [x0_parent_var],#, xp_des],
        #policy.outputs + [loss_fn] + KK.gradients(loss_fn, xp_)
        [
            u_abs,
            x1_parent,
            loss_fn,
        ]
    )
    '''
    res = fn([xp_act])
    print(res[0].shape)
    print(res[1].shape)
    print(res[2].shape)
    print(res)
    '''

    model = Model(
        [x0_parent_var],
        [
            u_abs,
            x1_parent,
            Lambda(lambda x: LQR_loss_callable(x))(x1_parent)
        ]
        #policy.outputs + [loss_fn] + KK.gradients(loss_fn, xp_)
    )

    #import ipdb; ipdb.set_trace()
    grads_cost_wrt_x1_parent = KK.gradients(KK.sum(model.output[2]), x1_parent)
    grads_cost_wrt_x1_parent = [KK.sum(grads_cost_wrt_x1_parent[0], axis=0, keepdims=True)]

    '''
    weights0 = KK.gradients(model.output[1], model.layers[1].layers[1].weights[0])
    biases0 = KK.gradients(model.output[1], model.layers[1].layers[1].weights[1])
    weights1 = KK.gradients(model.output[1], model.layers[1].layers[2].weights[0])
    biases1 = KK.gradients(model.output[1], model.layers[1].layers[2].weights[1])
    '''
    grads_w1_0 = KK.gradients(model.outputs[1][0], model.layers[1].layers[1].weights[0])
    grads_w1_1 = KK.gradients(model.outputs[1][1], model.layers[1].layers[1].weights[0])
    grads_b1_0 = KK.gradients(model.outputs[1][0], model.layers[1].layers[1].weights[1])
    grads_b1_1 = KK.gradients(model.outputs[1][1], model.layers[1].layers[1].weights[1])
    grads_w1 = KK.batch_flatten(KK.squeeze(KK.stack([grads_w1_0, grads_w1_1]), axis=1))
    grads_b1 = KK.squeeze(KK.stack([grads_b1_0, grads_b1_1]), axis=1)

    grads_w2_0 = KK.gradients(model.outputs[1][0], model.layers[1].layers[2].weights[0])
    grads_w2_1 = KK.gradients(model.outputs[1][1], model.layers[1].layers[2].weights[0])
    grads_b2_0 = KK.gradients(model.outputs[1][0], model.layers[1].layers[2].weights[1])
    grads_b2_1 = KK.gradients(model.outputs[1][1], model.layers[1].layers[2].weights[1])
    grads_w2 = KK.batch_flatten(KK.squeeze(KK.stack([grads_w2_0, grads_w2_1]), axis=1))
    grads_b2 = KK.squeeze(KK.stack([grads_b2_0, grads_b2_1]), axis=1)

    grads_wrt_weights = [grads_w1, grads_b1, grads_w2, grads_b2]
    '''
    fn = KK.function(
        [x0_parent_var],
        grads_wrt_weights
    )
    import ipdb; ipdb.set_trace()
    grads_wrt_weights = weights0 + biases0 + weights1 + biases1
    '''
    if avg_jac:
        grads_wrt_x0_parent = [KK.sum(dnnd.f_jacobian_symb(model, 0, 1, batch_size), axis=0)]
    else:
        grads_wrt_x0_parent = [dnnd.f_jacobian_symb(model, 0, 1, batch_size)]

    grad_fn = KK.function(
        [x0_parent_var],
        grads_wrt_weights + grads_cost_wrt_x1_parent \
        + grads_wrt_x0_parent
    )
    #temp = grad_fn([xp_act])
    #print(temp)

    horizon = 10
    n_epochs = 1000

    for eidx in np.arange(n_epochs):
        #current_weights = model.layers[1].layers[1].get_weights()
        #print("Current weights (", eidx, "):", current_weights)

        xt_parents, abs_controls, losses, total_loss = train_on_batch(
            model,
            grad_fn,
            abs_policy,
            horizon,
            batch_size,
            parent_state_size,
            abs_state_size,
            x0_parent
        )

        print("Loss ", eidx, ": ", total_loss)

    #final_weights = model.layers[1].layers[1].get_weights()
    #print("Final weights:", final_weights)


    import ipdb; ipdb.set_trace()
    
