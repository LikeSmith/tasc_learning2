'''
this script has all the stuff for training a tasc controller that works
for a variable number of swarm members.
'''

import numpy as np
import keras.backend as KK
from keras.callbacks import EarlyStopping
from keras.layers import Input, Concatenate, Add, Lambda, LSTM
from keras.models import Model, load_model
from tasc_learning.models import MLP, Abstract_Dynamics, Recurrent
from tasc_learning.utils.tasc_utils import nModel_Checkpoint, Add_Loss, quad

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

class Abstract_Encoder(object):
    home = ''

    def __init__(self, swarm_state_size=1, swarm_param_size=1, abs_size=1, lstm_size=1, layers=[1], activ='relu', final_activ='linear', param_file=None, net_file=None, home=None, name='abs_enc'):
        if home is not None:
            self.home = home

        if param_file is not None:
            self.swarm_state_size, self.swarm_param_size, self.abs_size, self.lstm_size, self.layers, self.activ, self.final_activ, self.name = pkl.load(open(self.home + param_file, 'rb'))
        else:
            self.swarm_state_size = swarm_state_size
            self.swarm_param_size = swarm_param_size
            self.abs_size = abs_size
            self.lstm_size = lstm_size
            self.layers = layers
            self.activ = activ
            self.final_activ = final_activ
            self.name = name

            if not isinstance(self.activ, list):
                self.activ = [self.activ]*len(self.layers)

        if net_file is not None:
            self.mdl = load_model(self.home + net_file)
        else:
            self.wire_network()

        self.compile_mdl()

    def wire_network(self):
        self.lstm_lay = LSTM(self.lstm_size, name=self.name+'_lstm')
        self.mlp_lay = MLP(input_size=self.lstm_size, output_size=self.abs_size, hidden_size=self.layers, hidden_activation=self.activ, output_activation=self.final_activ, name=self.name+'_mlp')

        self.x_si = Input(shape=(None, self.swarm_state_size), name=self.name+'_x_si')
        self.p_si = Input(shape=(None, self.swarm_param_size), name=self.name+'_p_si')

        self.full_state = Concatenate(2, name=self.name+'_concat')([x_si, p_si])
        self.lstm_out = self.lstm_lay(self.full_state)
        self.a_i = self.mlp_lay(self.lstm_out)

        self.mdl = Model([self.x_si, self.p_si], self.a_i)

    def compile_mdl(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def save_params(self, param_file):
        pkl.dump([self.swarm_state_size, self.swarm_param_size, self.abs_size, self.lstm_size, self.layers, self.activ, self.final_activ, self.name], open(self.home+param_file, 'wb'))

    def save(self, net_file):
        self.mdl.save(self.home+net_file)

    def eval(self, x, p):
        if len(x.shape) <= 2:
            x = x.reshape(1, self.swarm_size, self.swarm_state_size)
            p = p.reshape(1, self.swarm_size, self.swarm_param_size)
            batch_one = True
        else:
            batch_one = False

        a = self.mdl.predict([x, p])

        if batch_one:
            a = a[0, :]

        return a

    def __call__(self, args):
        return self.mdl(args)

class Parent_CTRL(object):
    home = ''

    def __init__(self, parent_state_size=1, abs_size=1, layers=[1], activ='relu', final_activ='linear', param_file=None, net_file=None, home=None, name='parent_ctrl'):
        if home is not None:
            self.home = home

        if param_file is not None:
            self.parent_state_size, self.abs_size, self.layers, self.activ, self.final_activ, self.name = pkl.load(open(self.home + param_file, 'rb'))
        else:
            self.parent_state_size = parent_state_size
            self.abs_size = abs_size
            self.layers = layers
            self.activ = activ
            self.final_activ = final_activ
            self.name = name

            if not isinstance(self.activ, list):
                self.activ = [self.activ]*len(self.layers)

        if net_file is not None:
            self.mdl = load_model(self.home + net_file)
        else:
            self.wire_network()

        self.compile_mdl()

    def wire_network(self):
        self.mlp_lay = MLP(input_size=self.parent_state_size*2, output_size=self.abs_size, hidden_size=self.layers, hidden_activation=self.activ, output_activation=self.final_activ, name=self.name+'_mlp')

        self.x_pi = Input(shape=(self.parent_state_size,), name=self.name+'_x_pi')
        self.x_pd = Input(shape=(self.parent_state_size,), name=self.name+'_x_pd')

        self.combined_state = Concatenate(name=self.name+'_concat')([self.x_pi, self.x_pd])

        self.a_d = self.mlp_lay(self.combined_state)

        self.mdl = Model([self.x_pi, self.x_pd], self.a_d)

    def compile_mdl(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def save_params(self, param_file):
        pkl.dump([self.parent_state_size, self.abs_size, self.layers, self.activ, self.final_activ, self.name], open(self.home+param_file, 'wb'))

    def save(self, net_file):
        self.mdl.save(self.home+net_file)

    def eval(self, x_pi, x_pd):
        if len(x.shape) <= 2:
            x_pi = x_pi.reshape(1, self.parent_state_size)
            x_pd = x_pd.reshape(1, self.parent_state_size)
            batch_one = True
        else:
            batch_one = False

        a_d = self.mdl.predict([x, p])

        if batch_one:
            a_d = a_d[0, :]

        return a_d

    def __call__(self, args):
        return self.mdl(args)

class Swarm_CTRL(object):
    home = ''

    def __init__(self, swarm_state_size=1, swarm_param_size=1, swarm_ctrl_size=1, abs_size=1, aux_size=1, layers=[1], activ='relu', final_activ='linear', param_file=None, net_file=None, home=None, name='swarm_ctrl'):
        if home is not None:
            self.home = home

        if param_file is not None:
            self.swarm_state_size, self.swarm_param_size, self.swarm_ctrl_size, self.abs_size, self.aux_size, self.layers, self.activ, self.final_activ, self.name = pkl.load(open(self.home + param_file, 'rb'))
        else:
            self.swarm_state_size = swarm_state_size
            self.swarm_param_size = swarm_param_size
            self.swarm_ctrl_size = swarm_ctrl_size
            self.abs_size = abs_size
            self.aux_size = aux_size
            self.layers = layers
            self.activ = activ
            self.final_activ = final_activ
            self.name = name

            if not isinstance(self.activ, list):
                self.activ = [self.activ]*len(self.layers)

        if net_file is not None:
            self.mdl = load_model(self.home + net_file)
        else:
            self.wire_network()

        self.compile_mdl()

    def wire_network(self):
        self.mlp_lay = MLP(input_size=self.swarm_state_size+self.swarm_param_size+self.aux_size+self.abs_size*2, output_size=self.swarm_ctrl_size, hidden_size=self.layers, hidden_activation=self.activ, output_activation=self.final_activ, name=self.name+'_mlp')

        self.x_si = Input(shape=(self.swarm_state_size,), name=self.name+'_x_si')
        self.p_si = Input(shape=(self.swarm_param_size,), name=self.name+'_p_si')
        self.abs_cur = Input(shape=(self.abs_size,), name=self.name+'_abs_cur')
        self.abs_aux = Input(shape=(self.aux_size,), name=self.name+'_abs_aux')
        self.abs_des = Input(shape=(self.abs_size), name=self.name+'_abs_des')

        self.combined_state = Concatenate(name=self.name+'_concat')([self.x_si, self.p_si, self.abs_cur, self.abs_aux, self.abs_des])
        self.u_si = self.mlp_lay(self.combined_state)

        self.mdl =  Model([self.x_si, self.p_si, self.abs_cur, self.abs_aux, self.abs_des], self.u_si)

    def compile_mdl(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def save_params(self, param_file):
        pkl.dump([self.swarm_state_size, self.swarm_param_size, self.abs_size, self.aux_size, self.layers, self.activ, self.final_activ, self.name], open(self.home+param_file, 'wb'))

    def save(self, net_file):
        self.mdl.save(self.home+net_file)

    def eval(self, x_si, p_si, abs_cur, abs_aux, abs_des):
        if len(x.shape) <= 2:
            x_si = x_si.reshape(1, self.swarm_state_size)
            p_si = p_si.reshape(1, self.swarm_param_size)
            abs_cur = abs_cur.reshape(1, self.abs_size)
            abs_aux = abs_aux.reshape(1, self.aux_size)
            abs_des = abs_des.reshape(1, self.abs_size)
            batch_one = True
        else:
            batch_one = False

        u_si = self.mdl.predict([x_si, p_si, abs_cur, abs_aux, abs_des])

        if batch_one:
            u_si = u_si[0, :]

        return u_si

    def __call__(self, args):
        return self.mdl(args)

class Trainer_Cell(object):
    def __init__(self, parent_state_size, swarm_state_size, swarm_param_size, swarm_ctrl_size, swarm_size, abs_size, aux_size, abs_encoder, aux_encoder, parent_ctrl, swarm_ctrl, abstract_dynamics, system_dynamics, Q_p, Q_s, weight_dyn, weight_abs, name):
        self.parent_state_size = parent_state_size
        self.swarm_state_size = swarm_state_size
        self.swarm_param_size = swarm_param_size
        self.swarm_ctrl_size = swarm_ctrl_size
        self.swarm_size = swarm_size
        self.abs_size = abs_size
        self.aux_size = aux_size
        self.abs_encoder = abs_encoder
        self.aux_encoder = aux_encoder
        self.parent_ctrl = parent_ctrl
        self.swarm_ctrl = swarm_ctrl
        self.abstract_dynamics = abstract_dynamics
        self.system_dynamics = system_dynamics
        self.Q_p = Q_p
        self.Q_s = Q_s
        self.weight_dyn = weight_dyn
        self.weight_abs = weight_abs

        self.wire_network()
        self.compile_mdl()

    def wire_network(self):
        # set up layers
        self.upack_lay = Lambda(self.unpack_swarm_state, name=self.name+'_upack')
        self.pack_lay = Lambda(self.pack_swarm_state, name=self.name+'_pack')
        self.concat_input_lay = Lambda(self.concat_inputs, name=self.name+'_concat_in')
        self.loss_lay = Lambda(self.loss_function, name=self.name+'_loss')
        self.upack_input_xp_d = Lambda(self.unpack_input_xp_d, name=self.name+'_upack_input_xp_d')
        self.upack_input_xp_i = Lambda(self.unpack_input_xp_i, name=self.name+'_upack_input_xp_i')
        self.upack_input_xs_i = Lambda(self.unpack_input_xs_i, name=self.name+'_upack_input_xs_i')

        # set up inputs
        self.inputs = Input(shape=(2*self.parent_state_size+self.swarm_state_size*self.swarm_size,), name=self.name+'_xp_d')
        self.xp_old = Input(shape=(self.parent_state_size,), name=self.name+'_xp_old')
        self.xs_old_packed = Input(shape=(self.swarm_state_size*self.swarm_size,), name=self.name+'_xs_old')
        self.ps = Input(shape=(None, self.swarm_param_size), name=self.name+'_ps')
        self.loss_old = Input(shape=(1,), name=self.name+'_old_loss')

        # unpack swarm state and inputs
        self.xs_old = self.upack_lay(self.xs_old_packed)
        self.xp_d = self.upack_input_xp_d(self.inputs)
        self.xp_i = self.upack_input_xp_i(self.inputs)
        self.xs_i = self.upack_input_xs_i(self.inputs)

        # apply initial states
        self.xp_old = Add(name=self.name+'_xp_old_add')([self.xp_old, self.xp_i])
        self.xs_old = Add(name=self.name+'_xs_old_add')([self.xs_old, self.xs_i])

        # calculate abstract states
        self.abs_cur = self.abs_encoder([self.xs_old, self.ps])
        self.abs_aux = self.aux_encoder([self.xs_old, self.ps])

        # calculate desired abstract state
        self.abs_des = self.parent_ctrl([self.xp_old, self.xp_d])

        # calculate swarm inputs
        self.us_i = []

        for i in range(self.swarm_size):
            self.us_i.append(self.swarm_ctrl([self.xs_old[:, i, :], self.ps[:, i, :],, self.abs_cur, self.abs_aux, self.abs_des]))

        self.us = self.concat_input_lay(self.us_i)

        # integrate real system
        self.xp_out, self.xs_out = self.system_dynamics([self.xp_old, xs_old, u, ps])

        # predict abstract dynamics
        self.xp_prd = self.abstract_dynamics([self.xp_old, self.abs_cur])

        # calculate loss
        self.loss_new = self.loss_old + self.loss_lay([self.xp_out, self.xp_prd, self.xp_des, self.xs_out, self.abs_cur, self.abs_des])

        # package new xs
        self.xs_new_packed = self.pack_lay(xs_new)
        self.x = Concatenate(name=self.name+'_state_concat')([self.xp_out, self.xs_new_packed])

        self.mdl = Model([self.xp_d, self.xp_old, self.xs_old, self.loss_old, self.ps], [self.x, self.xp_new, self.xs_new, self.loss_new])

    def compile_mdl(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def pack_swarm_state(self, swarm_state):
        return KK.concatenate([swarm_state[:, :, i, :] for i in range(self.swarm_size)])

    def unpack_swarm_state(self, swarm_state):
        z = KK.expand_dims(swarm_state)
        return KK.concatenate([z[:, i::self.swarm_state_size, :] for i in range(self.swarm_state_size)])

    def concat_inputs(self, u_si):
        for i in range(len(u_si)):
            self.u_si[i] = KK.expand_dims(u_si[i], axis=1)
        return KK.concatenate(u_si, axis=1)

    def unpack_input_xp_d(self, inputs):
        xp_d = inputs[:, :self.parent_state_size]
        return xp_d

    def unpack_input_xp_i(self, inputs):
        xp_i = inputs[:, self.parent_state_size:2*self.parent_state_size]
        return xp_i

    def unpack_input_xs_i(self, inputs):
        xs_i = self.unpack_swarm_state(inputs[:, 2*self.parent_state_size:])
        return xs_i

    def loss_function(self, args):
        xp_new, xp_prd, xp_des, xs, abs_cur, abs_des = args

        loss = quad(xp_new - xp_des, self.Q_p)

        for i in range(self.swarm_size):
            loss += quad(xs[:, i, :], self.Q_s)

        loss += self.weight_dyn * KK.batch_dot(xp_prd - xp_new, xp_prd - xp_new, 1)
        loss += self.weight_abs * KK.batch_dot(abs_cur - abs_des, abs_cur - abs_des, 1)

        return loss

    def __call__(self, args):
        return self.mdl(args)

class TASC_Modular_Policy_Trainer(object):
    home = ''

    def __init__(self, parent_state_size=1, swarm_state_size=1, swarm_param_size=1, swarm_ctrl_size=1, swarm_size=1, abs_size=1, aux_size=1, abs_lstm_size=1, abs_layers=[1], abs_activ='relu', abs_final_activ='linear', aux_lstm_size=1, aux_layers=[1], aux_activ='relu', aux_final_activ='linear', parent_layers=[1], parent_activ='relu', parent_final_activ='linear', swarm_layers=[1], swarm_activ='relu', swarm_final_activ='linear', abs_dyn_layers=[1], abs_dyn_activ='relu', abs_dyn_final_activ='linear', sys_dyn=None, Q_p=np.eye(1), Q_s=eye(1), weight_dyn=1, weight_abs=1, home=None):

        if home is not None:
            self.home = home

        self.parent_state_size = parent_state_size
        self.swarm_state_size = swarm_state_size
        self.swarm_param_size = swarm_param_size
        self.swarm_ctrl_size = swarm_ctrl_size
        self.swarm_size = swarm_size
        self.abs_size = abs_size
        self.aux_size = aux_size
        self.abs_lstm_size = abs_lstm_size
        self.abs_layers = abs_layers
        self.abs_activ = abs_activ
        self.abs_final_activ = abs_final_activ
        self.aux_lstm_size = aux_lstm_size
        self.aux_layers = aux_layers
        self.aux_activ = aux_activ
        self.aux_final_activ = aux_final_activ
        self.parent_layers = parent_layers
        self.parent_activ = parent_activ
        self.parent_final_activ = parent_final_activ
        self.swarm_layers = swarm_layers
        self.swarm_activ = swarm_activ
        self.swarm_final_activ = swarm_final_activ
        self.abs_dyn_layers = abs_dyn_layers
        self.abs_dyn_activ = abs_dyn_activ
        self.abs_dyn_final_activ = abs_dyn_final_activ
        self.sys_dyn = sys_dyn
        self.Q_p = KK.constant(Q_p)
        self.Q_s = KK.constant(Q_s)
        self.weight_dyn = KK.constant(weight_dyn)
        self.weight_abs = KK.constant(weight_abs)

        self.wire_network()
        self.compile_network()

    def wire_network(self):
        # set up components
        self.abs_encoder = Abstract_Encoder(swarm_state_size=self.swarm_state_size, swarm_param_size=self.swarm_param_size, abs_size=self.abs_size, lstm_size=self.abs_lstm_size, layers=self.abs_layers, activ=self.abs_activ, final_activ=self.abs_final_activ, home=self.home, name=self.name+'_abs_enc')
        self.aux_encoder = Abstract_Encoder(swarm_state_size=self.swarm_state_size, swarm_param_size=self.swarm_param_size, abs_size=self.aux_size, lstm_size=self.aux_lstm_size, layers=self.aux_layers, activ=self.aux_activ, final_activ=self.aux_final_activ, home=self.home, name=self.name+'_aux_enc')
        self.parent_ctrl = Parent_CTRL( parent_state_size=self.parent_state_size, abs_size=self.abs_size, layers=self.parent_layers, activ=self.parent_activ, final_activ=self.parent_final_activ, home=self.home, name=self.name+'_parent_ctrl')
        self.swarm_ctrl = Swarm_CTRL(swarm_state_size=self.swarm_state_size, swarm_param_size=self.swarm_param_size, swarm_ctrl_size=self.swarm_ctrl_size, abs_size=self.abs_size, aux_size=self.aux_size, layers=self.swarm_layers, activ=self.swarm_activ, final_activ=self.swarm_final_activ, home=self.home, name=self.name+'_swarm_ctrl')
        self.abs_dyn = Abstract_Dynamics(parent_state_size=self.parent_state_size, abs_size=self.abs_size, swarm_input_size=self.swarm_input_size, swarm_size=self.swarm_size, layers=self.abs_dyn_layers, layers_activ=self.abs_dyn_activ, layers_activ_final=self.abs_dyn_final_activ, name=self.name+'_abs_dyn', home=self.home)

        # setup cell
        self.rnn_cell = Trainer_Cell(
            parent_state_size=self.parent_state_size,
            swarm_state_size=self.swarm_state_size,
            swarm_param_size=self.swarm_param_size,
            swarm_ctrl_size=self.swarm_ctrl_size,
            swarm_size=self.swarm_size,
            abs_size=self.abs_size,
            aux_size=self.aux_size,
            abs_encoder=self.abs_encoder,
            aux_encoder=self.aux_encoder,
            parent_ctrl=self.parent_ctrl,
            swarm_ctrl=self.swarm_ctrl,
            abstract_dynamics=self.abs_dyn,
            system_dynamics=self.sys_dyn,
            Q_p=self.Q_p,
            Q_s=self.Q_s,
            weight_dyn=self.weight_dyn,
            weight_abs=self.weight_abs,
            name=self.name+'_rnn_cell'
            )

        # setup inputs
