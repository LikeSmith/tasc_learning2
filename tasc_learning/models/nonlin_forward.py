'''
forward model for a nonlinear system
'''

import numpy as np

from keras.layers import Input, Concatenate
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping
from keras.optimizers import RMSprop
from tasc_learning.models import MLP

class Nonlin_Forward(object):

    def __init__(self, state_size, input_size, n_hidden,
                 name='nonlin_fwd_model', from_file=False, from_MLP=False, MLP_name='mlp',  file_path='nonlin_fwd.h5'):
        self.state_size = state_size
        self.input_size = input_size
        self.n_hidden = n_hidden
        self.name = name
        self.opt = RMSprop(lr=0.001, decay=1e-8)

        if from_file:
            self.mdl = load_model(file_path)
            self.mdl.compile(optimizer=self.opt, loss='mse')
        else:
            self.x_t = Input(shape=(self.state_size,),
                             name=self.name+'_x_t')
            self.u = Input(shape=(self.input_size,),
                           name=self.name+'_u')
            self.m = Concatenate(name=self.name+'_m')([self.x_t, self.u])
            if from_MLP:
                self.mlp = MLP(self.state_size+self.input_size, self.state_size, 1, [self.n_hidden], ['tanh'], from_file=True, file_path=file_path, name=MLP_name)
            else:
                self.mlp = MLP(self.state_size+self.input_size, self.state_size, 1, [self.n_hidden], ['tanh'], name=self.name+'_mlp')

            self.x_t1 = self.mlp.mdl(self.m)

            self.mdl = Model([self.x_t, self.u], self.x_t1)
            self.mdl.compile(optimizer=self.opt, loss='mse')

    def train(self, train_in, train_out, val_split=0.2, epochs=50,
              batch_size=10, verbosity=2, patience=1):
        es = EarlyStopping(monitor='val_loss', patience=patience)
        hist = self.mdl.fit(train_in, train_out,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_split=val_split,
                            verbose=verbosity,
                            callbacks=[es])
        return hist

    def lock(self):
        for layer in self.mdl.layers:
            if layer.name == 'model_1':
                layer.trainable = False

    def unlock(self):
        for layer in self.mdl.layers:
            if layer.name == 'model_1':
                layer.trainable = True

    def eval(self, x, u):
        batch_size = int(x.size/2)
        return self.mdl.predict([x.reshape(batch_size, self.state_size), u.reshape(batch_size, self.input_size)])

    def save(self, save_path):
        self.mdl.save(save_path)
