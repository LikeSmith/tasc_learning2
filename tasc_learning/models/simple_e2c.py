import numpy as np
import _pickle as pkl
import matplotlib.pyplot as plt

import keras.backend as K
from keras.models import Model
from keras.layers import Input, Dense, Lambda, merge
from keras import objectives

import tasc_learning.datasets.LinSys as ls
from vae import VAE


def test_vaes_individually(xs_vae, xp_vae, u_vae):
    xs_vae.model.fit(
        data.xs_t, data.xs_t, shuffle=True,
        nb_epoch=2, batch_size=batch_size,
        validation_split=0.2
    )

    xp_vae.model.fit(
        data.xp_t, data.xp_t, shuffle=True,
        nb_epoch=2, batch_size=batch_size,
        validation_split=0.2
    )

    u_vae.model.fit(
        data.u_t, data.u_t, shuffle=True,
        nb_epoch=2, batch_size=batch_size,
        validation_split=0.2
    )
    

def test_vaes_together(xs_vae, xp_vae, u_vae):
    model = Model(
        [xs_vae.x, xp_vae.x, u_vae.x],
        [
            xs_vae.x_decoded_mean,
            xp_vae.x_decoded_mean,
            u_vae.x_decoded_mean
        ]
    )
    model.compile(
        optimizer='rmsprop',
        loss={
            'xs_decoded_mean': xs_vae.loss,
            'xp_decoded_mean': xp_vae.loss,
            'u_decoded_mean': u_vae.loss
        }
    )

    model.fit(
        {
            'xs_input': data.xs_t,
            'xp_input': data.xp_t,
            'u_input': data.u_t
        },
        {
            'xs_decoded_mean': data.xs_t,
            'xp_decoded_mean': data.xp_t,
            'u_decoded_mean': data.u_t
        },
        nb_epoch=10,
        batch_size=32
    )


if __name__ == '__main__':
    print("Loading data...", end="")
    data = ls.LinSys()
    print("")

    epochs = 100
    batch_size = 32
    xs_dim = data.xs_t.shape[1]
    xp_dim = data.xp_t.shape[1]
    u_dim = data.u_t.shape[1]
    
    n_xs_hiddens = 100
    n_xp_hiddens = 1000
    n_u_hiddens = 100

    latent_dim = 2
    epsilon_std = 1.0

    xs_vae = VAE(
        'xs', xs_dim, n_xs_hiddens, latent_dim,
        n_xs_hiddens, batch_size, epsilon_std
    )

    xp_vae = VAE(
        'xp', xp_dim, n_xp_hiddens, latent_dim,
        n_xp_hiddens, batch_size, epsilon_std
    )

    u_vae = VAE(
        'u', u_dim, n_u_hiddens, latent_dim,
        n_u_hiddens, batch_size, epsilon_std
    )

    #test_vaes_individually(xs_vae, xp_vae, u_vae)
    #test_vaes_together(xs_vae, xp_vae, u_vae)

    z_means = merge(
        [xs_vae.z_mean, u_vae.z_mean, xp_vae.z_mean],
        mode='sum'
    )

    z_log_vars = merge(
        [xs_vae.z_log_var, u_vae.z_log_var, xp_vae.z_log_var],
        mode='sum'
    )

    sample_z = xp_vae.sample(z_means, z_log_vars)

    x_decoded_mean = xp_vae.decoder(sample_z)

    def loss(x, x_decoded_mean):
        bce = objectives.binary_crossentropy(x, x_decoded_mean)
        ent_loss = xp_vae.input_dim * bce
        kl_loss = -0.5 * K.sum(
            1 + z_log_vars \
            - K.square(z_means) \
            - K.exp(z_log_vars),
            axis=-1
        )
        return ent_loss + kl_loss


    model = Model(
        [xs_vae.x, xp_vae.x, u_vae.x],
        [
            xs_vae.x_decoded_mean,
            u_vae.x_decoded_mean,
            x_decoded_mean
        ]
    )

    model.compile(
        optimizer='rmsprop',
        loss={
            'xs_decoded_mean': xs_vae.loss,
            'xp_decoded_mean': loss,
            'u_decoded_mean': u_vae.loss
        },
        loss_weights={
            'xs_decoded_mean': 0.01,
            'xp_decoded_mean': 0.98,
            'u_decoded_mean': 0.01
        }
    )

    model.fit(
        {
            'xs_input': data.xs_t,
            'xp_input': data.xp_t,
            'u_input': data.u_t
        },
        {
            'xs_decoded_mean': data.xs_t,
            'xp_decoded_mean': data.xp_t1,
            'u_decoded_mean': data.u_t
        },
        nb_epoch=100,
        batch_size=32
    )    
