'''
this file contains everything for learning a full context tasc
controller
'''

import numpy as np
from tasc_learning.models import MLP
from tasc_learning.utils.tasc_utils import nModel_Checkpoint
from keras.layers import Input, Concatenate, Lambda
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping
import keras.backend as KK

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

class Abstract_Dynamics(object):
    home = ''

    def __init__(self, parent_state_size=2, abs_size=3, layers=[10], layers_activ=['tanh'], layers_activ_final='linear', name='abs_dyn', param_file=None, from_file=False, net_file=None, home=None):
        if home != None:
            self.home = home

        if param_file != None:
            self.parent_state_size, self.abs_size, self.layers, self.layers_activ, self.layers_activ_final, self.name = pkl.load(open(self.home+param_file, 'rb'))
        else:
            self.parent_state_size = parent_state_size
            self.abs_size = abs_size
            self.layers = layers
            self.layers_activ = layers_activ
            self.layers_activ_final = layers_activ_final
            self.name = name

        if from_file or net_file != None:
            self.mdl = load_model(self.home+net_file)
        else:
            self.wire_network()

        self.compile_network()

    def wire_network(self):
        self.mlp = MLP(input_size=self.parent_state_size+self.abs_size, output_size=self.parent_state_size, n_hidden=len(self.layers), hidden_size=self.layers, hidden_activation=self.layers_activ, output_activation=self.layers_activ_final, name=self.name+'_mlp')

        self.x_p_old = Input(shape=(self.parent_state_size,), name=self.name+'_x_p_old')
        self.a = Input(shape=(self.abs_size,), name=self.name+'_a')

        self.merge = Concatenate(name=self.name+'_m')([self.x_p_old, self.a])

        self.x_p_new = self.mlp.mdl(self.merge)

        self.mdl = Model([self.x_p_old, self.a], self.x_p_new)

    def compile_network(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def eval(self, x_p, a):
        if len(x_p.shape) == 1:
            x_p = x_p.reshape((1, self.parent_state_size))
            a = a.reshape((1, self.abs_size))
        else:
            x_p = x_p.T
            a = a.T

        return self.mdl.predict([x_p, a, u])

    def save_params(self, param_file):
        pkl.dump([self.parent_state_size, self.abs_size, self.layers, self.layers_activ, self.layers_activ_final, self.name], open(self.home+param_file, 'wb'))

    def save(self, net_file):
        self.mdl.save(self.home+net_file)

    def __call__(self, args):
        return self.mdl(args)

class Swarm_Member_Policy(object):
    home = ''

    def __init__(self, state_size=2, abs_size=2, aux_size=2, ctrl_size=1, layers=[10], activ=['tanh'], activ_final='linear', name='member_policy', from_file=False, param_file=None, net_file=None, home=None, use_diff=False):
        if home != None:
            self.home = home
        self.name = name

        if param_file != None:
            self.state_size, self.abs_size, self.aux_size, self.ctrl_size, self.layers, self.activ, self.activ_final, self.use_diff, self.name = pkl.load(open(self.home+param_file, 'rb'))
        else:
            self.state_size = state_size
            self.abs_size = abs_size
            self.aux_size = aux_size
            self.ctrl_size = ctrl_size
            self.layers = layers
            self.activ = activ
            self.activ_final = activ_final
            self.use_diff = use_diff

        if from_file:
            self.mdl = load_model(self.home+net_file)
        else:
            self.wire_network()

        self.compile_mdl()

    def wire_network(self):
        # set up subnets
        if self.use_diff:
            self.mlp = MLP(self.state_size+self.aux_size+self.abs_size, self.ctrl_size, len(self.layers), self.layers, self.activ, self.activ_final, name=self.name+'_mlp')
        else:
            self.mlp = MLP(self.state_size+self.aux_size+2*self.abs_size, self.ctrl_size, len(self.layers), self.layers, self.activ, self.activ_final, name=self.name+'_mlp')

        # wire network
        self.x_s_i = Input(shape=(self.state_size,), name=self.name+'_x_s_i')
        self.a = Input(shape=(self.abs_size,), name=self.name+'_a')
        self.a_d = Input(shape=(self.abs_size,), name=self.name+'_a_d')
        self.aux =Input(shape=(self.aux_size,), name=self.name+'_aux')

        if self.use_diff:
            self.err = Lambda(self.diff, output_shape=(self.abs_size,), name=self.name+'_err')([self.a_d, self.a])
            self.merge = Concatenate(name=self.name+'_merge')([self.x_s_i, self.err, self.aux])
        else:
            self.merge = Concatenate(name=self.name+'_merge')([self.x_s_i, self.a, self.a_d, self.aux])

        self.u_i = self.mlp.mdl(self.merge)

        self.mdl = Model([self.x_s_i, self.a, self.a_d, self.aux], self.u_i, name=self.name+'_model')

    def compile_mdl(self, opt='rmsprop', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def save_params(self, param_file):
        pkl.dump([self.state_size, self.abs_size, self.aux_size, self.ctrl_size, self.layers, self.activ, self.activ_final, self.use_diff, self.name], open(self.home+param_file, 'wb'))

    def save(self, net_file):
        self.mdl.save(self.home+net_file)

    def eval(self, x_s, a, a_d, a_aux):
        if len(x_s.shape) == 1:
            x_s = x_s.reshape((1, self.state_size))
            a = a.reshape((1, self.abs_size))
            a_d = a_d.reshape((1, self.abs_size))
            a_aux = a_aux.reshape((1, self.aux_size))
            batch_one = True
        else:
            x_s = x_s.T
            a = a.T
            a_d = a_d.T
            a_aux = a_aux.T
            batch_one = False

        ret = self.mdl.predict([x_s, a, a_d, a_aux]).T

        if batch_one:
            ret = ret[:, 0]

        return ret

    def diff(self, args):
        a, b = args
        return a - b

class Iteration(object):
    def __init__(self, parent_state_size, swarm_state_size, swarm_input_size, swarm_size, parent_ctrl, swarm_member_ctrl, abs_enc, aux_enc, abs_dyn, sys_update, name='Iteration'):
        self.parent_state_size = parent_state_size
        self.swarm_state_size = swarm_state_size
        self.swarm_input_size = swarm_input_size
        self.swarm_size = swarm_size
        self.parent_ctrl = parent_ctrl
        self.swarm_member_ctrl = swarm_member_ctrl
        self.abs_enc = abs_enc
        self.aux_enc = aux_enc
        self.abs_dyn = abs_dyn
        self.sys_update = sys_update
        self.name = name

        self.x_p_old = Input(shape=(self.parent_state_size,), name=self.name+'_x_p_old')
        self.x_s_old = Input(shape=(self.swarm_state_size*self.swarm_size,), name=self.name+'_x_s_old')

        self.a_des = parent_ctrl.mdl(self.x_p_old)
        self.a_cur = abs_enc.mdl(self.x_s_old)
        self.a_aux = aux_enc.mdl(self.x_s_old)

        self.x_si = []
        self.masks = []
        self.u_i = []

        for i in range(self.swarm_size):
            self.masks.append(Lambda(lambda x_s: self.mask(x_s, i), output_shape=(self.swarm_state_size,), name=self.name+'_mask%d'%i))
            self.x_si.append(self.masks[i](self.x_s_old))

            if isinstance(self.swarm_member_ctrl, list):
                self.u_i.append(self.swarm_member_ctrl[i].mdl([self.x_si[i], self.a_cur, self.a_des, self.a_aux]))
            else:
                self.u_i.append(self.swarm_member_ctrl.mdl([self.x_si[i], self.a_cur, self.a_des, self.a_aux]))

        self.u = Concatenate(name=self.name+'_u')(self.u_i)

        self.x_p_dyn = self.abs_dyn.mdl([self.x_p_old, self.a_cur, self.u])
        self.x_new = Lambda(self.sys_update, output_shape=(self.parent_state_size+self.swarm_state_size*self.swarm_size,), name=self.name+'_x_new')([self.x_p_old, self.x_s_old, self.u])

        self.x_p_new = Lambda(self.parent_state_mask, output_shape=(self.parent_state_size,), name=self.name+'_x_p_new')(self.x_new)
        self.x_s_new = Lambda(self.swarm_state_mask, output_shape=(self.swarm_state_size*self.swarm_size,), name=self.name+'_x_s_new')(self.x_new)
        self.x_p_err = Lambda(self.diff, output_shape=(self.parent_state_size,), name=self.name+'_x_p_err')([self.x_p_dyn, self.x_p_new])
        self.abs_err = Lambda(self.diff, output_shape=(self.abs_enc.output_size,), name=self.name+'_abs_err')([self.a_des, self.a_cur])

        self.mdl_x_p = Model([self.x_p_old, self.x_s_old], self.x_p_new)
        self.mdl_x_s = Model([self.x_p_old, self.x_s_old], self.x_s_new)
        self.mdl_x_p_err = Model([self.x_p_old, self.x_s_old], self.x_p_err)
        self.mdl_err = Model([self.x_p_old, self.x_s_old], self.abs_err)

    def mask(self, x_s, i):
        return x_s[:, self.swarm_state_size*i:self.swarm_state_size*(i+1)]

    def parent_state_mask(self, x):
        return x[:, :self.parent_state_size]

    def swarm_state_mask(self, x):
        return x[:, self.parent_state_size:]

    def diff(self, args):
        x1, x2 = args
        return x1 - x2

class TASC_Policy_Trainer(object):
    home = ''
    net_files = None

    def __init__(self, parent_state_size=2, swarm_state_size=2, swarm_input_size=1, swarm_size=4, abs_size=3, aux_size=4, n_steps=10, parent_ctrl_layers=[10], parent_ctrl_activ=['tanh'], parent_ctrl_activ_final='linear', swarm_ctrl_layers=[10], swarm_ctrl_activ=['tanh'], swarm_ctrl_activ_final='linear', abs_enc_layers=[10], abs_enc_activ=['tanh'], abs_enc_activ_final='linear', aux_enc_layers=[10], aux_enc_activ=['tanh'], aux_enc_activ_final='linear', abs_dyn_layers=[10], abs_dyn_activ=['tanh'], abs_dyn_activ_final='linear', sys_update=None, Q_p=np.eye(2), Q_pf=np.eye(2), Q_si=np.eye(2), abs_err_gain=1, name='TASC_policy_trainer', param_file=None, net_file=None, home=None, indep_swarm_ctrls=True):
        if home != None:
            self.home = home

        if param_file != None:
            self.parent_state_size, self.swarm_state_size, self.swarm_input_size, self.swarm_size, self.abs_size, self.aux_size, self.n_steps, self.parent_ctrl_layers, self.parent_ctrl_activ, self.parent_ctrl_activ_final, self.swarm_ctrl_layers, self.swarm_ctrl_activ, self.swarm_ctrl_activ_final, self.abs_enc_layers, self.abs_enc_activ, self.abs_enc_activ_final, self.aux_enc_layers, self.aux_enc_activ, self.aux_enc_activ_final, self.abs_dyn_layers, self.abs_dyn_activ, self.abs_dyn_activ_final, self.sys_update, self.Q_p_np, self.Q_pf_np, self.Q_s_np, self.abs_err_gain, self.name, self.indep_swarm_ctrls = pkl.load(open(self.home+param_file, 'rb'))
            self.Q_p = KK.variable(self.Q_p_np)
            self.Q_s = KK.variable(self.Q_s_np)
            self.Q_pf = KK.variable(self.Q_pf_np)
        else:
            self.parent_state_size = parent_state_size
            self.swarm_state_size = swarm_state_size
            self.swarm_input_size = swarm_input_size
            self.swarm_size = swarm_size
            self.abs_size = abs_size
            self.aux_size = aux_size
            self.n_steps = n_steps
            self.parent_ctrl_layers = parent_ctrl_layers
            self.parent_ctrl_activ = parent_ctrl_activ
            self.parent_ctrl_activ_final = parent_ctrl_activ_final
            self.swarm_ctrl_layers = swarm_ctrl_layers
            self.swarm_ctrl_activ = swarm_ctrl_activ
            self.swarm_ctrl_activ_final = swarm_ctrl_activ_final
            self.abs_enc_layers = abs_enc_layers
            self.abs_enc_activ = abs_enc_activ
            self.abs_enc_activ_final = abs_enc_activ_final
            self.aux_enc_layers = aux_enc_layers
            self.aux_enc_activ = aux_enc_activ
            self.aux_enc_activ_final = aux_enc_activ_final
            self.abs_dyn_layers = abs_dyn_layers
            self.abs_dyn_activ = abs_dyn_activ
            self.abs_dyn_activ_final = abs_dyn_activ_final
            self.sys_update = sys_update
            self.abs_err_gain = abs_err_gain
            self.name = name
            self.indep_swarm_ctrls = indep_swarm_ctrls

            self.Q_s_np = np.eye(self.swarm_size*self.swarm_state_size)
            for i in range(self.swarm_size):
                self.Q_s_np[self.swarm_state_size*i:self.swarm_state_size*(i+1), self.swarm_state_size*i:self.swarm_state_size*(i+1)] = Q_si
            self.Q_p_np = Q_p
            self.Q_pf_np = Q_pf
            self.Q_s = KK.variable(self.Q_s_np)
            self.Q_p = KK.variable(self.Q_p_np)
            self.Q_pf = KK.variable(self.Q_pf_np)

        self.wire_network()
        self.compile_mdl(loss=[self.stacked_loss_p, self.stacked_loss_s, self.stacked_loss_dyn, self.stacked_loss_abs_err])

    def wire_network(self):
        # initialize all bits
        self.parent_ctrl = MLP(input_size=self.parent_state_size, output_size=self.abs_size, n_hidden=len(self.parent_ctrl_layers), hidden_size=self.parent_ctrl_layers, hidden_activation=self.parent_ctrl_activ, output_activation=self.parent_ctrl_activ_final, name=self.name+'_parent_ctrl', home=self.home)
        self.abs_enc = MLP(input_size=self.swarm_state_size*self.swarm_size, output_size=self.abs_size, n_hidden=len(self.abs_enc_layers), hidden_size=self.abs_enc_layers, hidden_activation=self.abs_enc_activ, output_activation=self.abs_enc_activ_final, name=self.name+'_abs_enc', home=self.home)
        self.aux_enc = MLP(input_size=self.swarm_state_size*self.swarm_size, output_size=self.aux_size, n_hidden=len(self.aux_enc_layers), hidden_size=self.aux_enc_layers, hidden_activation=self.aux_enc_activ, output_activation=self.aux_enc_activ_final, name=self.name+'_aux_enc', home=self.home)
        self.abs_dyn = Abstract_Dynamics(parent_state_size=self.parent_state_size, abs_size=self.abs_size, swarm_input_size=self.swarm_input_size, swarm_size=self.swarm_size, layers=self.abs_dyn_layers, layers_activ=self.abs_dyn_activ, layers_activ_final=self.abs_dyn_activ_final, name=self.name+'_abs_dyn', home=self.home)

        if self.indep_swarm_ctrls:
            self.swarm_ctrl = []
            for i in range(self.swarm_size):
                self.swarm_ctrl.append(Swarm_Member_Policy(state_size=self.swarm_state_size, abs_size=self.abs_size, aux_size=self.aux_size, ctrl_size=self.swarm_input_size, layers=self.swarm_ctrl_layers, activ=self.swarm_ctrl_activ, activ_final=self.swarm_ctrl_activ_final, name=self.name+'_member%d_policy'%i, home=self.home))

        else:
            self.swarm_ctrl = Swarm_Member_Policy(state_size=self.swarm_state_size, abs_size=self.abs_size, aux_size=self.aux_size, ctrl_size=self.swarm_input_size, layers=self.swarm_ctrl_layers, activ=self.swarm_ctrl_activ, activ_final=self.swarm_ctrl_activ_final, name=self.name+'_member_policy', home=self.home)

        # set up iteration
        self.iteration = Iteration(parent_state_size=self.parent_state_size, swarm_state_size=self.swarm_state_size, swarm_input_size=self.swarm_input_size, swarm_size=self.swarm_size, parent_ctrl=self.parent_ctrl, swarm_member_ctrl=self.swarm_ctrl, abs_enc=self.abs_enc, aux_enc=self.aux_enc, abs_dyn=self.abs_dyn, sys_update=self.sys_update, name=self.name+'_iteration')

        # stack iterations
        self.x_p = [Input(shape=(self.parent_state_size,), name=self.name+'_x_p_0')]
        self.x_s = [Input(shape=(self.swarm_state_size*self.swarm_size,), name=self.name+'_x_s_0')]
        self.x_p_err = []
        self.abs_err = []

        for i in range(self.n_steps):
            self.x_p.append(self.iteration.mdl_x_p([self.x_p[i], self.x_s[i]]))
            self.x_s.append(self.iteration.mdl_x_s([self.x_p[i], self.x_s[i]]))
            self.x_p_err.append(self.iteration.mdl_x_p_err([self.x_p[i], self.x_s[i]]))
            self.abs_err.append(self.iteration.mdl_err([self.x_p[i], self.x_s[i]]))

        self.mdl = Model([self.x_p[0], self.x_s[0]], [self.x_p[-1], self.x_s[-1], self.x_p_err[-1], self.abs_err[-1]])

    def compile_mdl(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def quad(self, x, A):
        return KK.batch_dot(x, KK.dot(x, KK.transpose(A)), 1)

    def stacked_loss_p(self, x_p_nom, x_p_final):
        loss = self.quad(x_p_nom - self.x_p[-1], self.Q_pf)

        for i in range(self.n_steps-1):
            loss += self.quad(x_p_nom - self.x_p[i], self.Q_p)

        return loss

    def stacked_loss_s(self, x_s_nom, x_s_final):
        loss = self.quad(x_s_nom - self.x_s[-1], self.Q_s)

        for i in range(self.n_steps-1):
            loss += self.quad(x_s_nom - self.x_s[i], self.Q_s)

        return loss

    def stacked_loss_dyn(self, x_p_err_nom, x_p_err_final):
        loss = self.quad(x_p_err_nom - self.x_p_err[-1], KK.eye(self.parent_state_size))

        for i in range(self.n_steps-2):
            loss += self.quad(x_p_err_nom - self.x_p_err[i], KK.eye(self.parent_state_size))

        return loss

    def stacked_loss_abs_err(self, abs_err_nom, abs_err_final):
        G = KK.variable(self.abs_err_gain)
        loss = G*self.quad(abs_err_nom - self.abs_err[-1], KK.eye(self.abs_size))

        for i in range(self.n_steps-2):
            loss += G*self.quad(abs_err_nom - self.abs_err[i], KK.eye(self.abs_size))

        return loss

    def train(self, train_in, train_out, val_split=0.2, epochs=50, batch_size=10, verbosity=2, patience=1, checkpoint=False):
        cb = [EarlyStopping(monitor='val_loss', patience=patience)]

        if checkpoint:
            cb.append(nModel_Checkpoint(self.get_mdl_list(), self.net_files, verbose=1))

        hist = self.mdl.fit(train_in, train_out, batch_size=batch_size, epochs=epochs, validation_split=val_split, verbose=verbosity, callbacks=cb)
        return hist

    def set_net_files(self, parent_net, abs_net, aux_net, abs_dyn_net, swarm_net):
        self.net_files = [parent_net, abs_net, aux_net, abs_dyn_net]
        if self.indep_swarm_ctrls:
            for i in range(self.swarm_size):
                self.net_files.append(swarm_net%i)
        else:
            self.net_files.append(swarm_net)

    def get_mdl_list(self):
        mdl_list = [self.parent_ctrl, self.abs_enc, self.aux_enc, self.abs_dyn]

        if self.indep_swarm_ctrls:
            for i in range(self.swarm_size):
                mdl_list.append(self.swarm_ctrl[i])
        else:
            mdl_list.append(self.swarm_ctrl)

        return mdl_list

    def save_all_params(self, parent_param, abs_param, aux_param, abs_dyn_param, swarm_param):
        self.parent_ctrl.save_params(parent_param)
        self.abs_enc.save_params(abs_param)
        self.aux_enc.save_params(aux_param)
        self.abs_dyn.save_params(abs_dyn_param)
        if self.indep_swarm_ctrls:
            for i in range(self.swarm_size):
                self.swarm_ctrl[i].save_params(swarm_param%i)
        else:
            self.swarm_ctrl.save_params(swarm_param)


    def save_params(self, param_file):
        pkl.dump([self.parent_state_size, self.swarm_state_size, self.swarm_input_size, self.swarm_size, self.abs_size, self.aux_size, self.n_steps, self.parent_ctrl_layers, self.parent_ctrl_activ, self.parent_ctrl_activ_final, self.swarm_ctrl_layers, self.swarm_ctrl_activ, self.swarm_ctrl_activ_final, self.abs_enc_layers, self.abs_enc_activ, self.abs_enc_activ_final, self.aux_enc_layers, self.aux_enc_activ, self.aux_enc_activ_final, self.abs_dyn_layers, self.abs_dyn_activ, self.abs_dyn_activ_final, self.parent_update, self.swarm_update, self.Q_p_np, self.Q_pf_np, self.Q_s_np, self.abs_err_gain, self.name, self.indep_swarm_ctrls], open(self.home+param_file, 'wb'))

    def save_parent_ctrl(self, net_file, param_file):
        self.parent_ctrl.save_params(param_file)
        self.parent_ctrl.save(net_file)

    def save_abs_enc(self, net_file, param_file):
        self.abs_enc.save_params(param_file)
        self.abs_enc.save(net_file)

    def save_aux_enc(self, net_file, param_file):
        self.aux_enc.save_params(param_file)
        self.aux_enc.save(net_file)

    def save_abs_dyn(self, net_file, param_file):
        self.abs_dyn.save_params(param_file)
        self.abs_dyn.save(net_file)

    def save_swarm_ctrl(self, net_file, param_file):
        if self.indep_swarm_ctrls:
            for i in range(self.swarm_size):
                self.swarm_ctrl[i].save_params(param_file%i)
                self.swarm_ctrl[i].save(net_file%i)
        else:
            self.swarm_ctrl.save_params(param_file)
            self.swarm_ctrl.save(net_file)
