'''
This file impliments a Lyopunov Optimized Learner (LOL)
'''

import numpy as np
import keras.backend as KK
from keras.callbacks import EarlyStopping
from keras.layers import Input, Concatenate, Lambda, Add
from keras.models import Model, load_model
from keras.utils.generic_utils import get_custom_objects
from tasc_learning.models import MLP, Recurrent
from tasc_learning.utils.tasc_utils import nModel_Checkpoint, Add_Loss, quad
import tasc_learning.utils.activations as tl_activ
import os

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

class Basic_CTRL(object):
    home = ''

    def __init__(self, state_size=1, input_size=1, param_size=1, layers=[10], activ='relu', final_activ='linear', param_file=None, net_file=None, home=None, name='ctrl'):
        if home is not None:
            self.home = home

        if param_file is not None:
            self.state_size, self.input_size, self.param_size, self.layers, self.activ, self.final_activ, self.name = pkl.load(open(self.home + param_file, 'rb'))
        else:
            self.state_size = state_size
            self.input_size = input_size
            self.param_size = param_size
            self.layers = layers
            self.activ= activ
            self.final_activ = final_activ
            self.name = name

            if not isinstance(self.activ, list):
                self.activ = [self.activ]*len(self.layers)

        self.cust_obj = tl_activ.gen_custom_obj(self.activ)
        self.cust_obj = tl_activ.gen_custom_obj(self.final_activ, self.cust_obj)

        get_custom_objects().update(self.cust_obj)

        if net_file is not None:
            self.mdl = load_model(self.home + net_file)
        else:
            self.wire_network()

        self.compile_mdl()

    def wire_network(self):
        self.mlp_lay = MLP(input_size=self.state_size+self.param_size, output_size=self.input_size, hidden_size=self.layers, hidden_activation=self.activ, output_activation=self.final_activ, name=self.name+'_mlp')

        self.x = Input(shape=(self.state_size,), name=self.name+'_x')
        self.p = Input(shape=(self.param_size,), name=self.name+'_p')

        self.full_state = Concatenate(1, name=self.name+'_concat')([self.x, self.p])

        self.u = self.mlp_lay(self.full_state)

        self.mdl = Model([self.x, self.p], self.u, name=self.name+"_mdl")

    def compile_mdl(self, opt='adam', loss='mse'):
        try:
            os.makedirs(self.home, exist_ok=True)
        except TypeError:
            try:
                os.makedirs(self.home)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise

        self.mdl.compile(optimizer=opt, loss=loss)

    def save_params(self, filename):
        try:
            os.makedirs(self.home, exist_ok=True)
        except TypeError:
            try:
                os.makedirs(self.home)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise

        pkl.dump([self.state_size, self.input_size, self.param_size, self.layers, self.activ, self.final_activ, self.name], open(self.home+filename, 'wb'))

    def save(self, filename):
        self.mdl.save(self.home+filename)

    def eval(self, x, p):
        if len(x.shape) == 1:
            x = np.reshape(x, (1, 3))
            batch_one = True
        if len(p.shape) == 1:
            p = np.reshape(p, (1, 1))
            batch_one = True
        else:
            batch_one = False

        assert x.shape[0] == p.shape[0]

        u = self.mdl.predict([x, p])

        if batch_one:
            u = u[0, :]

        return u

    def get_lipschitz_const(self):
        weights = self.mdl.layers[3].get_weights()
        assert len(weights) == 2*(len(self.layers) + 1)

        weights[0] = weights[0][:self.state_size, :]
        shapes = [self.state_size] + self.layers + [self.input_size]
        activs = self.activ+[self.final_activ]
        L = 1.0
        for i in range(len(shapes)-1):
            assert weights[i*2].shape[0] == shapes[i] and weights[i*2].shape[1] == shapes[i+1]
            L *= tl_activ.get_activ_lipshitz(activs[i])
            #L *= np.linalg.norm(weights[i*2], ord='fro')
            #L *= np.max(weights[i*2])
            _, s, _ = np.linalg.svd(weights[i*2])
            L *= s[0]

        return L

    def get_range(self, x_bar, r, p):
        y_bar = self.eval(x_bar, p)
        weights = self.mdl.layers[3].get_weights()
        assert len(weights) == 2*(len(self.layers) + 1)

        weights[0] = weights[0][:self.state_size, :]
        shapes = [self.state_size] + self.layers + [self.input_size]
        activs = self.activ+[self.final_activ]
        for i in range(len(shapes)-1):
            assert weights[i*2].shape[0] == shapes[i] and weights[i*2].shape[1] == shapes[i+1]
            assert activs[i] in ['relu', 'leakyRelu0_1', 'linear']
            #r *= np.linalg.norm(weights[i*2], ord='fro')
            #r *= np.max(weights[i*2])
            _, s, _ = np.linalg.svd(weights[i*2])
            r *= s[0]

        return y_bar, r

    def __call__(self, args):
        return self.mdl(args)

class LOL_Trainer_Cell(object):
    home = ''

    def __init__(self, state_size, param_size, ctrl, sys_dyn, lyap_candidate, gamma_neg, aux_loss=None, name='LOL_trainer_cell'):
        self.state_size = state_size
        self.param_size = param_size
        self.sys_dyn = sys_dyn
        self.ctrl = ctrl
        self.lyap_candidate = lyap_candidate
        self.gamma_neg = KK.constant(gamma_neg)
        self.aux_loss = aux_loss
        self.name = name

        self.wire_network()
        self.compile_mdl()

    def wire_network(self):
        self.total_loss_lay = Lambda(self.total_loss, name=self.name+'_total_loss')
        self.sys_dyn_lay = Lambda(self.sys_dyn, name=self.name+'_sys_dyn')
        self.lyap_cand_lay = Lambda(self.lyap_candidate, name=self.name+'_lyap_cand')
        self.aux_loss_lay = Lambda(self.aux_loss, name=self.name+'_aux_loss')

        self.x_0 = Input(shape=(self.state_size,), name=self.name+'_x_init')
        self.L_t = Input(shape=(1,), name=self.name+'_L_t')
        self.x_t = Input(shape=(self.state_size,), name=self.name+'_x_t')
        self.p = Input(shape=(self.param_size,), name= self.name+'_p')

        self.x_t_with_0 = Add(name=self.name+'_add_init')([self.x_t, self.x_0])
        self.u_t = self.ctrl([self.x_t_with_0, self.p])
        self.x_t1 = self.sys_dyn_lay([self.x_t_with_0, self.u_t, self.p])
        self.l_t = self.lyap_cand_lay(self.x_t_with_0)
        self.l_t1 = self.lyap_cand_lay(self.x_t1)

        if self.aux_loss is not None:
            self.L_aux = self.aux_loss_lay([self.x_t_with_0, self.u_t, self.x_t1])
        else:
            self.L_aux = KK.zeros_like(self.l_t)

        self.L_t1 = self.total_loss_lay([self.L_t, self.l_t, self.l_t1, self.L_aux, self.x_t_with_0])

        self.mdl = Model([self.x_0, self.L_t, self.x_t, self.p], [self.L_t1, self.x_t1], name=self.name+"_mdl")

    def compile_mdl(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def total_loss(self, args):
        L_t = args[0]
        l_t = args[1]
        l_t1 = args[2]
        L_aux = args[3]
        x = args[4]

        eps = KK.constant(1e-3)
        norms = KK.sqrt(KK.batch_dot(x, KK.transpose(x)))
        norms_reg = KK.max(KK.concatenate([norms, KK.ones_like(norms)*eps], axis=1), axis=1)

        return L_t + l_t + self.gamma_neg*KK.exp((l_t1 - l_t)/norms_reg) + L_aux

    def __call__(self, args):
        return self.mdl(args)

class LOL_Trainer(object):
    home = ''

    def __init__(self, state_size, input_size, param_size, sys_dyn, ctrl, lyap_cand, gamma_neg=1, aux_loss=None, name='LOL_trainer', home=None, ctrl_net_save='ctrl_mdl.h5', ctrl_par_save='ctrl_par.pkl'):
        if home is not None:
            self.home=home

        self.state_size = state_size
        self.input_size = input_size
        self.param_size = param_size

        self.sys_dyn = sys_dyn
        self.ctrl = ctrl

        self.lyap_cand = lyap_cand
        self.gamma_neg = gamma_neg
        self.aux_loss = aux_loss

        self.name = name

        self.ctrl_net_save = ctrl_net_save
        self.ctrl_par_save = ctrl_par_save

        self.wire_network()
        self.compile_mdl()

    def wire_network(self):
        self.lol_cell = LOL_Trainer_Cell(self.state_size, self.param_size, self.ctrl, self.sys_dyn, self.lyap_cand, self.gamma_neg, aux_loss=self.aux_loss, name=self.name+'_cell')

        self.x_0 = Input(shape=(None, self.state_size), name=self.name+'_x_0')
        self.p = Input(shape=(self.param_size,), name=self.name+'_p')

        self.rnn_lay = Recurrent(self.lol_cell, input_size=self.state_size, state_size=[1, self.state_size], constants=[self.p], name=self.name+'_RNN', home=self.home)

        self.L_final = self.rnn_lay(self.x_0)
        self.total_loss = KK.mean(self.L_final)

        self.mdl = Model([self.x_0, self.p], self.L_final, name=self.name+"_mdl")
        self.mdl.add_loss(self.total_loss)

    def compile_mdl(self, opt='adam', loss=None):
        self.mdl.compile(optimizer=opt, loss=loss)

    def save_ctrl_mdl(self):
        self.ctrl.save(self.ctrl_net_save)

    def save_ctrl_par(self):
        self.ctrl.save_params(self.ctrl_par_save)

    def train(self, x_0, p, horizon=100, val_split=0.2, epochs=50, batch_size=10, verbosity=2, patience=1, checkpoint=False):
        assert x_0.shape[1] == self.state_size
        assert p.shape[1] == self.param_size
        assert x_0.shape[0] == p.shape[0]

        x_0_full = np.zeros((x_0.shape[0], horizon, self.state_size))
        x_0_full[:, 0, :] = x_0

        cb = [EarlyStopping(monitor='val_loss', patience=patience)]

        if checkpoint:
            cb.append(nModel_Checkpoint([self.ctrl], [self.ctrl_net_save], verbose=1))
            self.save_ctrl_par()

        hist = self.mdl.fit([x_0_full, p], batch_size=batch_size, epochs=epochs, verbose=verbosity, callbacks=cb, validation_split=val_split)

        return hist

    def __call__(self, args):
        return self.mdl(args)

    # static methods that generate standard lyapunov condidates
    @staticmethod
    def lyap_cand_quad(Q):
        Q_k = KK.constant(Q)
        def func(x):
            return quad(x, Q_k)
        return func

    # static methods that generate standard auxilliary loss functions
    @staticmethod
    def loss_aux_quad_ctrl(R):
        R_k = KK.constant(R)
        def func(args):
            u_t = args[1]
            return quad(u_t, R_k)
        return func

    @staticmethod
    def loss_aux_lqr(Q, R):
        Q_k = KK.constant(Q)
        R_k = KK.constant(R)
        def func(args):
            x_t = args[2]
            u_t = args[1]
            return quad(x_t, Q_k) + quad(u_t, R_k)
        return func
