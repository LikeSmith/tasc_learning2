'''
classes for implimenting recurrent neural net
'''

import numpy as np
from keras.layers import Input, Concatenate, RNN, Layer
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping

try:
    import _pickle as pkl
except ImportError:
    import pickle as pkl

class RNN_Cell(Layer):
    def __init__(self, cell, state_size, constants=None, name='rnn_cell', **kwargs):
        self.cell = cell
        self.state_size = state_size
        self.constants = constants
        self.name = name

        super(RNN_Cell, self).__init__(**kwargs)

    def build(self, input_shape):
        self.trainable_weights = self.cell.mdl.trainable_weights
        self.non_trainable_weights = self.cell.mdl.non_trainable_weights
        self.built = True

    def call(self, inputs, states):
        if not isinstance(states, list):
            states = list(states)

        if self.constants is None:
            out = self.cell([inputs] + states)
        else:
            out = self.cell([inputs] + states + self.constants)

        if not isinstance(out, list):
            return out, [out]
        else:
            return out[0], out

class Recurrent(object):
    home = ''

    def __init__(self, cell, input_size=1, state_size=1, constants=None, return_sequences=False, return_states=False, name='RNN', param_file=None, net_file=None, home=None):
        if home is not None:
            self.home = home

        if param_file is not None:
            self.input_size, self.state_size, self.constants, self.return_sequences, self.return_states, self.name = pkl.load(open(self.home+param_file, 'rb'))
        else:
            self.input_size = input_size
            self.state_size = state_size
            self.constants = constants
            self.return_sequences = return_sequences
            self.return_states = return_states
            self.name = name

        self.cell = cell

        if net_file is not None:
            self.mdl = load_model(self.home+net_file)
        else:
            self.wire_network()

        self.compile()

    def wire_network(self):
        self.cell_lay = RNN_Cell(self.cell, self.state_size, self.constants, '%s_cell'%self.name)
        self.rnn_lay = RNN(self.cell_lay, self.return_sequences, self.return_states)

        self.x_in = Input(shape=(None, self.input_size))

        if self.return_states:
            self.states_out, self.y_out = self.rnn_lay(self.x_in)
            self.mdl = Model(x_in, [self.states_out, self.y_out], name=self.name+'_mdl')
        else:
            self.y_out = self.rnn_lay(self.x_in)
            self.mdl = Model(self.x_in, self.y_out, name=self.name+'_mdl')

    def compile(self, opt='adam', loss='mse'):
        self.mdl.compile(optimizer=opt, loss=loss)

    def save_params(self, filename):
        pkl.dump([self.input_size, self.state_size, self.constants, self.return_sequences, self.return_states, self.name], open(self.home+filename, 'wb'))

    def save(self, filename):
        self.mdl.save(self.home+filename)

    def __call__(self, args):
        return self.mdl(args)
