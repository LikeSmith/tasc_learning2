import numpy as np

import keras.backend as K
from keras.models import Model
from keras.layers import Input, Dense, Lambda, merge
from keras import objectives

class VAE(object):
    def __init__(
            self, name,
            input_dim, encoder_dim, latent_dim, decoder_dim,
            batch_size, epsilon_std
    ):
        self.name = name
        self.input_dim = input_dim
        self.encoder_dim = encoder_dim
        self.latent_dim = latent_dim
        self.decoder_dim = decoder_dim
        self.batch_size = batch_size
        self.epsilon_std = epsilon_std

        self.x = Input(
            shape=(self.input_dim,),
            name=self.name+'_input'
        )

        self.encode_hids = Dense(
            self.encoder_dim,
            activation='relu',
            name=self.name+'_encoder'
        )
        self.z_mean = Dense(
            self.latent_dim,
            name=self.name+'_mean'
        )
        self.z_log_var = Dense(
            self.latent_dim,
            name=self.name+'_log_var'
        )

        self.z_mean, self.z_log_var = self.encoder(self.x)

        self.z_sampler = Lambda(
            self.sampling,
            output_shape=(self.latent_dim,),
            name=self.name+'_lambda'
        )

        self.sample_z = self.sample(self.z_mean, self.z_log_var)

        self.decode_hids = Dense(
            self.decoder_dim,
            activation='relu',
            name=self.name+'_decoder'
        )
        self.decode_x_mean = Dense(
            self.input_dim,
            name=self.name+'_decoded_mean'
        )
        self.x_decoded_mean = self.decoder(self.sample_z)


        self.model = Model(self.x, self.x_decoded_mean)
        self.model.compile(optimizer='rmsprop', loss=self.loss)

        self.encoder = Model(self.x, self.z_mean)

        decoder_input = Input(shape=(self.latent_dim,))


    def encoder(self, x):
        encoded_hids = self.encode_hids(x)
        z_mean = self.z_mean(encoded_hids)
        z_log_var = self.z_log_var(encoded_hids)
        return (z_mean, z_log_var)


    def sample(self, z_mean, z_log_var):
        sample_z = self.z_sampler([z_mean, z_log_var])
        return sample_z


    def decoder(self, sample_z):
        decoded_hids = self.decode_hids(sample_z)
        x_decoded_mean = self.decode_x_mean(decoded_hids)
        return x_decoded_mean


    def forward(self, x):
        dist = self.encoder(x)
        sample_z = self.sample(dist[0], dist[1])
        x_decoded_mean = self.decoder(sample_z)
        return x_decoded_mean


    def sampling(self, dist):
        z_mean, z_log_var = dist
        epsilon = K.random_normal(
            shape=(self.batch_size, self.latent_dim),
                   mean=0.,
                   std=self.epsilon_std
        )
        return z_mean + K.exp(z_log_var / 2) * epsilon


    def loss(self, x, x_decoded_mean):
        bce = objectives.binary_crossentropy(x, x_decoded_mean)
        ent_loss = self.input_dim * bce
        kl_loss = -0.5 * K.sum(
            1 + self.z_log_var \
            - K.square(self.z_mean) \
            - K.exp(self.z_log_var),
            axis=-1
        )
        return ent_loss + kl_loss


