'''
untrainable model for adding analyic first order differential equation
to the network
'''

import numpy as np
from keras.layers import Input, Lambda
from keras.models import Model
import keras.backend as KK

class Analytic_Model(object):
    def __init__(self, n_states, n_inputs, cont_fun_keras, cont_fun, method='RK1', del_t=0.1, name='analytic'):
        self.n_states = n_states
        self.n_inputs = n_inputs
        self.cont_fun_keras = cont_fun_keras
        self.cont_fun = cont_fun
        self.del_t = del_t
        self.name = name

        self.generate_disc_funcs(method)

        self.x_in = Input(shape=(n_states,), name=self.name+'_x_in')
        self.u_in = Input(shape=(n_inputs,), name=self.name+'_u_in')

        self.x_out = Lambda(self.disc_fun_keras, output_shape=(n_states,), name=self.name+'_x_out')([self.x_in, self.u_in])

        self.mdl = Model([self.x_in, self.u_in], self.x_out)
        self.mdl.compile(optimizer='rmsprop', loss='mse')

    def generate_disc_funcs(self, method):
        self.disc_funcs
        if method == 'euler':
            self.disc_fun_keras = lambda args: self.euler_keras(args, self.cont_fun_keras)
            self.disc_fun = lambda x, u: self.euler(x, u, self.cont_fun)
        elif method == 'RK4':
            self.disc_fun_keras = lambda args: self.RK4_keras(args, self.cont_fun_keras)
            self.disc_fun = lambda x, u: self.RK4(x, u, self.cont_fun)

    def eval(self, x, u):
        return self.disc_fun(x, u)

    def euler_keras(self, args, f):
        x, u = args
        x_dot = self.cont_fun(x, u)
        x_t1 = x + self.del_t*x_dot
        return KK.cast(x_t1, 'float32')

    def euler(self, x, u, f):
        x_dot = self.cont_fun(x, u)
        x_t1 = x + self.del_t*x_dot
        return x_t1

    def RK4_keras(self, args, f):
        x, u = args
        x_dot1 = f(x, u)
        x_dot2 = f(x+self.del_t/2.0*x_dot1, u)
        x_dot3 = f(x+self.del_t/2.0*x_dot2, u)
        x_dot4 = f(x+self.del_t*x_dot3, u)
        x_t1 = x + self.del_t/6.0*(x_dot1 + 2*x_dot2 + 2*x_dot3 + x_dot4)
        return KK.cast(x_t1, 'float32')

    def RK4(self, x, u, f):
        x_dot1 = f(x, u)
        x_dot2 = f(x+self.del_t/2.0*x_dot1, u)
        x_dot3 = f(x+self.del_t/2.0*x_dot2, u)
        x_dot4 = f(x+self.del_t*x_dot3, u)
        x_t1 = x + self.del_t/6.0*(x_dot1 + 2*x_dot2 + 2*x_dot3 + x_dot4)
        return x_t1

class Analytic_Model_Parent_Swarm(object):
    def __init__(self, n_parent_states, n_swarm_states, n_swarm_inputs, n_swarm_params, n_swarm_members, cont_parent_fun_keras, cont_parent_fun, cont_swarm_fun_keras, cont_swarm_fun, method='RK4', del_t=0.1, name='analytic'):
        self.n_parent_states = n_parent_states
        self.n_swarm_states = n_swarm_states
        self.n_swarm_inputs = n_swarm_inputs
        self.n_swarm_params = n_swarm_params
        self.n_swarm_members = n_swarm_members
        self.fp_k = cont_parent_fun_keras
        self.fp = cont_parent_fun
        self.fs_k = cont_swarm_fun_keras
        self.fs = cont_swarm_fun
        self.del_t = del_t
        self.name = name

        if method == 'euler':
            self.lam_lay_parent = Lambda(self.euler_parent_keras, output_shape=(self.n_parent_states), name=self.name+'_lam_p')
            self.lam_lay_swarm = Lambda(self.euler_swarm_keras, output_shape=(self.n_swarm_members, self.n_swarm_states), name=self.name+'_lam_s')
            self.f_d = self.euler
        elif method == 'RK4':
            self.lam_lay_parent = Lambda(self.RK4_parent_keras, output_shape=(self.n_parent_states), name=self.name+'_lam_p')
            self.lam_lay_swarm = Lambda(self.RK4_swarm_keras, output_shape=(self.n_swarm_members, self.n_swarm_states), name=self.name+'_lam_s')
            self.f_d = self.RK4

        self.xp_old = Input(shape=(self.n_parent_states,), name=self.name+'_xp_old')
        self.xs_old = Input(shape=(self.n_swarm_members, self.n_swarm_states), name=self.name+'_xs_old')
        self.u = Input(shape=(self.n_swarm_members, self.n_swarm_inputs), name=self.name+'_u')
        self.ps = Input(shape=(self.n_swarm_members, self.n_swarm_params), name=self.name+'_params')

        self.xp_new = self.lam_lay_p([self.xp_old, self.xs_old])
        self.xs_new = self.lam_lay_s([self.xs_old, self.u, self.ps])

        self.mdl = Model([self.xp_old, self.xs_old, self.u, self.ps], [self.xp_new, self.xs_new])
        self.mdl.compile(optimizer='adam', loss='mse')

    def euler_parent_keras(self, args):
        xp_old, xs_old = args

        xp_dot = self.fp_k(xp_old, xs_old, self.n_swarm_members, ps)

        return KK.cast(xp_old + self.del_t*xp_dot, 'float32')

    def euler_swarm_keras(self, args):
        xs_old, u, ps = args

        xs_dot = self.fs_k(xs_old, u, self.n_swarm_members, ps)

        return KK.cast(xs_old + self.del_t*xs_dot, 'float32')

    def euler(self, xp_old, xs_old, u, ps):
        xp_dot = self.fp(xp_old, xs_old, self.n_swarm_members, ps)
        xs_dot = self.fs(xs_old, u, self.n_swarm_members, ps)

        xp_new = xp_old + self.del_t*xp_dot
        xs_new = xs_old + self.del_t*xs_dot

        return [xp_new, xs_new]

    def RK4_parent_keras(self, args):
        xp_old, xs_old = args

        xp_dot1 = self.fp_k(xp_old, xs_old, self.n_swarm_members, ps)
        xs_dot1 = self.fs_k(xs_old, u, self.n_swarm_members, ps)
        xp_dot2 = self.fp_k(xp_old+self.del_t/2.0*xp_dot1, xs_old+self.del_t/2.0*xs_dot1, self.n_swarm_members, ps)
        xs_dot2 = self.fs_k(xs_old+self.del_t/2.0*xs_dot1, u, self.n_swarm_members, ps)
        xp_dot3 = self.fp_k(xp_old+self.del_t/2.0*xp_dot2, xs_old+self.del_t/2.0*xs_dot2, self.n_swarm_members, ps)
        xs_dot3 = self.fs_k(xs_old+self.del_t/2.0*xs_dot2, u, self.n_swarm_members, ps)
        xp_dot4 = self.fp_k(xp_old+self.del_t*xp_dot3, xs_old+self.del_t*xs_dot3, self.n_swarm_members, ps)
        xs_dot4 = self.fs_k(xs_old+self.del_t*xs_dot3, u, self.n_swarm_members, ps)

        xp_new = xp_old + self.del_t/6.0*(xp_dot1 + 2.0*xp_dot2 + 2.0*xp_dot3 + xp_dot4)

        return KK.cast(xs_new, 'float32')

    def RK4_swarm_keras(self, args):
        xp_old, xs_old, u, ps = args

        xs_dot1 = self.fs_k(xs_old, u, self.n_swarm_members, ps)
        xs_dot2 = self.fs_k(xs_old+self.del_t/2.0*xs_dot1, u, self.n_swarm_members, ps)
        xs_dot3 = self.fs_k(xs_old+self.del_t/2.0*xs_dot2, u, self.n_swarm_members, ps)
        xs_dot4 = self.fs_k(xs_old+self.del_t*xs_dot3, u, self.n_swarm_members, ps)

        xs_new = xs_old + self.del_t/6.0*(xs_dot1 + 2.0*xs_dot2 + 2.0*xs_dot3 + xs_dot4)

        return KK.cast(xs_new, 'float32')

    def RK4(self, xp_old, xs_old, u, ps):
        xp_dot1 = self.fp(xp_old, xs_old, self.n_swarm_members, ps)
        xs_dot1 = self.fs(xs_old, u, self.n_swarm_members, ps)
        xp_dot2 = self.fp(xp_old+self.del_t/2.0*xp_dot1, xs_old+self.del_t/2.0*xs_dot1, self.n_swarm_members, ps)
        xs_dot2 = self.fs(xs_old+self.del_t/2.0*xs_dot1, u, self.n_swarm_members, ps)
        xp_dot3 = self.fp(xp_old+self.del_t/2.0*xp_dot2, xs_old+self.del_t/2.0*xs_dot2, self.n_swarm_members, ps)
        xs_dot3 = self.fs(xs_old+self.del_t/2.0*xs_dot2, u, self.n_swarm_members, ps)
        xp_dot4 = self.fp(xp_old+self.del_t*xp_dot3, xs_old+self.del_t*xs_dot3, self.n_swarm_members, ps)
        xs_dot4 = self.fs(xs_old+self.del_t*xs_dot3, u, self.n_swarm_members, ps)

        xp_new = xp_old + self.del_t/6.0*(xp_dot1 + 2.0*xp_dot2 + 2.0*xp_dot3 + xp_dot4)
        xs_new = xs_old + self.del_t/6.0*(xs_dot1 + 2.0*xs_dot2 + 2.0*xs_dot3 + xs_dot4)

        return [xp_new, xs_new]

    def eval(self, xp_t, xs_t, us_t, ps):
        if len(xp_t.shape) == 1:
            xp_t = xp_t.reshape(1, self.n_parent_states)
            xs_t = xs_t.reshape(1, self.n_swarm_states*self.n_swarm_members)
            us_t = us_t.reshape(1, self.n_swarm_inputs*self.n_swarm_members)
            ps = ps.reshape(1, self.n_swarm_params, self.n_swarm_members)
            batch_one = True
        else:
            xp_t = xp_t.T
            xs_t = xs_t.T
            us_t = us_t.T
            batch_one = False

        xp_t1, xs_t1 = self.mdl.predict([xp_t, xs_t, us_t, ps])

        xp_t1 = xp_t1.T
        xs_t1 = xs_t1.T

        if batch_one:
            xp_t1 = xp_t1[:, 0]
            xs_t1 = xs_t1[:, 0]

        return [xp_t1, xs_t1]

    def __call__(self, args):
        return self.mdl(args)

class Analytic_Model_Discrete(object):
    def __init__(self, n_states, n_inputs, n_params, diff_func_keras, diff_func):
        self.n_states = n_states
        self.n_inputs = n_inputs
        self.n_params = n_params
        self.diff_func = diff_func

        self.x_in = Input(shape=(self.n_states,))
        self.u_in = Input(shape=(self.n_inputs,))
        self.p_in = Input(shape=(self.n_params,))
        self.x_out = Lambda(diff_func_keras, output_shape=(self.n_states,))([self.x_in, self.u_in, self.p_in])

        self.mdl = Model([self.x_in, self.u_in, self.p_in], self.x_out)
        self.mdl.compile(optimizer='adam', loss='mse')

    def eval(self, x, u, p):
        if len(x.shape) == 1:
            x = np.reshape(x, (1, x.shape[0]))
            batch_one = True
        if len(u.shape) == 1:
            u = np.reshape(u, (1, u.shape[0]))
            batch_one = True
        if len(p.shape) == 1:
            p = np.reshape(p, (1, p.shape[0]))
            batch_one = True
        else:
            batch_one = False

        x_new = self.diff_func(x, u, p)

        if batch_one:
            x_new = x_new[0, :]

        return x_new

    def __call__(self, args):
        return self.mdl(args)
