'''
class for creating a MultiLayer Perceptron (MLP)
'''

import numpy as np
import _pickle as pkl
from keras.layers import Input, Dense, Activation
from keras.models import Model, load_model
from keras.callbacks import EarlyStopping
from keras.utils.generic_utils import get_custom_objects
import tasc_learning.utils.activations as tl_activ

class MLP(object):
    home = ''

    def __init__(self, input_size=1, output_size=1, n_hidden=-1, hidden_size=[10], hidden_activation=['tanh'], output_activation='linear', name='mlp', from_file=False, net_file=None, initial_activation=None, loss_function='mse', param_file=None, home=None):
        if home != None:
            self.home = home

        if param_file != None:
            self.input_size, self.output_size, self.n_hidden, self.hidden_size, self.hidden_activation, self.output_activation, self.name, self.initial_activation = pkl.load(open(self.home+param_file, 'rb'))
        else:
            self.input_size = input_size
            self.output_size = output_size
            if n_hidden == -1:
                self.n_hidden = len(hidden_size)
            else:
                self.n_hidden = n_hidden
            self.hidden_size = hidden_size
            self.hidden_activation = hidden_activation
            self.output_activation = output_activation
            self.name = name
            self.initial_activation = initial_activation

        self.cust_obj = tl_activ.gen_custom_obj(self.hidden_activation)
        self.cust_obj = tl_activ.gen_custom_obj(self.output_activation, self.cust_obj)
        self.cust_obj = tl_activ.gen_custom_obj(self.initial_activation, self.cust_obj)

        get_custom_objects().update(self.cust_obj)

        if from_file or net_file != None:
            self.mdl = load_model(self.home+net_file)
            self.mdl.compile(optimizer='rmsprop', loss=loss_function)
            for layer in self.mdl.layers:
                if layer.name == self.name+'_input':
                    self.input = layer
                    break
        else:
            self.input = Input(shape=(self.input_size,),
                               name=self.name+'_input')

            if initial_activation != None:
                self.initial_activ_l = Activation(initial_activation)
                self.initial_activ = self.initial_activ_l(self.input)

            self.hidden_l = []
            self.hidden = []
            for i in range(len(hidden_size)):
                self.hidden_l.append(Dense(hidden_size[i],
                                           activation=hidden_activation[i],
                                           name=self.name+'_hidden%d'%i))
                if i == 0:
                    if initial_activation != None:
                        self.hidden.append(self.hidden_l[i](self.initial_activ))
                    else:
                        self.hidden.append(self.hidden_l[i](self.input))
                else:
                    self.hidden.append(self.hidden_l[i](self.hidden[i-1]))

            self.output_l = Dense(self.output_size,
                            activation=self.output_activation,
                            name=self.name+'_output')
            if len(self.hidden) > 0:
                self.output = self.output_l(self.hidden[-1])
            elif initial_activation != None:
                self.output = self.output_l(self.initial_activ)
            else:
                self.output = self.output_l(self.input)

            self.mdl = Model(self.input, self.output, name=self.name+'_model')
            self.mdl.compile(optimizer='adam', loss=loss_function)

    def train(self, train_in, train_out, val_split=0.2, epochs=50, batch_size=10, verbosity=2, patience=1):
        es = EarlyStopping(monitor='val_loss', patience=patience)
        hist = self.mdl.fit(train_in, train_out,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_split=val_split,
                            verbose=verbosity,
                            callbacks=[es])
        return hist

    def lock(self):
        lockable_layers = [self.name+'_output']
        for i in range(self.n_hidden):
            lockable_layers.append(self.name+'_hidden%d'%i)

        for layer in self.mdl.layers:
            if layer.name in lockable_layers:
                layer.trainable = False

    def unlock(self):
        lockable_layers = [self.name+'_output']
        for i in range(self.n_hidden):
            lockable_layers.append(self.name+'_hidden%d'%i)

        for layer in self.mdl.layers:
            if layer.name in lockable_layers:
                layer.trainable = True

    def eval(self, x):
        if len(x.shape) == 1:
            x = x.reshape(1, self.input_size)
            batch_one = True
        else:
            x = x.T
            batch_one = False

        res = self.mdl.predict(x).T

        if batch_one:
            res = res[:, 0]

        return res

    def save(self, save_path):
        self.mdl.save(self.home+save_path)

    def save_params(self, save_path):
        pkl.dump([self.input_size, self.output_size, self.n_hidden, self.hidden_size, self.hidden_activation, self.output_activation, self.name, self.initial_activation], open(self.home+save_path, 'wb'))

    def get(self, x):
        return self.mdl.predict(x)

    def __call__(self, args):
        return self.mdl(args)
