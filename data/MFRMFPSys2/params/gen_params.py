'''
dumb script to make param file
'''

import numpy as np
import _pickle as pkl

dataset_num = 1

del_t = 0.1
J = 0.75
g = 9.81
gamma_p = [0.01, 1000, 700, 0.02, 1000, 1]

f_l_min = 0.25
f_l_max = 0.5
f_s_min = 0.0
f_s_max = 0.0
f_c_min = 0.0
f_c_max = 0.0
g2_min = 900.0
g2_max = 1100.0
g3_min = 200.0
g3_max = 400.0
g5_min = 900.0
g5_max = 1100.0
m_min = 0.5
m_max = 1.0

theta_max = np.pi/6
theta_min = np.pi/6
omega_max = 0.5
omega_min = -0.5
p_max = 0.5
p_min = -0.5
v_max = 0.5
v_min = -0.5
u_max = 0.5
u_min = 0.5

params = [del_t, J, g, gamma_p]

limits = [[theta_min, theta_max], [omega_min, omega_max], [p_min, p_max], [v_min, v_max], [u_min, u_max], [m_min, m_max], [f_l_min, f_l_max], [f_s_min, f_s_max], [f_c_min, f_c_max], [g2_min, g2_max], [g3_min, g3_max], [g5_min, g5_max]]

pkl.dump(params, open('dataset%d_params.pkl'%dataset_num, 'wb'))
pkl.dump(limits, open('dataset%d_limits.pkl'%dataset_num, 'wb'))
