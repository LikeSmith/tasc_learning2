'''
dumb script to make param file
'''

import numpy as np
import _pickle as pkl

dataset_num = 2

del_t = 0.01

px_max = 10
px_min = -10
py_max = 10
py_min = -10
theta_max = np.pi
theta_min = -np.pi
l_max = 0.5
l_min = 0.1
v_max = 1.0
v_min = -1.0

params = del_t

limits = [[px_min, px_max], [py_max, py_min], [theta_min, theta_max], [l_min, l_max], [v_min, v_max]]

pkl.dump(params, open('dataset%d_params.pkl'%dataset_num, 'wb'))
pkl.dump(limits, open('dataset%d_limits.pkl'%dataset_num, 'wb'))
