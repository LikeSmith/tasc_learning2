import scipy.io as io
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import _pickle as pkl

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

mat = io.loadmat('LQR_sim.mat')

x_p_LQR = mat['x_p']
x_s_LQR = mat['x_s']
t_LQR = mat['t'][0, :]

t, xp1, xp2, xs1, xs2, a1, a2, a_d1, a_d2, a_aux1, a_aux2, u1, u = pkl.load(open('results_sim.pkl', 'rb'))

x_p_learn = xp1
x_s_learn = xs1
t_learn = t

p_LQR = np.zeros((4, x_s_LQR.shape[1]))
p_learn = np.zeros((4, x_s_learn.shape[1]))

for i in range(4):
    p_LQR[i, :] = x_s_LQR[i*2, :]
    p_learn[i, :] = x_s_learn[i*2, :]

plt.figure()
plt.subplot(211)
plt.plot(t_learn, x_p_learn.T)
plt.gca().set_prop_cycle(None)
plt.plot(t_LQR, x_p_LQR.T, ':')
plt.xlabel('time (s)')
plt.legend([r'$\theta$ rad', r'$\dot{\theta}$ rad/s'])
plt.subplot(212)
plt.plot(t_learn, p_learn.T)
plt.gca().set_prop_cycle(None)
plt.plot(t_LQR, p_LQR.T, ':')
plt.xlabel('time (s)')
plt.ylabel('robot positions (m)')
plt.savefig('figs/compareison.pdf')
plt.show()
