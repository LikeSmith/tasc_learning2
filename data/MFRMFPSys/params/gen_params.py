'''
dumb script to make param file
'''

import numpy as np
import _pickle as pkl

dataset_num = 1
N = 4

del_t = 0.1
J = 0.75
g = 9.81
gamma_p = [0.01, 1000, 700, 0.02, 1000, 1]

c_min = 0.5
c_max = 1.5
m_min = 0.25
m_max = 0.75

theta_max = np.pi/6
theta_min = np.pi/6
omega_max = 0.5
omega_min = -0.5
p_max = 0.5
p_min = -0.5
v_max = 0.5
v_min = -0.5
u_max = 0.5
u_min = 0.5

m = []
gamma_s = []
for i in range(N):
    m.append(m_min + (m_max - m_min)*np.random.rand(1)[0])
    gamma_s.append([0.0, 1000.0, 700.0, 0.0, 1000.0, c_min + (c_max - c_min)*np.random.rand(1)[0]])

params = [del_t, J, g, gamma_p, N]

for i in range(N):
    params.append(m[i])
    params.append(gamma_s[i])

limits = [[theta_min, theta_max], [omega_min, omega_max], [p_min, p_max], [v_min, v_max], [u_min, u_max], [m_min, m_max], [c_min, c_max]]

pkl.dump(params, open('dataset%d_%dRob_params.pkl'%(dataset_num, N), 'wb'))
pkl.dump(limits, open('dataset%d_%dRob_limits.pkl'%(dataset_num, N), 'wb'))
