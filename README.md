# README #
V1.0

This repo contains all of the learning experiments related to the TASC project.

## Contents: ##

* tasc_learning: directory containing python code related to the tasc project
    * datasets: directory containing code for generating and managing datasets
    * models: directory containing code for creating, saving, loading, training, and using the various models we use in our experiments
    * experiments: directory contains scripts for running experiments
    * utils: utility functions used in experiments
* data: directory contains datasets as well as results from the experiments run on the datasets

## Systems ##
### MFRMFPSys ###
This is the Multi Functional Robot in a Mean Formation on a Plane System.  It consists of a swarm of robots on a plane that tilts about an axis.  Each robot is a mass with nonlinear friction and a force input.  The plane is modeled with a moment of inertia and nonlinear friction, as well as the effects fo the robot swarm on top of it.

### MFRMFPSys2 ###
Same as the MFRMFPSys, but rebuilt from the ground up to support variable swarm sizes

## Experiments ##
### exp_quad ###
mlp test experiment that learns a quadratic function.  Good demonstration of an MLP's mapping ability.

### exp_tasc ###
Initial successful tasc experiment, results in our ACC2018 paper come from this experiment.

### exp_tasc_modular ###
experiment for running the tasc controller in a modular manner, allowing for variable swarm sizes.

## Contacts ##
* Kyle Crandall (primary): crandallk@gwu.edu
* Dustin Webb: dustin@cs.utah.edu
* Adam Wickenheiser: amwick@gwu.edu
